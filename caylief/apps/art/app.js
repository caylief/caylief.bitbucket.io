import ldb from 'localdata';

import templates from './templates/config.json';
import emojis from './templates/emojis.json';

// Record storage used for debugging purposes, if we run into issues with recording local drawing state
// To make it less noisy we won't log unless it's above 50%.
navigator.storage.estimate().then((quota) => {
	  const percentageUsed = (quota.usage / quota.quota) * 100;
	  if (percentageUsed > 50) {
    console.log(`You've used ${percentageUsed}% of the available storage.`);
	  }
});

let states = [];

let pentype = 'brush'; // or "erase" or "paint_bucket"

const templateImages = new Map();
const templateText = new Map();
const templateEmojis = new Map();

// Variables for referencing the canvas and 2dcanvas context
let canvas; let ctx; let canvas_front; let ctx_front; let canvas_back; let ctx_back;

// Variables to keep track of the mouse position and left-button status
let mouseX; let mouseY; let mouseDown = 0;

// Variables to keep track of the touch position
let touchX; let touchY;

// Keep track of the old/last position when drawing a line
// We set it to -1 at the start to indicate that we don't have a good value for it yet
let lastX; let lastY = -1;

// Draws a line between the specified position on the supplied canvas name
// Parameters are: A canvas context, the x position, the y position, the size of the dot
function drawLine(ctx, x, y, size) {
  // If lastX is not set, set lastX and lastY to the current position
  if (lastX == -1) {
    lastX = x;
	   		lastY = y;
  }

  // Select a fill style
  ctx.strokeStyle = document.querySelector('#pencolor').value;

  // Set the line "cap" style to round, so lines at different angles can join into each other
  ctx.lineCap = 'round';

  ctx.beginPath();

  if (pentype == 'brush') {
    ctx.globalCompositeOperation = 'source-over';
    ctx.globalAlpha = document.querySelector('#penalpha').value;

    ctx.moveTo(lastX, lastY);
    ctx.lineTo(x, y);
    ctx.lineWidth = size;
    ctx.stroke();
  } else if (pentype == 'erase') {
    ctx.globalCompositeOperation = 'destination-out';
    ctx.globalAlpha = 1.0;

    ctx.moveTo(lastX, lastY);
    ctx.lineTo(x, y);
    ctx.lineWidth = size;
    ctx.stroke();
  } else {
    console.warn(`Ignored attempt to drawLine with an unexpected pentype: ${pentype}`);
  }

  ctx.closePath();

  // Update the last position to reference the current position
  lastX = x;
  lastY = y;
}

// Clear the canvas context using the canvas width and height
function clearCanvas() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function clearFull() {
  clearCanvas();

  states = [];
  document.querySelector('#undo').innerHTML = 'Undo<br />...';
  document.querySelector('#undo').disabled = true;

  ldb.delete(`${canvasId}states`);
  ldb.delete(`${canvasId}penState`);
}

function isEmptyCanvas() {
  return states.length == 0;
}
window.isEmptyCanvas = isEmptyCanvas;

let overrideWidth;
let overrideHeight;
function resizeCanvasToFitBounds(width, height) {
  overrideWidth = width;
  overrideHeight = height;

  resizeCanvasToFitScreen();
}
window.resizeCanvasToFitBounds = resizeCanvasToFitBounds;

function resizeCanvasToFitScreen() {
  const toolbarHeight = document.querySelector('#toolbar').clientHeight;
  canvas.width = overrideWidth || (window.innerWidth - 20);
  canvas.height = overrideHeight || (window.innerHeight - toolbarHeight - 40);
  canvas_back.width = canvas.width + 300;
  canvas_back.height = canvas.height + 300;
  canvas_front.width = canvas.width;
  canvas_front.height = canvas.height;

  document.querySelector('#sketchpads').style.width = canvas.width;
  document.querySelector('#sketchpads').style.height = canvas.height;

  document.querySelector('#pentype').style.height = toolbarHeight - 2;
  document.querySelector('#pencolor').style.height = toolbarHeight - 2;
  document.querySelector('#clear').style.height = toolbarHeight - 2;

  if (states.length > 0) {
    drawDataURIOnCanvas(states[states.length - 1], true);
  }
}

function matchStartColor(pixelPos, startR, startG, startB, startA, targetColor, canvasData) {
  const r = canvasData.data[pixelPos];
  const g = canvasData.data[pixelPos + 1];
  const b = canvasData.data[pixelPos + 2];
  const a = canvasData.data[pixelPos + 3];

  // If the current pixel matches the clicked color
  if (r === startR && g === startG && b === startB && a === startA) {
    return true;
  }

  // If current pixel matches the new color
  if (r === targetColor.r && g === targetColor.g && b === targetColor.b) {
    return false;
  }

  return false;
}

function floodFill(startX, startY, startR, startG, startB, startA, targetColor, canvasData) {
  let newPos;
  let x;
  let y;
  let pixelPos;
  let reachLeft;
  let reachRight;
  const drawingBoundLeft = 0;
  const drawingBoundTop = 0;
  const drawingBoundRight = canvas.width - 1;
  const drawingBoundBottom = canvas.height - 1;
  const pixelStack = [[startX, startY]];

  while (pixelStack.length) {
    newPos = pixelStack.pop();
    x = newPos[0];
    y = newPos[1];

    // Get current pixel position
    pixelPos = (y * canvas.width + x) * 4;

    // Go up as long as the color matches and are inside the canvas
    while (y >= drawingBoundTop && matchStartColor(pixelPos, startR, startG, startB, startA, targetColor, canvasData)) {
      y -= 1;
      pixelPos -= canvas.width * 4;
    }

    pixelPos += canvas.width * 4;
    y += 1;
    reachLeft = false;
    reachRight = false;

    // Go down as long as the color matches and in inside the canvas
    while (y <= drawingBoundBottom && matchStartColor(pixelPos, startR, startG, startB, startA, targetColor, canvasData)) {
      y += 1;

      // color a pixel
      canvasData.data[pixelPos] = targetColor.r;
      canvasData.data[pixelPos + 1] = targetColor.g;
      canvasData.data[pixelPos + 2] = targetColor.b;
      canvasData.data[pixelPos + 3] = targetColor.a !== undefined ? targetColor.a : 255;

      if (x > drawingBoundLeft) {
        if (matchStartColor(pixelPos - 4, startR, startG, startB, startA, targetColor, canvasData)) {
          if (!reachLeft) {
            // Add pixel to stack
            pixelStack.push([x - 1, y]);
            reachLeft = true;
          }
        } else if (reachLeft) {
          reachLeft = false;
        }
      }

      if (x < drawingBoundRight) {
        if (matchStartColor(pixelPos + 4, startR, startG, startB, startA, targetColor, canvasData)) {
          if (!reachRight) {
            // Add pixel to stack
            pixelStack.push([x + 1, y]);
            reachRight = true;
          }
        } else if (reachRight) {
          reachRight = false;
        }
      }

      pixelPos += canvas.width * 4;
    }
  }
}

// Start painting with paint bucket tool starting from pixel specified by startX and startY
function paintAt(startX, startY) {
  const hexToRgb = document.querySelector('#pencolor').value.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, (m, r, g, b) => `#${r}${r}${g}${g}${b}${b}`)
    .substring(1).match(/.{2}/g)
    .map((x) => parseInt(x, 16));
  const targetColor = {
    r: hexToRgb[0],
    g: hexToRgb[1],
    b: hexToRgb[2],
  };

  const canvasData = ctx.getImageData(0, 0, canvas.width, canvas.height);
  const pixelPos = (startY * canvas.width + startX) * 4;
  const r = canvasData.data[pixelPos];
  const g = canvasData.data[pixelPos + 1];
  const b = canvasData.data[pixelPos + 2];
  const a = canvasData.data[pixelPos + 3];

  if (r === targetColor.r && g === targetColor.g && b === targetColor.b && a !== 0) {
    // Return because trying to fill with the same color, when the canvas isn't transparent pixels.
    return;
  }

  floodFill(startX, startY, r, g, b, a, targetColor, canvasData);
  ctx.putImageData(canvasData, 0, 0);
}

// Keep track of the mouse button being pressed and draw a dot at current location
function sketchpad_mouseDown(e) {
	    // only draw if it's primary button click. Secondary button (e.g. right click by default) shouldn't trigger drawing, so image can be saved to file or copied to clipboard.
	    if (e.button != 0) {
    return;
  }

  mouseDown = 1;
  if (pentype === 'paint_bucket') {
    paintAt(e.offsetX, e.offsetY);
  } else {
    drawLine(ctx, e.offsetX, e.offsetY, document.querySelector('#penwidth').value);
  }
}

// Keep track of the mouse button being released
function sketchpad_mouseUp(e) {
  if (mouseDown == 1) {
    save();
  }
  mouseDown = 0;

  // Reset lastX and lastY to -1 to indicate that they are now invalid, since we have lifted the "pen"
  lastX = -1;
  lastY = -1;
}

const ANIMATE_BRUSH = true; // whether or not to animate the brush cursor (rotation/spinning. mouse tracking will always occur).
const ANIM_RAD_PER_SEC = 0.05; // how many radians we will add per second when rotating the brush cursor

// the animated cursor is currently a dashed ring of the pen color and a dashed ring of white. This is the
// angle adjustment we make to the dashed ring of white so they don't overlap.
const CONTRAST_OFFSET = Math.PI * 0.25;

let cursorText;
let currentAngle = 0;
let drawCursorAnim;
let lastCursorAnimTimestamp;

function sketchpad_mouseMove(e) {
  // Draw a line if the mouse button is currently being pressed
  if (mouseDown == 1 && (pentype === 'brush' || pentype === 'erase')) {
    drawLine(ctx, e.offsetX, e.offsetY, document.querySelector('#penwidth').value);
  }

  mouseX = e.offsetX;
  mouseY = e.offsetY;
  cursorText = document.querySelector('#pentype').textContent;

  // if we haven't started drawing the cursor yet, start it.
  // this is an animation loop. Even if we turn off animating the cursor, we still need to move it around with the mouse position.
  if (!drawCursorAnim) {
		    currentAngle = 0;
    drawCursorAnim = window.requestAnimationFrame(drawCursor);
  }
}

function drawCursor() {
  if (mouseX == -1) {
    return;
  }

  ctx_front.clearRect(0, 0, canvas_front.width, canvas_front.height);

  // paint bucket will be a static icon only as it doesn't have a brush width.
  if (pentype === 'paint_bucket') {
    ctx_front.save();

    // Draw a cross. horizontal line first, then vertical
    ctx_front.beginPath();
    ctx_front.shadowColor = 'white';
    ctx_front.shadowBlur = 1;
    ctx_front.moveTo(mouseX - 10, mouseY);
    ctx_front.lineTo(mouseX + 10, mouseY);
    ctx_front.stroke();
    ctx_front.closePath();
    ctx_front.moveTo(mouseX, mouseY - 10);
    ctx_front.lineTo(mouseX, mouseY + 10);
    ctx_front.stroke();
    ctx_front.closePath();

    // Then a paint bucket nearby.
    ctx_front.shadowColor = 'black';
    ctx_front.shadowBlur = 3;
    ctx_front.fillStyle = document.querySelector('#pencolor').value;
    ctx_front.font = 'bold 32px Apple Color Emoji,Segoe UI Emoji,Noto Color Emoji,Android Emoji,EmojiSymbols,EmojiOne Mozilla,Twemoji Mozilla,Segoe UI Symbol,Noto Color Emoji Compat,emoji,noto-emojipedia-fallback';
    ctx_front.fillText(cursorText, mouseX, mouseY + 36);

    ctx_front.restore();

    drawCursorAnim = window.requestAnimationFrame(drawCursor);
    return;
  }

  if (!lastCursorAnimTimestamp) {
    lastCursorAnimTimestamp = Date.now();
  }

  if (ANIMATE_BRUSH) {
    const currentCursorAnimTimestamp = Date.now();
    const deltaMillis = (currentCursorAnimTimestamp - lastCursorAnimTimestamp);
    lastCursorAnimTimestamp = currentCursorAnimTimestamp;

    currentAngle += deltaMillis / 60 * ANIM_RAD_PER_SEC;
    if (currentAngle >= Math.PI * 2) {
      currentAngle -= Math.PI * 2;
    }
  } else {
    currentAngle = 0;
  }

  ctx_front.save();
  ctx_front.globalCompositeOperation = 'source-over';
  ctx_front.strokeStyle = '#FFFFFF';
  ctx_front.lineWidth = 3;
  ctx_front.beginPath();
  ctx_front.setLineDash([2, 2]);
  ctx_front.arc(mouseX, mouseY, document.querySelector('#penwidth').value / 2, currentAngle + CONTRAST_OFFSET, Math.PI * 2 + currentAngle + CONTRAST_OFFSET);
  ctx_front.closePath();
  ctx_front.stroke();

  ctx_front.strokeStyle = document.querySelector('#pencolor').value;
  ctx_front.lineWidth = 3;
  ctx_front.beginPath();
  ctx_front.shadowColor = 'black';
  ctx_front.shadowBlur = 3;
  ctx_front.setLineDash([2, 2]);
  ctx_front.arc(mouseX, mouseY, document.querySelector('#penwidth').value / 2, currentAngle, Math.PI * 2 + currentAngle);
  ctx_front.closePath();
  ctx_front.stroke();

  ctx_front.font = 'bold 32px Apple Color Emoji,Segoe UI Emoji,Noto Color Emoji,Android Emoji,EmojiSymbols,EmojiOne Mozilla,Twemoji Mozilla,Segoe UI Symbol,Noto Color Emoji Compat,emoji,noto-emojipedia-fallback';
  ctx_front.fillText(cursorText, mouseX, mouseY + (pentype === 'brush' ? 0 : 36));

  ctx_front.restore();
  drawCursorAnim = window.requestAnimationFrame(drawCursor);
}

function sketchpad_mouseLeave(e) {
  if (drawCursorAnim) {
    window.cancelAnimationFrame(drawCursorAnim);
    drawCursorAnim = '';
  }

  // clear the drawing reticule
  ctx_front.clearRect(0, 0, canvas_front.width, canvas_front.height);
}

// Draw something when a touch start is detected
function sketchpad_touchStart() {
  // Update the touch co-ordinates
  getTouchPos();

  if (pentype === 'brush' || pentype === 'erase') {
    drawLine(ctx, touchX, touchY, document.querySelector('#penwidth').value);
  } else if (pentype === 'paint_bucket') {
    paintAt(touchX, touchY);
  }

  // Prevents an additional mousedown event being triggered
  event.preventDefault();
}

function sketchpad_touchEnd() {
	    save();

  // Reset lastX and lastY to -1 to indicate that they are now invalid, since we have lifted the "pen"
  lastX = -1;
  lastY = -1;

  // Provide consistency with handling of other touch events.
  event.preventDefault();
}

// Draw something and prevent the default scrolling when touch movement is detected
function sketchpad_touchMove(e) {
  // Update the touch co-ordinates
  getTouchPos(e);

  if (pentype === 'brush' || pentype === 'erase') {
    // During a touchmove event, unlike a mousemove event, we don't need to check if the touch is engaged, since there will always be contact with the screen by definition.
    drawLine(ctx, touchX, touchY, document.querySelector('#penwidth').value);
  }

  // Prevent a scrolling action as a result of this touchmove triggering.
  event.preventDefault();
}

// Get the touch position relative to the top-left of the canvas
// When we get the raw values of pageX and pageY below, they take into account the scrolling on the page
// but not the position relative to our target div. We'll adjust them using "target.offsetLeft" and
// "target.offsetTop" to get the correct values in relation to the top left of the canvas.
function getTouchPos(e) {
  if (!e) { var e = event; }

  if (e.touches) {
    if (e.touches.length == 1) { // Only deal with one finger
      const touch = e.touches[0]; // Get the information for finger #1
      touchX = touch.pageX - touch.target.offsetLeft;
      touchY = touch.pageY - touch.target.offsetTop;
    }
  }
}

function save() {
  const latestState = canvas.toDataURL();

  // if we've just saved the previous state, don't duplicate it.
  if (states.length > 0 && states[states.length - 1] == latestState) {
    return;
  }

  // we'll keep 255 states for now. if we exceed, we'll forget the oldest states
  if (states.length >= 255) {
    states.shift();
  }

  states.push(latestState);
  document.querySelector('#undo').innerHTML = `Undo<br />${states.length}`;
  document.querySelector('#undo').disabled = false;

  try {
    ldb.set(`${canvasId}states`, JSON.stringify(states));
  } catch (e) {
    // local storage is full, clear earlier state.
    // NOTE: since we moved to LDB which wraps IndexedDB, we probably don't need this anymore, but will keep it for now.

    let stillFull = true;
    while (states.length > 0 && stillFull) {
      states.shift();

      try {
        ldb.set(`${canvasId}states`, JSON.stringify(states));

        // we won't get here if exception is thrown.
        stillFull = false;
      } catch (e) {
        // do nothing here. we'll keep looping until storage is no longer full, we'll keep shifting states out.
      }
    }

    // represent current state history after we cleared older states.
    document.querySelector('#undo').innerHTML = `Undo<br />${states.length > 0 ? `${states.length}` : '...'}`;
    document.querySelector('#undo').disabled = (!(states.length > 0));
  }
}

function undo() {
  if (states.length <= 0) {
    return;
  }
  states.pop();
  ldb.set(`${canvasId}states`, JSON.stringify(states));

  document.querySelector('#undo').innerHTML = `Undo<br />${states.length > 0 ? `${states.length}` : '...'}`;
  document.querySelector('#undo').disabled = (!(states.length > 0));

  if (states.length > 0) {
    drawDataURIOnCanvas(states[states.length - 1]);
  } else {
    clearCanvas();
  }
}

// simple cache which speeds up when we're repeatedly drawing the same image, which occurs particularly when we're resizing the window.
let cachedLastImage;

function drawOnCanvas(img) {
  clearCanvas();
  ctx.globalAlpha = 1.0;
  ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
}
window.drawOnCanvas = drawOnCanvas;

function drawDataURIOnCanvas(strDataURI, fitCanvas) {
  console.log(`drawing canvas, fitCanvas${fitCanvas}`);

  if (cachedLastImage && strDataURI === cachedLastImage.getAttribute('src')) {
    const img = cachedLastImage;
    clearCanvas();
    ctx.globalAlpha = 1.0;

    // if image is too big...
    if (img.width > canvas.width || img.height > canvas.height) {
      // size it down to fit.
      const widthFactor = canvas.width / img.width;
      const heightFactor = canvas.height / img.height;
      const useWidthFactor = widthFactor < heightFactor;

      ctx.drawImage(img, 0, 0, useWidthFactor ? img.width * widthFactor : img.width * heightFactor, useWidthFactor ? img.height * widthFactor : img.height * heightFactor);
    } else if (fitCanvas) {
      ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
    } else {
      ctx.drawImage(img, 0, 0);
    }

    return;
  }

  'use strict';
  const img = new window.Image();
  img.addEventListener('load', () => {
    clearCanvas();
    ctx.globalAlpha = 1.0;

    // if image is too big...
    if (img.width > canvas.width || img.height > canvas.height) {
      // size it down to fit.
      const widthFactor = canvas.width / img.width;
      const heightFactor = canvas.height / img.height;
      const useWidthFactor = widthFactor < heightFactor;

      ctx.drawImage(img, 0, 0, useWidthFactor ? img.width * widthFactor : img.width * heightFactor, useWidthFactor ? img.height * widthFactor : img.height * heightFactor);
    } else if (fitCanvas) {
      ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
    } else {
      ctx.drawImage(img, 0, 0);
    }
  });
  img.setAttribute('src', strDataURI);
  cachedLastImage = img;
}

function loadTemplate(templateKey, templateType) {
  if (templateType == 'image') {
    document.querySelector('#template').style.backgroundImage = `url('${templateImages.get(templateKey)}')`;
    return;
  }

  let textToDraw;
  if (templateType == 'text') {
    textToDraw = templateText.get(templateKey);
  } else if (templateType == 'emoji') {
    textToDraw = templateEmojis.get(templateKey);
  } else {
    document.querySelector('#template').style.backgroundImage = '';
    return;
  }

  ctx_back.clearRect(0, 0, canvas_back.width, canvas_back.height);
  const desiredHeight = Math.min(700, canvas_back.height); // 160px is apparently the max size before emojis get blurry. oh well.
  ctx_back.save();
  ctx_back.font = `${desiredHeight}px ` + 'Apple Color Emoji,Segoe UI Emoji,Noto Color Emoji,Android Emoji,EmojiSymbols,EmojiOne Mozilla,Twemoji Mozilla,Segoe UI Symbol,Noto Color Emoji Compat,emoji,noto-emojipedia-fallback';
  ctx_back.textAlign = 'center';
  ctx_back.textBaseline = 'middle';
  ctx_back.fillText(textToDraw, canvas_back.width / 2, canvas_back.height / 2 + 42);
  ctx_back.restore();

  document.querySelector('#template').style.backgroundImage = `url('${canvas_back.toDataURL()}')`;
  ctx_back.clearRect(0, 0, canvas_back.width, canvas_back.height);
}

let controlsState = {
  color: '',
  width: '',
  alpha: '',
};

function saveControlsState() {
  // No reason to save to local persistence if we've already recorded the last state.
  if (controlsState.color == document.querySelector('#pencolor').value
			&& controlsState.width == document.querySelector('#penwidth').value
			&& controlsState.alpha == document.querySelector('#penalpha').value) {
    return;
  }

  controlsState.color = document.querySelector('#pencolor').value;
  controlsState.width = document.querySelector('#penwidth').value;
  controlsState.alpha = document.querySelector('#penalpha').value;

  ldb.set(`${canvasId}penState`, JSON.stringify(controlsState));
}

function loadStates() {
	  states = [];
	  document.querySelector('#undo').disabled = true;

	  ldb.get(`${canvasId}states`, (value) => {
	  	const loadedStates = JSON.parse(value);
    if (loadedStates) {
      states = loadedStates;
    }

    if (states.length > 0) {
      document.querySelector('#undo').innerHTML = `Undo<br />${states.length}`;
      document.querySelector('#undo').disabled = false;
      drawDataURIOnCanvas(states[states.length - 1], true);
    }
	  });

	  ldb.get(`${canvasId}penState`, (value) => {
	    const loadedPenState = JSON.parse(value);
    if (loadedPenState) {
      controlsState = loadedPenState;
    }

    if (controlsState) {
      if (controlsState.color) document.querySelector('#pencolor').value = controlsState.color;
      if (controlsState.width) document.querySelector('#penwidth').value = controlsState.width;
      if (controlsState.alpha) document.querySelector('#penalpha').value = controlsState.alpha;
    }
	  });
}

function ready(callback) {
  // in case the document is already rendered
  if (document.readyState != 'loading') callback();
  // modern browsers
  else if (document.addEventListener) document.addEventListener('DOMContentLoaded', callback);
}

let templatesDropdown;
let lastColorInteractTimeout;
var canvasId = '';

// Set-up the canvas and add our event handlers after the page has loaded
ready(() => {
	    const queryString = window.location.search;
	    const urlParams = new URLSearchParams(queryString);

  if (urlParams.has('id')) {
    canvasId = urlParams.get('id');
  }

  canvas = document.querySelector('#sketchpad');
  canvas_back = document.querySelector('#sketchpad_back');
  canvas_front = document.querySelector('#sketchpad_front');

  resizeCanvasToFitScreen();

  window.addEventListener('resize', (event) => {
    resizeCanvasToFitScreen();
  });

  templatesDropdown = new Choices('#templates', {
    allowHTML: false,
		  sorter: (a, b) => a.value.localeCompare(b.value),
  }).setChoices(() => {

    const templateImagesOptions = templates.templateImages.map((e) => {
        // side effect. We can probably get rid of this by using customProperties.
        templateImages.set(e.key, e.value);

        return {
            label: e.key,
            value: `${e.key},image`,
        };
    });

    const emojisOptions = Object.entries(emojis)
    .filter(([key, value]) => value.category !== '_custom' && value.char != null)
    .sort(([akey, avalue], [bkey, bvalue]) => akey.localeCompare(bkey))
    .map(([key, value]) => {
      // side effect. We can probably get rid of this by using customProperties.
      templateEmojis.set(key, value.char);

      return {
        label: `${value.char} ${key}`,
        value: `${key},emoji`,
      };
    });

    return Promise.resolve(templateImagesOptions.concat(emojisOptions));
  }).then((instance) => {
    instance.setChoiceByValue('None');

    instance.passedElement.element.addEventListener('change', (e) => {
      const options = e.detail.value.split(',');
      loadTemplate(...options);
    });
  });

  // If the browser supports the canvas tag, get the 2d drawing context for this canvas
  if (canvas.getContext) { ctx = canvas.getContext('2d'); }
  if (canvas_back.getContext) { ctx_back = canvas_back.getContext('2d'); }
  if (canvas_front.getContext) { ctx_front = canvas_front.getContext('2d'); }

  // Check that we have a valid context to draw on/with before adding event handlers
  if (ctx) {
    // React to mouse events on the canvas, and mouseup on the entire document
    // catch mouseup on canvas just in case it traps the event.
    // Not able to repro how Kegan gets it stuck, but sometimes it gets stuck where mouseup doesn't fire while all of the other events do.
    canvas.addEventListener('mousedown', sketchpad_mouseDown);
    canvas.addEventListener('mousemove', sketchpad_mouseMove);
    canvas.addEventListener('mouseleave', sketchpad_mouseLeave);
    canvas.addEventListener('mouseup', sketchpad_mouseUp);
    canvas_front.addEventListener('mousedown', sketchpad_mouseDown);
    canvas_front.addEventListener('mousemove', sketchpad_mouseMove);
    canvas_front.addEventListener('mouseleave', sketchpad_mouseLeave);
    canvas_front.addEventListener('mouseup', sketchpad_mouseUp);
    window.addEventListener('mouseup', sketchpad_mouseUp);

    // React to touch events on the canvas. We will actually prevent scrolling when drawing, so the corresponding listeners are not passive.
    canvas.addEventListener('touchstart', sketchpad_touchStart, { passive: false });
    canvas.addEventListener('touchend', sketchpad_touchEnd, { passive: false });
    canvas.addEventListener('touchmove', sketchpad_touchMove, { passive: false });
    canvas_front.addEventListener('touchstart', sketchpad_touchStart, { passive: false });
    canvas_front.addEventListener('touchend', sketchpad_touchEnd, { passive: false });
    canvas_front.addEventListener('touchmove', sketchpad_touchMove, { passive: false });
  }

  document.querySelector('#clear').addEventListener('click', clearFull);
  document.querySelector('#undo').addEventListener('click', undo);

  document.querySelector('#pentype').addEventListener('click', () => {
    if (pentype == 'brush') {
      pentype = 'paint_bucket';
    } else if (pentype == 'paint_bucket') {
      pentype = 'erase';
    } else if (pentype == 'erase') {
      pentype = 'brush';
    }

    if (pentype == 'brush') {
      document.querySelector('#pentype').innerHTML = '&#x1F58C;';
    } else if (pentype == 'paint_bucket') {
      document.querySelector('#pentype').innerHTML = '&#x1FAA3;';
    } else if (pentype == 'erase') {
      document.querySelector('#pentype').innerHTML = '&#x2326;';
    }
  });
  document.querySelector('#penwidth').addEventListener('change', saveControlsState);
  document.querySelector('#penalpha').addEventListener('change', saveControlsState);
  document.querySelector('#pencolor').addEventListener('input', () => {
    if (lastColorInteractTimeout) {
      clearTimeout(lastColorInteractTimeout);
    }

    // auto-closes the color picker after interaction.
    lastColorInteractTimeout = setTimeout(() => {
      document.querySelector('#pencolor').type = 'text';
      document.querySelector('#pencolor').type = 'color';
    }, 1000);

    saveControlsState();
  });

  document.querySelector('#loadfromfile').addEventListener('change', function () {
		  drawDataURIOnCanvas(URL.createObjectURL(this.files[0]));
  });

  document.addEventListener('paste', (e) => {
    if (!e.clipboardData) {
      return;
    }

    const { items } = e.clipboardData;
    if (!items) {
      return;
    }

    let isImage = false;
    for (let i = 0; i < items.length; i++) {
      if (items[i].type.indexOf('image') == -1) {
        continue;
      }

      const blob = items[i].getAsFile();
      const URLObj = window.URL || window.webkitURL;
      drawDataURIOnCanvas(URLObj.createObjectURL(blob));
      isImage = true;
    }
    if (isImage == true) {
      e.preventDefault();
    }
  }, false);

  loadStates();
});
