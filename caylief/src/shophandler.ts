/* eslint-disable @typescript-eslint/no-floating-promises */
import { GameObject } from './models/gameobject';
import GameObjectDefinition from './models/gameobjectdefinition';
import GameState from './models/gamestate';
import Narrator from './narrator';

class ShopHandler {
  readonly narrator: Narrator;

  readonly gameState: GameState;

  readonly shopview: HTMLElement;

  readonly onPurchase: () => void;

  readonly randomizerInt: (max: number) => number;

  activeShop: GameObject | undefined;

  lastShelfWidth: number | undefined;

  constructor(
    narrator: Narrator,
    gameState: GameState,
    shopview: HTMLElement,
    onPurchase: () => void,
    randomizerInt: (max: number) => number,
  ) {
    this.narrator = narrator;
    this.gameState = gameState;
    this.shopview = shopview;
    this.onPurchase = onPurchase;
    this.randomizerInt = randomizerInt;
  }

  organizeShopIfActive() {
    if (this.shopview.style.display !== 'block') {
      return;
    }

    const minShelfItems = 3;
    const shelfWidth = (this.shopview.querySelector('#shopview .shelf') as HTMLElement).offsetWidth;

    this.shopview.querySelectorAll('#shopview .shopItems').forEach((e) => {
      const el = e as HTMLElement;

      // optimization: if items fit on the shelf and we're shrinking the shelf, we can skip the check.
      if (el.offsetWidth <= shelfWidth && (this.lastShelfWidth && this.lastShelfWidth > shelfWidth)) {
        return;
      }

      Array.from(el.children).forEach((c) => {
        const cEle = c as HTMLElement;
        cEle.style.display = 'inline-block';
      });

      let childIndex = el.children.length - 1;
      while (el.offsetWidth > shelfWidth && childIndex >= minShelfItems) {
        (el.children[childIndex] as HTMLElement).style.display = 'none';
        childIndex -= 1;
      }
    });

    this.lastShelfWidth = shelfWidth;
  }

  static allowDrop(e: DragEvent) {
    e.preventDefault();
  }

  static dragToPurchase(e: DragEvent) {
    if (!e.target) {
      return;
    }

    // If we start the drag from image or text inside, pop back to the container so we're referencing a consistent eleemnt.
    let targetE = e.target as Element;
    if (targetE.nodeName === 'IMG') {
      targetE = targetE.parentElement as Element;
    }

    if (!targetE.getAttribute) {
      return;
    }

    e.dataTransfer?.setData('itemName', targetE.getAttribute('itemName') ?? '');
    e.dataTransfer?.setData('price', targetE.getAttribute('price') ?? '');
  }

  dropToPurchase(e: DragEvent) {
    e.preventDefault();
    const element = e.target as Element;

    if (element.id !== 'purchase') {
      return;
    }

    const itemName = e.dataTransfer?.getData('itemName') ?? '';
    const price = parseInt(e.dataTransfer?.getData('price') ?? '0', 10);
    const objectProperties = this.gameState.objectDictionary.get(itemName) as GameObjectDefinition;

    e.dataTransfer?.setData('itemName', '');
    e.dataTransfer?.setData('price', '');

    if (!itemName || itemName.length <= 0 || !price || !objectProperties) {
      return;
    }

    const moneyAvailable = this.gameState.savable.backpack.resources.get('coin') ?? 0;
    if (moneyAvailable < price) {
      const message = `${this.gameState.savable.playerName} tried to purchase 1 ${GameObjectDefinition.getDisplayName(objectProperties, itemName)} for ${price} coins, but didn't have enough money.`;
      this.narrator.configureNarratorBox(message);
      return;
    }

    Narrator.playSound('shopPurchaseSound');
    const message = `${this.gameState.savable.playerName} purchased 1 ${GameObjectDefinition.getDisplayName(objectProperties, itemName)} with ${price} coins.`;
    this.narrator.configureNarratorBox(message);

    this.gameState.savable.backpack.add(itemName, 1);
    this.gameState.savable.backpack.remove('coin', price);

    this.onPurchase();
  }

  activateShop(shop: GameObject) {
    Narrator.playSound('shopVisitSound');

    this.activeShop = shop;
    const message = `${this.gameState.savable.playerName} visited the ${this.activeShop.getDisplayName()}.`;
    this.narrator.configureNarratorBox(message);

    const allShopItems = this.shopview.querySelector('#allShopItems') as Element;

    const shopInventory = this.gameState.shopInventories.get(shop.ser.name) ?? [];
    if (shopInventory.length === 0) {
      return;
    }

    let shopItemsHTML = '';
    for (let shelf = 0; shelf < 3; shelf += 1) {
      shopItemsHTML += '<div class="shopItems">';

      // stagger the shop inventory reading by the shelf we're on
      let shopInventoryIndex = Math.min(shelf, shopInventory.length - 1);
      for (let item = 0; item < 10; item += 1) {
        const itemName = shopInventory[shopInventoryIndex];
        const objectProperties = this.gameState.objectDictionary.get(itemName) as GameObjectDefinition;
        const price = objectProperties.price ?? '1';

        shopItemsHTML += `<div class="shopItem" itemName="${itemName}" price="${price}" draggable="true"><span class="itemDisplay">${objectProperties.textRepresentation}</span><p class="price">Price: ${price} 🪙</p></div>`;

        shopInventoryIndex += 1;
        if (shopInventoryIndex >= shopInventory.length) {
          shopInventoryIndex = 0;
        }
      }

      shopItemsHTML += '</div>\
        <div class="shelf" style="width: 80%;"></div>';
    }

    allShopItems.innerHTML = shopItemsHTML;

    this.narrator.configureToRead(this.shopview, '#shopview .shopItem p.price');
    this.narrator.configureToRead(this.shopview, '#shopview p.purchaseInstruction');

    this.shopview.querySelectorAll('#shopview .shopItem .price').forEach((e) => {
      (e as HTMLElement).style.setProperty('--pricetag-wobble', `${this.randomizerInt(14) - 7}deg`);
    });

    this.shopview.querySelectorAll('#shopview .shopItem').forEach((e) => {
      e.addEventListener('dragstart', (ev) => ShopHandler.dragToPurchase(ev as DragEvent));
      e.addEventListener('dragover', (ev) => ShopHandler.allowDrop(ev as DragEvent));
    });

    const purchaseTarget = this.shopview.querySelector('#purchase') as Element;
    if (!purchaseTarget.getAttribute('setup')) {
      purchaseTarget.addEventListener('drop', (ev) => this.dropToPurchase(ev as DragEvent));
      purchaseTarget.addEventListener('dragover', (ev) => ShopHandler.allowDrop(ev as DragEvent));
      purchaseTarget.setAttribute('setup', 'true');
    }

    this.shopview.style.display = 'block';
    this.organizeShopIfActive();
  }
}

export default ShopHandler;
