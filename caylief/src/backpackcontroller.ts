import CraftEvaluation from './models/craftingevaluation';
import { SerializableGameObject } from './models/gameobject';
import GameObjectDefinition from './models/gameobjectdefinition';
import GameObjectProperties from './models/gameobjectproperties';
import GameState from './models/gamestate';
import Recipe from './models/recipe';
import Narrator from './narrator';
import ObjectSpawner from './objectspawner';

enum RemoveType {
  Drop,
  Eat,
  DropStateful,
}

class BackpackController {
  readonly gameState: GameState;

  readonly backpackview: HTMLElement;

  readonly narrator: Narrator;

  readonly objectSpawner: ObjectSpawner;

  readonly randomizerInt: (max: number) => number;

  readonly onRemoveItemCallback: () => void;

  constructor(
    gameState: GameState,
    backpackview: HTMLElement,
    narrator: Narrator,
    objectSpawner: ObjectSpawner,
    randomizerInt: (max: number) => number,
    onRemoveItemCallback: () => void,
  ) {
    this.gameState = gameState;
    this.backpackview = backpackview;
    this.narrator = narrator;
    this.objectSpawner = objectSpawner;
    this.randomizerInt = randomizerInt;
    this.onRemoveItemCallback = onRemoveItemCallback;
  }

  configureBackpackForReading(backpackHTML: string) {
    this.narrator.configureForReading(this.backpackview, backpackHTML, '#backpackview > .content', '#backpackview > .content p');
  }

  configureCraftingResultsForReading(resultsHTML: string) {
    this.narrator.configureForReading(document, resultsHTML, '#craftingresults', '#craftingresults > p');
  }

  configureCraftingHintsForReading(craftingHintsHTML: string) {
    this.narrator.configureForReading(document, craftingHintsHTML, '.craftingrecipes', '.craftingrecipes > p.hint');
  }

  evaluateCrafting(): CraftEvaluation | undefined {
    const resourceCosts: Map<string, number> = new Map();
    const slots: string[] = [];
    for (let i = 0; i < 20; i += 1) {
      const slot = document.getElementById(`slot${i + 1}`) as Element;
      const resourceName = slot.getAttribute('itemName') ?? '';

      slots.push(resourceName);
      if (resourceName && resourceName !== '') {
        resourceCosts.set(resourceName, (resourceCosts.get(resourceName) ?? 0) + 1);
      }
    }

    const matchedRecipes: Map<string, CraftEvaluation> = new Map();
    this.gameState.objectRecipes.forEach((fullRecipe, objName) => {
      if (fullRecipe.madeIn.includes('crafting') && fullRecipe.recipe.length === slots.length && fullRecipe.recipe.every((val, index) => val === slots[index])) {
        let enoughMaterials = true;
        resourceCosts.forEach((resourceCost, resourceName) => {
          if ((this.gameState.savable.backpack.resources.get(resourceName) ?? 0) < resourceCost) {
            enoughMaterials = false;
          }
        });

        matchedRecipes.set(objName, new CraftEvaluation(
          objName,
          fullRecipe.recipe,
          resourceCosts,
          enoughMaterials,
        ));
      }
    });

    // At this point we don't expect to have more than one matched recipe since the recipes will be quite unique. If this ever changes, we'll reconsider how to choose this.
    const firstMatchedRecipe = [...matchedRecipes][0] ?? null;
    const craftButton = (document.querySelector('#craftbutton') as HTMLButtonElement);

    if (firstMatchedRecipe) {
      const objProperties = this.gameState.objectDictionary.get(firstMatchedRecipe[1].objName) as GameObjectDefinition;
      const objDisplayName = GameObjectDefinition.getDisplayName(objProperties, firstMatchedRecipe[1].objName);

      let displayText;
      if (objProperties.imageRepresentation) {
        displayText = `${objDisplayName} <img src='${objProperties.imageRepresentation}' width='18' height='18' alt='${objDisplayName}' />`;
      } else {
        displayText = `${objDisplayName} ${objProperties.textRepresentation}`;
      }

      if (firstMatchedRecipe[1].enoughMaterials) {
        this.configureCraftingResultsForReading(`<p>${this.gameState.savable.playerName} can craft a ${displayText}.</p>`);
        craftButton.disabled = false;
      } else {
        this.configureCraftingResultsForReading(`<p>${this.gameState.savable.playerName === 'You' ? 'You do' : `${this.gameState.savable.playerName} does`} not have enough materials to craft a ${displayText}.</p>`);
        craftButton.disabled = true;
      }
    } else {
      this.configureCraftingResultsForReading('...');
      craftButton.disabled = true;
    }

    return (firstMatchedRecipe ? firstMatchedRecipe[1] : undefined);
  }

  configureCraftingRecipes() {
    let craftingRecipesHTML = '<div class=\'spacer\'>&nbsp;</div>';
    this.gameState.objectRecipes.forEach((v, k) => {
      if (!v.madeIn.includes('crafting')) {
        return;
      }

      const known = this.gameState.savable.discoveredRecipes.crafting.includes(k);
      const objProperties = this.gameState.objectDictionary.get(k) as GameObjectDefinition;

      let displayText;
      if (objProperties.imageRepresentation) {
        displayText = `${objProperties.textRepresentation}`;
      } else {
        displayText = `${objProperties.displayName ?? objProperties.name} ${objProperties.textRepresentation}`;
      }

      craftingRecipesHTML += `<p itemName="${k}" class="recipe">${displayText} (select ${known ? 'to set recipe' : 'to view hints'})</p>`;
    });

    const craftingrecipes = document.querySelector('.craftingrecipes') as HTMLElement;
    craftingrecipes.innerHTML = craftingRecipesHTML;

    craftingrecipes.querySelectorAll('.craftingrecipes > p').forEach((p) => p.addEventListener('click', (e) => {
      const recipeName = (e.target as Element).getAttribute('itemName');
      if (!recipeName) {
        return;
      }

      if (this.gameState.savable.discoveredRecipes.crafting.includes(recipeName)) {
        const fullRecipe = this.gameState.objectRecipes.get(recipeName) as Recipe;

        // Update slots with the correct recipe.
        for (let i = 0; i < 20; i += 1) {
          const slot = document.getElementById(`slot${i + 1}`) as Element;

          const ingredient = fullRecipe.recipe[i];
          if (ingredient === '') {
            slot.setAttribute('itemName', '');
            slot.innerHTML = '&nbsp;';
          } else {
            const objectProperties = this.gameState.objectDictionary.get(ingredient) as GameObjectDefinition;
            slot.setAttribute('itemName', ingredient);
            slot.innerHTML = objectProperties.imageRepresentation ? `<img src='${objectProperties.imageRepresentation}' width='48' height='48' alt='${ingredient}' itemName='${ingredient}'\
                draggable="true" />`
              : objectProperties.textRepresentation;

            slot.querySelector('img')?.addEventListener('drop', (ev) => this.drop(ev));
            slot.querySelector('img')?.addEventListener('dragover', (ev) => BackpackController.allowDrop(ev));
            slot.querySelector('img')?.addEventListener('dragstart', (ev) => BackpackController.dragStart(ev));
          }
        }

        this.evaluateCrafting();
      } else {
        // Show hints
        let craftingHintsHTML = '<div class=\'spacer\'>&nbsp;</div>';
        const fullRecipe = this.gameState.objectRecipes.get(recipeName) as Recipe;
        const objProperties = this.gameState.objectDictionary.get(recipeName) as GameObjectDefinition;

        let displayText;
        if (objProperties.imageRepresentation) {
          displayText = `${objProperties.textRepresentation}`;
        } else {
          displayText = `${objProperties.displayName ?? objProperties.name} ${objProperties.textRepresentation}`;
        }

        if (fullRecipe.hints) {
          craftingHintsHTML += `<p class="hint">Hints for making a ${displayText}.</p>`;
          craftingHintsHTML += fullRecipe.hints.map((hint) => `<p class="hint">${hint}</p>`).join('');
        } else {
          craftingHintsHTML += '<p>No hints yet.</p>';
        }

        craftingHintsHTML += '<button id=\'resetrecipes\'><span class="icon">↩</span> Pick another recipe</button>';

        this.configureCraftingHintsForReading(craftingHintsHTML);

        (document.querySelector('#resetrecipes') as HTMLButtonElement).addEventListener('click', () => {
          this.configureCraftingRecipes();
        });
      }
    }));
  }

  static allowDrop(e: DragEvent) {
    e.preventDefault();
  }

  static dragStart(e: DragEvent) {
    if (!e.target || !(e.target as Element).getAttribute) {
      return;
    }

    // If we start the drag from the image inside, pop back to the container to match what we do when it's text-represented.
    let targetE = e.target as Element;
    if (targetE.nodeName === 'IMG') {
      targetE = targetE.parentElement as Element;
    }

    e.dataTransfer?.setData('itemName', targetE.getAttribute('itemName') ?? '');

    if (targetE.className.indexOf('craftingslot') !== -1) {
      e.dataTransfer?.setData('slotid', targetE.id);
    }
  }

  drop(e: DragEvent) {
    e.preventDefault();
    let element = e.target as Element;

    if (element.id === 'clearcraftingslot') {
      const slotid = e.dataTransfer?.getData('slotid');
      if (slotid) {
        const slot = document.getElementById(slotid) as Element;
        slot.setAttribute('itemName', '');
        slot.innerHTML = '&nbsp;';

        this.evaluateCrafting();
      }
    } else {
      // If we start the drag from the image inside, pop back to the container to match what we do when it's text-represented.
      if (element.nodeName === 'IMG') {
        element = element.parentElement as Element;
      }

      const itemName = e.dataTransfer?.getData('itemName');
      if (itemName && itemName.length > 0) {
        const objectProperties = this.gameState.objectDictionary.get(itemName) as GameObjectDefinition;

        if (objectProperties) {
          element.setAttribute('itemName', itemName);
          element.innerHTML = objectProperties.imageRepresentation ? `<img src='${objectProperties.imageRepresentation}' width='48' height='48' alt='${itemName}' draggable="true" />`
            : objectProperties.textRepresentation;

          element.querySelector('img')?.addEventListener('drop', (ev) => this.drop(ev));
          element.querySelector('img')?.addEventListener('dragover', (ev) => BackpackController.allowDrop(ev));
          element.querySelector('img')?.addEventListener('dragstart', (ev) => BackpackController.dragStart(ev));
          this.evaluateCrafting();
        }
      }
    }
  }

  updateBackpackView(addDropButtons: boolean, makeDraggable: boolean) {
    let backpackHTML = '';
    this.gameState.savable.backpack.resources.forEach((resourceCount: number, resourceName: string) => {
      if (resourceCount <= 0) {
        return;
      }

      const objectProperties = this.gameState.objectDictionary.get(resourceName) as GameObjectDefinition;
      const displayName = GameObjectDefinition.getDisplayName(objectProperties, resourceName);
      const representation = objectProperties.imageRepresentation ? `<img src='${objectProperties.imageRepresentation}' width='48' height='48' alt='${resourceName}' itemName='${resourceName}' />` : objectProperties.textRepresentation;

      backpackHTML += `<div class='row' itemName="${resourceName}" ${makeDraggable ? 'draggable="true"' : ''}>\
          ${representation} <p class='backpackItem'>${displayName}: ${resourceCount}</p>`;

      if (addDropButtons) {
        backpackHTML += `<div class="backpackItemButtons"><button class="dropItemButton" itemName="${resourceName}">🫳 Drop 1</button>`;

        const objectClassProperties = this.gameState.objectClassProperties.get(resourceName) as GameObjectProperties;
        if (objectClassProperties.isEdible) {
          backpackHTML += `<button class="eatItemButton" itemName="${resourceName}">😋 Eat 1</button>`;
        }

        backpackHTML += '</div>';
      }

      backpackHTML += '</div>';
    });

    this.gameState.savable.backpack.statefulObjects.forEach((statefulObject: SerializableGameObject) => {
      const definition = this.gameState.objectDictionary.get(statefulObject.type) as GameObjectDefinition;
      const displayName = GameObjectDefinition.getDisplayName(definition, statefulObject.name);

      let representation;
      if (statefulObject.customState.paintingImageUrl) {
        representation = `<img src="${statefulObject.customState.paintingImageUrl}" width="50" height="50" />`;
      } else {
        representation = definition.textRepresentation;
      }

      backpackHTML += `<div class='row' itemName="${statefulObject.customState.id}">\
          ${representation} <p class='backpackItem'>${displayName}</p>`;

      if (addDropButtons) {
        backpackHTML += `<div class="backpackItemButtons"><button class="dropStatefulItemButton" itemName="${statefulObject.customState.id}">🫳 Drop</button></div>`;
      }

      backpackHTML += '</div>';
    });

    if (backpackHTML === '') {
      backpackHTML += '<p>The backpack is empty. Drag items on top of the backpack to put them in.</p>';
    }
    this.configureBackpackForReading(backpackHTML);

    if (makeDraggable) {
      this.backpackview.querySelectorAll('#backpackview > .content > .row').forEach((el) => {
        const ele = el as HTMLElement;

        ele.addEventListener('dragstart', (ev) => BackpackController.dragStart(ev));
      });
    }
  }

  removeItemFromBackpack(e: Event, removeType: RemoveType) {
    const resourceToDrop = (e.target as Element).getAttribute('itemName');
    if (!resourceToDrop) {
      return;
    }

    if (removeType === RemoveType.DropStateful) {
      let droppedItem;
      this.gameState.savable.backpack.statefulObjects.forEach((statefulObject, i, objs) => {
        if (statefulObject.customState.id !== resourceToDrop) {
          return;
        }

        // Because we're only looking for one specific object to drop, it's OK that we don't completely iterate the rest of them after this occurs.
        [droppedItem] = objs.splice(i, 1);

        this.backpackview.querySelectorAll('#backpackview > .content > .row').forEach((rowe) => {
          if (rowe.getAttribute('itemName') === resourceToDrop) {
            rowe.remove();
          }
        });

        droppedItem.x = -this.gameState.savable.globalXOffset + 100 + this.randomizerInt(50);
        droppedItem.y = -this.gameState.savable.globalYOffset + 100 + this.randomizerInt(50);
        this.objectSpawner.makeObjectFromSerialized(droppedItem);
      });

      this.narrator.narrateFromAction(this.gameState, 'removeItemSound', droppedItem);
      this.onRemoveItemCallback();
      return;
    }

    if (!this.gameState.savable.backpack.remove(resourceToDrop, 1)) {
      // if backpack couldn't remove the item, quit early. Unlikely.
      return;
    }

    const newCount = this.gameState.savable.backpack.resources.get(resourceToDrop) ?? 0;
    const objectProperties = this.gameState.objectDictionary.get(resourceToDrop) as GameObjectDefinition;
    const displayName = GameObjectDefinition.getDisplayName(objectProperties, resourceToDrop);

    this.backpackview.querySelectorAll('#backpackview > .content > .row').forEach((rowe) => {
      if (rowe.getAttribute('itemName') !== resourceToDrop) {
        return;
      }

      if (newCount <= 0) {
        rowe.remove();
      } else {
        const contentp = rowe.querySelector('.backpackItem') as Element;
        contentp.textContent = `${displayName}: ${newCount}`;

        Narrator.addEmphasis(contentp as HTMLElement);
      }
    });

    if (removeType === RemoveType.Drop) {
      // this isn't a stateful object so we'll create a new one from scratch.
      // globalXOffset  and globalYOffset should be applied negatively to place the item in the top-left corner of the current view.
      // This is because the Y offset pushes all items downwards (higher Y) in order to "scroll up"
      const xCoord = -this.gameState.savable.globalXOffset + 100 + this.randomizerInt(50);
      const yCoord = -this.gameState.savable.globalYOffset + 100 + this.randomizerInt(50);
      const droppedItem = this.objectSpawner.makeObject(resourceToDrop, xCoord, yCoord);
      this.narrator.narrateFromAction(this.gameState, 'removeItemSound', droppedItem);
    } else {
      this.narrator.narrateFromAction(this.gameState, objectProperties.eatSound ?? 'eatingSound', [displayName]);
    }

    this.onRemoveItemCallback();
  }
}

export {
  RemoveType,
};

export default BackpackController;
