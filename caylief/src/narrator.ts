/* eslint-disable @typescript-eslint/no-floating-promises */
import { GameObject } from './models/gameobject';
import GameState from './models/gamestate';

const synth = window.speechSynthesis;
// when highlighting a word, how long to wait before removing the highlight.
// this is probably subjective taste, and subject to how fast the synthesis engine says the words.
// we could be more deterministic later, if we want.
const EMPHASIS_REMOVE_DELAY_MS = 300;
const WORD_BOUNDARIES = [' ', '/'];

const LIST_FORMATTER = new Intl.ListFormat('en');

let sounds: Record<string, () => Promise<string>>;

interface UtterOptions {
  readonly content?: Element;

  readonly contentOffset?: number;

  readonly wordOffset?: number;

  readonly highlightWords?: boolean;

  readonly singleWord?: boolean;

  readonly sayTheLetters?: boolean;
}

const delay = (t: number) => new Promise((resolve) => { setTimeout(resolve, t); });

class Narrator {
  readonly utterRate: number;

  readonly narratorBox: HTMLElement;

  constructor(utterRate: number, narratorBox: HTMLElement) {
    this.utterRate = utterRate;
    this.narratorBox = narratorBox;
  }

  static readonly VOWELS = ['a', 'e', 'i', 'o', 'u'];

  static adjustPronunciation(text: string) {
    // hack, will think of a more structured way to enable these adjustments.
    return text.replace(/bow/ig, 'bō');
  }

  static addEmphasis(content: HTMLElement) {
    let emphasisHtml = '';

    // Deduplicate spaces to make detecting words easier.
    const phrase = content.innerHTML?.replace(/\s+/ig, ' ') ?? '';

    let wordIndex = 0;
    let inWord = false;
    let inEmoji = false;
    let inHtml = false;

    // eslint-disable-next-line max-len
    let nextEmojiIndex = phrase.search(/(?![*#0-9]+)[\p{Emoji}\p{Emoji_Modifier}\p{Emoji_Component}\p{Emoji_Modifier_Base}\p{Emoji_Presentation}]/gu);
    for (let i = 0; i < phrase.length; i += 1) {
      // include emojis in the final phrase but omit them from the words/letter emphasis
      if (i === nextEmojiIndex) {
        emphasisHtml += phrase[i];
        inEmoji = true;

        if (i + 1 < phrase.length) {
          // eslint-disable-next-line max-len
          nextEmojiIndex = phrase.substring(i + 1).search(/(?![*#0-9]+)[\p{Emoji}\p{Emoji_Modifier}\p{Emoji_Component}\p{Emoji_Modifier_Base}\p{Emoji_Presentation}]/gu);

          if (nextEmojiIndex !== -1) {
            nextEmojiIndex += i + 1;
          }
        }

        // eslint-disable-next-line no-continue
        continue;
      }

      if (inEmoji) {
        if (WORD_BOUNDARIES.includes(phrase[i])) {
          inWord = false;
          inEmoji = false;
        }

        emphasisHtml += phrase[i];
        // eslint-disable-next-line no-continue
        continue;
      }

      // include HTML in the final phrase but omit from the words/letter emphasis
      if (phrase[i] === '<') {
        emphasisHtml += phrase[i];
        inHtml = true;

        // eslint-disable-next-line no-continue
        continue;
      }

      if (inHtml) {
        if (phrase[i] === '>') {
          inWord = false;
          inHtml = false;
        }

        emphasisHtml += phrase[i];
        // eslint-disable-next-line no-continue
        continue;
      }

      // This isn't emoji and this isn't HTML, so we should be able to decorate it into words and letters.
      // We won't allow words to start with ".", as this may be the first thing we see after closing HTML or emoji.
      if (!inWord && !WORD_BOUNDARIES.includes(phrase[i]) && phrase[i] !== '.') {
        inWord = true;
        emphasisHtml += `<span class="word word_${wordIndex}">`;
      }

      if (WORD_BOUNDARIES.includes(phrase[i]) && inWord) {
        wordIndex += 1;
        inWord = false;
        emphasisHtml += '</span> ';
      } else if (inWord) {
        emphasisHtml += `<span class="letter letter_${i}">${phrase[i]}</span>`;
      } else {
        emphasisHtml += phrase[i];
      }
    }

    const theContent = content;
    theContent.innerHTML = emphasisHtml;
  }

  /* options {
    content:           jQuery paragraph
    contentOffset:     when highlighting a single word, this is how many letters deep we are
    wordOffset:        when highlighting a single word, this is how many words deep we are
    highlightWords:    whether to highlight/emphasize words when reading the paragraph.
    singleWord:        when reading a single word (affects highlight words behavior)
    sayTheLetters:     whether to read and highlight/emphasize letters after reading the paragraph or word.
    }
    */
  sayTheText(text: string, options: UtterOptions) {
    synth.cancel();
    const adjustedText = Narrator.adjustPronunciation(text);
    const utterThis = new SpeechSynthesisUtterance(adjustedText);
    utterThis.rate = this.utterRate;

    if (options.content && options.highlightWords) {
      const contentElement = options.content;
      let wordIndex = options.wordOffset ?? 0;

      // Note: this event doesn't work on Chrome Android :( https://bugs.chromium.org/p/chromium/issues/detail?id=521666
      // The TTS works fine, but no visual emphasis will occur.
      utterThis.onboundary = (e) => {
        // ignore sentence boundaries; we get a word boundary event anyway for the next word.
        if (e.name === 'sentence') {
          return;
        }

        const word = contentElement.querySelector(`.word_${wordIndex}`) as HTMLElement;
        wordIndex += 1;
        if (word) {
          contentElement.scrollTop = word.offsetTop;
          word.animate([{ fontWeight: 'bold', textDecoration: 'underline dotted' },
            { fontWeight: 'normal', textDecoration: 'none' }], EMPHASIS_REMOVE_DELAY_MS);
        }
      };
    }

    synth.speak(utterThis);

    if (!options.sayTheLetters) {
      return;
    }

    for (let i = 0; i < text.length; i += 1) {
      const letter = text[i];
      const utterThisLetter = new SpeechSynthesisUtterance(letter);

      if (options.content) {
        const contentElement = options.content;
        const index = i + (options.contentOffset ?? 0);

        utterThisLetter.onstart = () => {
          contentElement.querySelector(`.letter_${index}`)?.classList.add('emphasis');
        };

        utterThisLetter.onend = () => {
          contentElement.querySelector(`.letter_${index}`)?.classList.remove('emphasis');
        };
      }

      synth.speak(utterThisLetter);
    }
  }

  makeClickToReadListener(targetElement: Element, text: string | null): EventListener {
    return (e: Event) => {
      const element = e.target as HTMLElement;

      if (element.matches('.letter') || element.matches('.word')) {
        const wordContent = element.closest('.word') as Element;
        const wordOffset = parseInt(wordContent.className.split(/\s+/).find((c: string) => c.startsWith('word_'))?.substring('word_'.length) ?? '-1', 10);
        const letterOffset = parseInt(wordContent.children[0].className.split(/\s+/).find((c: string) => c.startsWith('letter_'))?.substring('letter_'.length) ?? '-1', 10);

        if (wordOffset === -1 || letterOffset === -1 || !wordContent.textContent) {
          return;
        }

        this.sayTheText(wordContent.textContent, {
          content: wordContent.parentElement ?? undefined,
          contentOffset: letterOffset,
          wordOffset,
          highlightWords: true,
          singleWord: true,
          sayTheLetters: true,
        });

        e.preventDefault();
        e.stopImmediatePropagation();
      } else if (text) {
        this.sayTheText(text, {
          content: targetElement,
          contentOffset: 0,
          wordOffset: 0,
          highlightWords: true,
          singleWord: false,
          sayTheLetters: false,
        });

        e.preventDefault();
        e.stopImmediatePropagation();
      }
    };
  }

  // Instead of queuing up clicks on the message box to trigger reads, just keep the last attempt queued.
  // This should reduce interrupting/duplicate attempts for TTS to 'say' what's in the message box.
  // Note that if the callback activated and we activate another while TTS is occurring, it will still interrupt. It's not clear what the best solution is in that scenario, since
  // we don't want to narrate every action the player is taking if they are taking too many actions at once.
  lastMessageTimedCallback: number | undefined;

  clickToReadListeners: Map<HTMLElement, EventListenerOrEventListenerObject> = new Map();

  // Debounce -- if we execute multiple actions inbetween the delay time, we shouldn't queue up more than one read attempt.
  scheduledClickCount = 0;

  clickCount = 0;

  configureNarratorBox(message: string, timeToWaitMillis?: number) {
    this.configureMessageBox(this.narratorBox, message, () => {
      if (this.clickCount < this.scheduledClickCount && this.narratorBox.style.visibility === 'visible') {
        this.narratorBox.click();
        this.clickCount = this.scheduledClickCount;
      }
    }, timeToWaitMillis ?? 2500);
  }

  configureMessageBox(targetMessageBox: HTMLElement, message: string, callback?: () => void, timeToWaitMillis?: number) {
    if (targetMessageBox.textContent === message) {
      this.scheduledClickCount += 1;
      if (callback && timeToWaitMillis) {
        delay(timeToWaitMillis).then(callback);
      } else if (callback) {
        callback();
      }

      return;
    }

    // We will support getting <p> directly or getting a wrapper around it (e.g. <div>)
    const targetElement = targetMessageBox;
    let targetPara: HTMLElement;
    if (targetMessageBox.nodeName === 'P') {
      targetPara = targetElement;
    } else {
      targetPara = targetMessageBox.querySelector('p') as HTMLElement;
    }

    while (targetPara.firstChild) {
      targetPara.removeChild(targetPara.firstChild);
    }

    if (!message || message.length === 0) {
      targetPara.textContent = '';
      targetElement.style.visibility = 'hidden';
      return;
    }

    targetPara.textContent = message;
    targetElement.style.visibility = 'visible';
    Narrator.addEmphasis(targetPara);

    let clickToReadListener = this.clickToReadListeners.get(targetPara);
    if (clickToReadListener) {
      targetPara.removeEventListener('click', clickToReadListener);
    }

    clickToReadListener = this.makeClickToReadListener(targetElement, targetPara.textContent);
    targetPara.addEventListener('click', clickToReadListener);
    this.clickToReadListeners.set(targetPara, clickToReadListener);

    this.scheduledClickCount += 1;
    if (callback && timeToWaitMillis) {
      delay(timeToWaitMillis).then(callback);
    } else if (callback) {
      callback();
    }
  }

  // Assuming text has already been configured, sets it up for text-to-speech with word-specific and whole text reading.
  configureToRead(parentElement: Element | Document, paraSelector: string) {
    parentElement.querySelectorAll(paraSelector).forEach((contentp) => {
      Narrator.addEmphasis(contentp as HTMLElement);
      const textToRead = contentp.textContent?.replace(/(?![*#0-9]+)[\p{Emoji}\p{Emoji_Modifier}\p{Emoji_Component}\p{Emoji_Modifier_Base}\p{Emoji_Presentation}]/gu, '') ?? '';

      contentp.addEventListener('click', this.makeClickToReadListener(contentp, textToRead));
    });
  }

  // Sets the text on provided elements and then sets them up for text-to-speech with word-specific and whole text reading.
  configureForReading(parentElement: Element | Document, html: string, contentSelector: string, paraSelector: string) {
    parentElement.querySelectorAll(contentSelector).forEach((content: Element) => {
      const theContent = content;
      theContent.innerHTML = html;
      this.configureToRead(content, paraSelector);
    });
  }

  static toSnakeCase(str: string) {
    return str.replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase();
  }

  // woodChopSound => wood_chop-(some vite asset id).mp3. as long as we follow this pattern, sounds will be automatically detected and added to the app the first time they need to play, and recycled thereafter.
  static playSound(id: string) {
    sounds = sounds || import.meta.glob<string>('./assets/sounds/*.mp3', { eager: false, as: 'url' });

    const soundFromId = Narrator.toSnakeCase(id.replace('Sound', '')); // `./assets/sounds/...mp3`;
    Object.entries(sounds).filter(([path]) => path.includes(soundFromId)).forEach(([, sound]) => {
      sound().then((url: string) => {
        let audioEle = (document.getElementById(id) as HTMLAudioElement) ?? undefined;
        if (!audioEle) {
          audioEle = new Audio(url);
          audioEle.id = id;
          audioEle.addEventListener('play', (e) => {
            setTimeout(() => {
              (e.target as HTMLAudioElement).pause();
            }, 3000);
          }, false);
          document.body.appendChild(audioEle);
        }

        audioEle.play().catch(() => {
          // Couldn't find the sound to play it.
        });
      }, () => {});
    });
  }

  narrateFromAction(
    gameState: GameState,
    interactSound: string,
    targetObj?: GameObject | string[],
    drops?: Map<string, number>,
  ) {
    Narrator.playSound(interactSound);

    let targetName = '';

    if (Array.isArray(targetObj)) {
      targetName = LIST_FORMATTER.format(targetObj);
    } else if (targetObj && targetObj.definition.displayName) {
      targetName = targetObj.definition.displayName;
    } else if (targetObj && targetObj.ser.name) {
      targetName = targetObj.ser.name;
    }

    let message = '';
    if (interactSound === 'woodChopSound') {
      const targetStartsWithVowel = Narrator.VOWELS.includes(targetName[0].toLowerCase());

      message = `${gameState.savable.playerName} chopped ${targetStartsWithVowel ? 'an' : 'a'} ${targetName} with an axe.`;
    } else if (interactSound === 'stoneMineSound') {
      message = `${gameState.savable.playerName} mined in a mountain with a pickaxe.`;
    } else if (interactSound === 'shovelingSound') {
      message = `${gameState.savable.playerName} dug in a desert with a shovel.`;
    } else if (interactSound === 'waterFillSound') {
      message = `${gameState.savable.playerName} filled up a bucket with water from a ${targetName}.`;
    } else if (interactSound === 'milkFillSound') {
      message = `${gameState.savable.playerName} filled up a bucket with milk from a ${targetName}.`;
    } else if (interactSound === 'shearSheepSound') {
      message = `${gameState.savable.playerName} sheared a sheep with scissors.`;
    } else if (interactSound === 'waterPourSound') {
      message = `${gameState.savable.playerName} poured out a bucket of water on ${targetName}.`;
    } else if (interactSound === 'groundRakeSound') {
      message = `${gameState.savable.playerName} cultivated ${targetName} with a gardening hoe.`;
    } else if (interactSound === 'trashItemSound') {
      message = `${gameState.savable.playerName} threw 1 ${targetName} in the trash.`;
    } else if (interactSound === 'itemGiveSound') {
      message = `${gameState.savable.playerName} gave 1 ${targetName} to a villager.`;
    } else if (interactSound === 'ovenLoadSound') {
      message = `${gameState.savable.playerName} put 1 ${targetName} in an oven.`;
    } else if (interactSound === 'blenderLoadSound') {
      message = `${gameState.savable.playerName} put 1 ${targetName} in a blender.`;
    } else if (interactSound === 'brewerLoadSound') {
      message = `${gameState.savable.playerName} put 1 ${targetName} in a brewer.`;
    } else if (interactSound === 'storeItemSound') {
      message = `${gameState.savable.playerName} put 1 ${targetName} in the backpack.`;
    } else if (interactSound === 'removeItemSound') {
      message = `${gameState.savable.playerName} removed 1 ${targetName} from the backpack.`;
    } else if (interactSound === 'knifeSwingSound') {
    // for now, these interactions are one-time (enemies don't require more than one 'swing' or arrow shot. This may change later.
      const targetStartsWithVowel = Narrator.VOWELS.includes(targetName[0].toLowerCase());

      message = `${gameState.savable.playerName} swung a sword at ${targetStartsWithVowel ? 'an' : 'a'} ${targetName}. The ${targetName} was defeated.`;
    } else if (interactSound === 'bowShotSound') {
      const targetStartsWithVowel = Narrator.VOWELS.includes(targetName[0].toLowerCase());

      message = `${gameState.savable.playerName} fired an arrow from a bow at ${targetStartsWithVowel ? 'an' : 'a'} ${targetName}. The ${targetName} was defeated.`;
    } else if (interactSound === 'fishingSound') {
      message = `${gameState.savable.playerName} went fishing.`;
    } else if (interactSound === 'appleEatSound' || interactSound === 'toastEatSound' || interactSound === 'eatingSound') {
      const targetStartsWithVowel = Narrator.VOWELS.includes(targetName[0].toLowerCase());

      message = `${gameState.savable.playerName} ate ${targetStartsWithVowel ? 'an' : 'a'} ${targetName}.`;
    } else if (interactSound === 'drinkingSound') {
      const targetStartsWithVowel = Narrator.VOWELS.includes(targetName[0].toLowerCase());

      message = `${gameState.savable.playerName} drank ${targetStartsWithVowel ? 'an' : 'a'} ${targetName}.`;
    } else if (interactSound === 'ovenCookingSound') {
      const targetStartsWithVowel = Narrator.VOWELS.includes(targetName[0].toLowerCase());

      message = `${gameState.savable.playerName} made ${targetStartsWithVowel ? 'an' : 'a'} ${targetName} in an oven.`;
    } else if (interactSound === 'blenderBlendingSound') {
      message = `${gameState.savable.playerName} blended ${targetName} in a blender.`;
    } else if (interactSound === 'brewerBrewingSound') {
      message = `${gameState.savable.playerName} brewed ${targetName} in a brewer.`;
    } else if (interactSound === 'wandWishSound') {
      const targetStartsWithVowel = Narrator.VOWELS.includes(targetName[0].toLowerCase());
      message = `${gameState.savable.playerName} wished for ${targetStartsWithVowel ? 'an' : 'a'} ${targetName}.`;
    } else if (interactSound === 'itemPickupSound') {
      const targetStartsWithVowel = Narrator.VOWELS.includes(targetName[0].toLowerCase());
      message = `${gameState.savable.playerName} picked up ${targetStartsWithVowel ? 'an' : 'a'} ${targetName}.`;
    } else if (interactSound === 'paintingSound') {
      const targetStartsWithVowel = Narrator.VOWELS.includes(targetName[0].toLowerCase());
      message = `${gameState.savable.playerName} started painting ${targetStartsWithVowel ? 'an' : 'a'} ${targetName}.`;
    }

    if (drops) {
      drops.forEach((amount, key) => {
        if (amount <= 0) {
          return;
        }

        let verb;
        if (interactSound === 'waterPourSound' || interactSound === 'groundRakeSound') {
          verb = 'grew';
        } else if (interactSound === 'fishingSound') {
          verb = 'caught';
        } else if (interactSound === 'wandWishSound' || interactSound === 'itemPickupSound') {
          verb = 'obtained';
        } else {
          verb = 'found';
        }

        let description;
        if (amount > 1 && gameState.objectDictionary.get(key)?.plural) {
          description = gameState.objectDictionary.get(key)?.plural ?? '';
        } else {
          description = key;
        }
        message += ` ${gameState.savable.playerName} ${verb} ${amount} ${description}.`;
      });
    }

    this.configureMessageBox(this.narratorBox, message, () => {
      if (this.narratorBox.style.visibility === 'visible') {
        this.narratorBox.click();
      }
    }, 2500);
  }
}

export default Narrator;
