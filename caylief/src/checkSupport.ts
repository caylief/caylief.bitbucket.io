// Kudos to https://github.com/nicoalbanese/ai-sdk-chrome-ai/blob/main/lib/utils.ts
export default async function checkEnv() {
  function getChromeVersion() {
    const raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);
    return raw ? parseInt(raw[2], 10) : 0;
  }

  const version = getChromeVersion();
  if (version < 127) {
    throw new Error(
      'If you want the LLM-supplemented experience, your browser is not supported. Please update to Chrome 127 version or greater: https://www.google.com/chrome/dev/?extra=devchannel',
    );
  }

  if (!('ai' in globalThis)) {
    throw new Error(
      'If you want the LLM-supplemented experience, the Prompt API is not available, check chrome://flags/#prompt-api-for-gemini-nano is set to Enabled',
    );
  }

  const state = await ai?.canCreateGenericSession();
  if (state !== 'readily') {
    throw new Error(
      'Built-in LLM (Gemini Nano) is not ready, check chrome://flags/#optimization-guide-on-device-model is set to Enabled BypassPrefRequirement and make sure chrome://components has Optimization Guide On Device Model listed as "Up-to-date"',
    );
  }
}
