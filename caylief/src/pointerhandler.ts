/* eslint-disable @typescript-eslint/no-floating-promises */
import { GameObject } from './models/gameobject';
import GameState from './models/gamestate';
import Puzzle from './models/puzzles/puzzle';
import ObjectSpawner from './objectspawner';
import Narrator from './narrator';
import { CountType } from './models/savablegamestate';
import WandHandler from './wandhandler';

class PointerHandler {
  mouseIsDown = false;

  mouseX = 0;

  mouseY = 0;

  lastX = 0;

  lastY = 0;

  activeConversation: GameObject | undefined;

  conversationProgression = 0;

  talkingToSomeone: GameObject | undefined;

  readonly gameState: GameState;

  readonly objectSpawner: ObjectSpawner;

  readonly narrator: Narrator;

  readonly wandHandler: WandHandler;

  readonly talkMessageBox: HTMLElement;

  readonly canvasFront: HTMLCanvasElement;

  readonly randomizerInt: (max: number) => number;

  readonly pointInObjectPathChecker: (object: GameObject, x: number, y: number) => boolean;

  readonly onActionTaken: () => void;

  readonly activatePuzzle: (puzzle: Puzzle<unknown>) => void;

  readonly backpackIsClosed: () => boolean;

  readonly activateBackpack: () => void;

  readonly activatePiano: () => void;

  readonly activateShop: (shopObj: GameObject) => void;

  readonly activatePainting: (paintingObj: GameObject) => void;

  readonly trashPaintable: (paintingObj: GameObject) => void;

  constructor(
    gameState: GameState,
    objectSpawner: ObjectSpawner,
    narrator: Narrator,
    wandHandler: WandHandler,
    talkMessageBox: HTMLElement,
    canvasFront: HTMLCanvasElement,
    randomizerInt: (max: number) => number,
    pointInObjectPathChecker: (object: GameObject, x: number, y: number) => boolean,
    onActionTaken: () => void,
    activatePuzzle: (puzzle: Puzzle<unknown>) => void,
    backpackIsClosed: () => boolean,
    activateBackpack: () => void,
    activatePiano: () => void,
    activateStore: (shopObj: GameObject) => void,
    activatePainting: (paintingObj: GameObject) => void,
    trashPainting: (paintingObj: GameObject) => void,
  ) {
    this.gameState = gameState;
    this.objectSpawner = objectSpawner;
    this.narrator = narrator;
    this.wandHandler = wandHandler;
    this.talkMessageBox = talkMessageBox;
    this.canvasFront = canvasFront;
    this.randomizerInt = randomizerInt;
    this.pointInObjectPathChecker = pointInObjectPathChecker;
    this.onActionTaken = onActionTaken;
    this.activatePuzzle = activatePuzzle;
    this.backpackIsClosed = backpackIsClosed;
    this.activateBackpack = activateBackpack;
    this.activatePiano = activatePiano;
    this.activateShop = activateStore;
    this.activatePainting = activatePainting;
    this.trashPaintable = trashPainting;
  }

  handlePointerDown(e: PointerEvent) {
    this.mouseX = e.offsetX;
    this.mouseY = e.offsetY;

    this.mouseIsDown = true;

    this.talkingToSomeone = undefined;

    // Check for interactions with people.
    this.gameState.savable.objects.filter((obj: GameObject) => obj.properties.isPerson
    && this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)).forEach((obj: GameObject) => {
      if (this.gameState.grabbedMovable) {
        return;
      }

      if (this.mouseIsDown) {
        this.talkingToSomeone = obj;
      }
    });

    this.lastX = this.mouseX;
    this.lastY = this.mouseY;
  }

  handleContextMenu(e: MouseEvent) {
    this.mouseX = e.offsetX;
    this.mouseY = e.offsetY;

    this.lastX = this.mouseX;
    this.lastY = this.mouseY;

    let tookAction = false;

    // Activate backpack if we clicked on it.
    this.gameState.savable.objects.filter((obj: GameObject) => !this.gameState.grabbedMovable && obj.ser.name === 'backpack'
    && this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)).forEach(() => {
      this.activateBackpack();
      tookAction = true;
    });

    if (tookAction) {
      e.preventDefault();
      return false;
    }

    this.gameState.savable.objects.filter((obj: GameObject) => !this.gameState.grabbedMovable && obj.ser.name === 'magicwand' && this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)).forEach(() => {
      this.wandHandler.activate(this.lastX, this.lastY);
      tookAction = true;
    });

    if (tookAction) {
      e.preventDefault();
      return false;
    }

    // Activate piano if we clicked on it.
    this.gameState.savable.objects.filter((obj: GameObject) => !this.gameState.grabbedMovable && obj.ser.name === 'piano'
    && this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)).forEach(() => {
      this.activatePiano();
      tookAction = true;
    });

    if (tookAction) {
      e.preventDefault();
      return false;
    }

    // Activate store if we clicked on it.
    this.gameState.savable.objects.filter((obj: GameObject) => !this.gameState.grabbedMovable && obj.properties.isShop
    && this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)).forEach((obj: GameObject) => {
      this.activateShop(obj);
      tookAction = true;
    });

    if (tookAction) {
      e.preventDefault();
      return false;
    }

    // Start updating painting if we clicked on it.
    this.gameState.savable.objects.filter((obj: GameObject) => !this.gameState.grabbedMovable && obj.ser.name === 'painting'
    && this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)).forEach((obj: GameObject) => {
      this.activatePainting(obj);
      tookAction = true;
    });

    if (tookAction) {
      e.preventDefault();
      return false;
    }

    // Eat something edible if we clicked on it.
    // Only eat one thing per click, if they're all stacked up.
    let justAte = false;
    this.gameState.savable.objects.forEach((obj: GameObject, i, objs) => {
      if (justAte || !this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)) {
        return;
      }

      if (obj.properties.isEdible) {
        justAte = true;

        if (obj.definition.eatSound) {
          this.narrator.narrateFromAction(this.gameState, obj.definition.eatSound, obj);
        }

        this.gameState.savable.removeCount(CountType.Objects, obj.ser.name);

        // Because we're only looking for one object to remove, it's OK that we don't completely iterate the objects list after this occurs.
        objs.splice(i, 1);

        this.onActionTaken();
      }
    });

    if (justAte || this.gameState.grabbedMovable) {
      e.preventDefault();
      return false;
    }

    // Note: run this after checking for eating because it's currently the same action -- we'd otherwise create an item and then eat it at once.
    // Activate oven if we clicked on it, and it has a recipe to execute
    this.gameState.savable.objects.filter((obj: GameObject) => obj.definition.isCookingImplement
    && this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)).forEach((obj: GameObject) => {
      // If needing and missing fuel, skip
      if (obj.definition.fuelRequired && obj.definition.fuelRequired?.length > 0
            && !obj.definition.fuelRequired?.some((fuel) => obj.ser.holding.includes(fuel))) {
        return;
      }

      // Check all recipes we can create...
      const matchedRecipes: Map<string, string[]> = new Map();
      this.gameState.objectRecipes.forEach((v, k) => {
        if (v.recipe.every((val) => obj.ser.holding.includes(val)) && v.madeIn.includes(obj.ser.name)) {
          matchedRecipes.set(k, v.recipe);
        }
      });

      if (matchedRecipes.size > 0) {
        // Pick the most complex one for now (may allow choice later).
        const mostComplexRecipe = [...matchedRecipes].sort(([, recipea], [, recipeb]) => recipeb.length - recipea.length)[0];
        const objectName = mostComplexRecipe[0];

        const cookedObject = this.objectSpawner.makeObject(objectName, obj.ser.x, obj.ser.y);

        this.gameState.savable.cookedCounts.set(objectName, (this.gameState.savable.cookedCounts.get(objectName) ?? 0) + 1);

        if (!this.gameState.savable.discoveredRecipes.cooking.includes(objectName)) {
          this.gameState.savable.discoveredRecipes.cooking.push(objectName);
        }

        obj.removeHolding((ingredient) => !mostComplexRecipe[1].includes(ingredient)
        && !obj.definition.fuelRequired?.includes(ingredient));

        if (obj.definition.cookingSound) {
          this.narrator.narrateFromAction(this.gameState, obj.definition.cookingSound, cookedObject);
        }

        this.onActionTaken();
      }
    });

    e.preventDefault();
    return false;
  }

  handlePointerUp(e: PointerEvent) {
    this.mouseX = e.offsetX;
    this.mouseY = e.offsetY;

    if (this.talkingToSomeone) {
      // progress conversation because we're talking to the same person again.
      if (this.activeConversation === this.talkingToSomeone) {
        this.conversationProgression += 1;
      } else {
        // otherwise, start new one
        this.activeConversation = this.talkingToSomeone;
        this.conversationProgression = 0;
      }

      let message;
      if (this.conversationProgression === 0) {
        const personIntro = this.activeConversation.ser.givenName ? `My name is ${this.activeConversation.ser.givenName}. ` : '';
        if (this.gameState.savable.playerName === 'You') {
          message = `Hello! ${personIntro}How are you today?`;
        } else {
          message = `Hello, ${this.gameState.savable.playerName}! ${personIntro}How are you today?`;
        }

        if (this.activeConversation.ser.quests.length > 0) {
          message += ' I could use your help! (Talk to me again to know more)';
        }
      } else if (this.activeConversation.ser.quests.length === 0) {
        message = 'I am doing fine, thanks.';
      } else {
        const theQuest = this.activeConversation.ser.quests[0];

        // initialize the quest
        if (!theQuest.state) {
          [message] = theQuest.dialogueStart;

          // if there is defeated done criteria, initialize based on existing defeat counts -- defeats that occurred before the quest was given do not count.
          // (however, if multiple copies of the quest exist at the moment with different people and are moved to given state, defeats are shared across)
          if (theQuest.doneCriteria.defeated && theQuest.doneCriteria.defeated.size > 0) {
            theQuest.doneCriteria.defeated.forEach((count, enemyName, map) => {
              map.set(enemyName, count + (this.gameState.savable.defeatedCounts.get(enemyName) ?? 0));
            });
          }

          theQuest.state = 'given';
        } else if (theQuest.state === 'given') {
          let doneQuest = true;

          if (theQuest.doneCriteria.bring && theQuest.doneCriteria.bring.size > 0) {
            theQuest.doneCriteria.bring.forEach((count) => {
              if (count > 0) {
                doneQuest = false;
              }
            });
          }

          if (theQuest.doneCriteria.defeated && theQuest.doneCriteria.defeated.size > 0) {
            const questTemplate = this.objectSpawner.questTemplates.filter((q) => q.name === theQuest.name)[0];
            theQuest.doneCriteria.defeated.forEach((countToReach, enemyName, map) => {
              const defeatedEnemies = this.gameState.savable.defeatedCounts.get(enemyName) ?? 0;
              const totalToDefeat = questTemplate.doneCriteria.defeated?.get(enemyName) ?? 0;

              if (countToReach - defeatedEnemies > totalToDefeat) {
                console.warn('Something went wrong and the quest is asking for more enemies to be defeated. Resetting to the max.');

                map.set(enemyName, defeatedEnemies + totalToDefeat);
              }

              if (defeatedEnemies < countToReach) {
                doneQuest = false;
              }
            });
          }

          if (theQuest.doneCriteria.puzzle) {
            const { puzzle } = theQuest.doneCriteria;
            doneQuest = puzzle.isDone ?? false;

            if (!doneQuest) {
              this.activatePuzzle(puzzle);
            }
          }

          if (doneQuest) {
            message = theQuest.dialogueComplete;

            // for now quests won't have RNG drops (will be 100% chance), but we'll roll anyway in case this changes later.
            this.objectSpawner.rollDrops(this.activeConversation.ser.x, this.activeConversation.ser.y, theQuest.drops);

            this.gameState.savable.addCount(CountType.Quests, theQuest.name);
            this.activeConversation.ser.quests.pop();

            // we'll skip narration for this, as the quest-giver has their own message that is spoken next.

            this.onActionTaken();
          } else {
            // keep replaying the quest dialogue describing how to complete the quest.
            [message] = theQuest.dialogueStart;
          }
        }
      }

      if (message) {
        this.talkMessageBox.style.left = `${this.activeConversation.ser.x - 40 + (this.gameState.savable.globalXOffset % this.canvasFront.width)}px`;
        this.talkMessageBox.style.top = `${this.activeConversation.ser.y - 40 + (this.gameState.savable.globalYOffset % this.canvasFront.height)}px`;
        this.narrator.configureMessageBox(this.talkMessageBox, message, () => {
          if (this.talkMessageBox.style.visibility === 'visible') {
            this.talkMessageBox.querySelector('p')?.click();
          }
        }, 500);
      }

      this.talkingToSomeone = undefined;
    }

    let gaveToVillager = false;

    this.gameState.savable.objects.filter((obj: GameObject) => obj.properties.isPerson && this.gameState.grabbedMovable
        && obj.ser.quests && obj.ser.quests.length > 0
        && this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)).forEach((obj: GameObject) => {
      const [theQuest] = obj.ser.quests;

      if (theQuest.state === 'given' && theQuest.doneCriteria.bring) {
        theQuest.doneCriteria.bring.forEach((count, objName, map) => {
          if (objName === this.gameState.grabbedMovable?.ser.name && count > 0) {
            map.set(objName, count - 1);

            gaveToVillager = true;
          }
        });
      }
    });

    let changedState = false;
    let tookVoicedAction = false;
    let interactSound: string | undefined;
    let actionTargetObj: GameObject | string[] | undefined;
    let actualDrops: Map<string, number> = new Map();
    const atBackpackWhenNotOpen = this.gameState.savable.objects.filter((obj: GameObject) => obj.ser.name === 'backpack' && this.backpackIsClosed() && this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)).length > 0;
    const atTrashcan = this.gameState.savable.objects.filter((obj: GameObject) => obj.ser.name === 'trashcan' && this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)).length > 0;
    const [atCookerWithCompatibleObj] = this.gameState.savable.objects.filter((obj: GameObject) => obj.definition.isCookingImplement
          && this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY) && this.gameState.grabbedMovable
          && (this.gameState.grabbedMovable.definition.isIngredient
            || this.gameState.grabbedMovable.ser.holding.some((h) => this.gameState.objectDictionary.get(h)?.isIngredient)))
      .filter((obj: GameObject) => {
        // eslint ideally would infer this from the filter above, but it doesn't.
        if (!this.gameState.grabbedMovable) {
          return false;
        }

        // If this is fuel the cooking implement needs to run (and it doesn't already have the ingredient), it's compatible.
        if (obj.definition.fuelRequired?.includes(this.gameState.grabbedMovable.ser.name)) {
          return true;
        }

        // This is a compatible ingredient, but the cooking implement already has it.
        if (obj.ser.holding.includes(this.gameState.grabbedMovable.ser.name)
            || (this.gameState.grabbedMovable.ser.holding.length > 0
                && this.gameState.grabbedMovable.ser.holding.every((h) => obj.ser.holding.includes(h)))) {
          return false;
        }

        // otherwise only allow the ingredient to be put in if there's a recipe compatible with it.
        let matchedAnyRecipe = false;
        this.gameState.objectRecipes.forEach((v) => {
          // eslint ideally would infer this from the filter above, but it doesn't.
          if (!this.gameState.grabbedMovable) {
            return;
          }

          if ((v.recipe.includes(this.gameState.grabbedMovable.ser.name)
              || v.recipe.some((i) => this.gameState.grabbedMovable?.ser.holding.includes(i))) && v.madeIn.includes(obj.ser.name)) {
            matchedAnyRecipe = true;
          }
        });

        return matchedAnyRecipe;
      }) as [GameObject];

    // Check all interactions with tools
    this.gameState.savable.objects.forEach((obj: GameObject, i, objs) => {
      // if we grabbed a movable and took it to a backpack, we'll put in. Unless it's a tool (for now).
      // if we grabbed a movable and took it to a trashcan, we'll throw it out. Unless it's a tool (for now).
      // if we grabbed a movable and took it to a compatible cooking implement, we'll put it in.
      // If we gave it to villager, this is when we'll delete it.
      if (this.gameState.grabbedMovable && this.gameState.grabbedMovable === obj
            && (atBackpackWhenNotOpen || atTrashcan || atCookerWithCompatibleObj || gaveToVillager)) {
        interactSound = '';
        if (atBackpackWhenNotOpen && !this.gameState.grabbedMovable.properties.isTool) {
          actionTargetObj = this.gameState.grabbedMovable;
          interactSound = 'storeItemSound';
        } else if (atTrashcan) {
          if (this.gameState.grabbedMovable.properties.isTool && this.gameState.grabbedMovable.ser.holding.length > 0) {
            actionTargetObj = this.gameState.grabbedMovable.ser.holding;
            this.gameState.grabbedMovable.ser.holding = [];
            interactSound = 'trashItemSound';
          } else if (!this.gameState.grabbedMovable.properties.isTool) {
            actionTargetObj = this.gameState.grabbedMovable;
            interactSound = 'trashItemSound';
          }
        } else if (atCookerWithCompatibleObj) {
          if (this.gameState.grabbedMovable.properties.isTool && this.gameState.grabbedMovable.ser.holding) {
            const movedIngredients: string[] = [];
            this.gameState.grabbedMovable.ser.holding = this.gameState.grabbedMovable.ser.holding.filter((h) => {
              // shift what the grabbed movable is holding into the cooking implement (e.g. water, milk)
              if (!atCookerWithCompatibleObj.ser.holding.includes(h)) {
                atCookerWithCompatibleObj.ser.holding.push(h);
                movedIngredients.push(h);

                return false;
              }

              return true;
            });

            if (movedIngredients.length > 0) {
              actionTargetObj = movedIngredients;
              interactSound = atCookerWithCompatibleObj.definition.loadingSound;
            }
          } else if (this.gameState.grabbedMovable.definition.isIngredient) {
            actionTargetObj = this.gameState.grabbedMovable;
            atCookerWithCompatibleObj.ser.holding.push(this.gameState.grabbedMovable.ser.name);
            interactSound = atCookerWithCompatibleObj.definition.loadingSound;
          }
        } else if (gaveToVillager) {
          actionTargetObj = this.gameState.grabbedMovable;
          interactSound = 'itemGiveSound';
        }

        // if no interaction was taken, skip over the rest.
        if (!actionTargetObj) {
          return;
        }

        // we're continuing the interaction with the grabbed item (not something it was holding)
        if (actionTargetObj === this.gameState.grabbedMovable) {
          this.gameState.savable.removeCount(CountType.Objects, obj.ser.name);

          // TODO: need to figure out a better coupling here. This is trying to clean up after ourselves -- if a painting has an associated canvas for editing and it is then trashed, we should clear it.
          // this helps cut down on the local storage space taken up by Caylief.
          if (obj.ser.customState.paintingImageUrl) {
            if (atBackpackWhenNotOpen) {
              this.gameState.savable.backpack.statefulObjects.push(actionTargetObj.ser);
            } else if (atTrashcan) {
              this.trashPaintable(obj);
            }
          } else if (atBackpackWhenNotOpen) {
            this.gameState.savable.backpack.add(obj.ser.name, 1);
          }

          // Because we have only one grabbed movable that we're removing at this point, it's OK that we don't completely iterate the objects list after this occurs.
          objs.splice(i, 1);
        }

        changedState = true;
        tookVoicedAction = true;
        return;
      }

      // Don't look at any further interactions unless we have a grabbed movable and we're interacting with an object with it.
      if (!this.gameState.grabbedMovable || !this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)) {
        return;
      }

      // For now, only paintings and furniture will be repaintable with the paintbrush
      if (this.gameState.grabbedMovable.ser.name === 'paintbrush' && (obj.ser.name === 'painting' || obj.properties.isFurniture)) {
        this.activatePainting(obj);
        return;
      }

      // we have a grabbed tool, but it is incompatible with the object (cannot defeat or interact with it)
      if ((!obj.definition.defeatedBy || !obj.definition.defeatedBy.includes(this.gameState.grabbedMovable.ser.name))
          && (!obj.definition.tools || !obj.definition.tools.includes(this.gameState.grabbedMovable.ser.name))) {
        return;
      }

      // Defeating is analogous to consuming in this logic, but it feels wrong to tie it that way, so will keep it separate for now.
      if (obj.definition.defeatedBy && obj.definition.defeatedBy.includes(this.gameState.grabbedMovable.ser.name)) {
        if (this.gameState.grabbedMovable.definition.useSound) {
          interactSound = this.gameState.grabbedMovable.definition.useSound;
        }
        actionTargetObj = obj;

        this.gameState.savable.removeCount(CountType.Objects, obj.ser.name);
        this.gameState.savable.addCount(CountType.Defeated, obj.ser.name);

        // Because we're only looking for one object (enemy) to remove, it's OK that we don't completely iterate the objects list after this occurs.
        objs.splice(i, 1);

        changedState = true;
        tookVoicedAction = true;
      }

      // some resources don't have consumption limits, like ponds or dirt. This may change later.
      if (obj.definition.isConsumed) {
        this.gameState.savable.removeCount(CountType.Objects, obj.ser.name);

        // Because we're only looking for one object (consumed resource) to remove, it's OK that we don't completely iterate the resources list after this occurs.
        objs.splice(i, 1);

        if (obj.definition.consumeSound) {
          interactSound = obj.definition.consumeSound;
        }
        actionTargetObj = obj;

        changedState = true;
        tookVoicedAction = true;
      }

      // this is a resource that can fill a bucket and we've brought a bucket.
      if (obj.definition.provides && this.gameState.grabbedMovable.ser.name === 'bucket') {
        if (this.gameState.grabbedMovable.ser.holding.length === 0) {
          this.gameState.grabbedMovable.ser.holding = [obj.definition.provides];

          if (obj.definition.providesSound) {
            interactSound = obj.definition.providesSound;
          }
          actionTargetObj = obj;

          changedState = true;
          tookVoicedAction = true;
        }

        // Filling a bucket should avoid rolling drops from the pond, which we will use for fishing.
        return;
      }

      if (obj.definition.consumes) {
        // We brought a tool that is compatible with the object, but isn't holding what it needs.
        if (!this.gameState.grabbedMovable.ser.holding.includes(obj.definition.consumes)) {
          return;
        }

        if (obj.definition.consumeSound) {
          interactSound = obj.definition.consumeSound;
        }
        actionTargetObj = obj;

        this.gameState.grabbedMovable.removeHolding((h) => h !== obj.definition.consumes);
        changedState = true;
        tookVoicedAction = true;
      }

      if (obj.definition.drops) {
        // For fishing. TODO: consider if there's a better way to trigger this.
        if (!interactSound && obj.definition.consumeSound) {
          interactSound = obj.definition.consumeSound;
          actionTargetObj = obj;
          tookVoicedAction = true;
        }

        actualDrops = this.objectSpawner.rollDrops(
          obj.ser.x,
          obj.ser.y,
          new Map(obj.definition.drops.map((dropEntry) => [dropEntry.key, dropEntry.value])),
        );

        if (actualDrops.size > 0) {
          changedState = true;
          tookVoicedAction = true;
        }
      }
    });

    if (tookVoicedAction && interactSound) {
      this.narrator.narrateFromAction(this.gameState, interactSound, actionTargetObj, actualDrops);
    }

    if (this.gameState.grabbedMovable) {
      this.gameState.grabbedMovable = undefined;
      changedState = true;
    }

    if (changedState) {
      this.onActionTaken();
    }
    this.mouseIsDown = false;
  }

  // Grab Priority is descending order -- higher should be picked first.
  static sortByGrabPriority(obja: GameObject, objb: GameObject) {
    const ascore = (obja.properties.isTool ? 3 : 0) + (obja.properties.isGrowable ? 1 : 0)
    + (obja.properties.isFurniture ? 1 : 0);
    const bscore = (objb.properties.isTool ? 3 : 0) + (objb.properties.isGrowable ? 1 : 0)
    + (objb.properties.isFurniture ? 1 : 0);

    return bscore - ascore;
  }

  handlePointerMove(e: PointerEvent) {
    this.mouseX = e.offsetX;
    this.mouseY = e.offsetY;

    this.canvasFront.style.cursor = 'default';

    let changedState = false;
    this.gameState.savable.objects.filter((obj: GameObject) => obj.properties.isMovable
    && (this.pointInObjectPathChecker(obj, this.lastX, this.lastY)
    || this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY) || (e.pointerType !== 'mouse' && this.gameState.grabbedMovable))).sort((a: GameObject, b: GameObject) => PointerHandler.sortByGrabPriority(a, b)).forEach((obj: GameObject) => {
      if (this.mouseIsDown) {
        if (!this.gameState.grabbedMovable || this.gameState.grabbedMovable === obj) {
          this.gameState.grabbedMovable = obj;
          this.gameState.grabbedMovable.moveObject(this.mouseX - this.lastX, this.mouseY - this.lastY, e.pointerType);
          changedState = true;
        }

        this.canvasFront.style.cursor = 'grabbing';
      } else {
        this.canvasFront.style.cursor = 'grab';
      }
    });

    let pickedUpItems: Map<string, number> | undefined;

    this.gameState.savable.objects = this.gameState.savable.objects.filter((obj: GameObject) => {
      // If this object can't be picked up by what we're holding, or the path doesn't match where we are, ignore.
      if (!this.pointInObjectPathChecker(obj, this.lastX, this.lastY)
      || !this.gameState.grabbedMovable?.definition.pickup
      || !this.gameState.grabbedMovable?.definition.pickup.includes(obj.ser.name)) {
        return true;
      }

      if (!pickedUpItems) {
        pickedUpItems = new Map<string, number>();
      }

      pickedUpItems.set(obj.ser.name, (pickedUpItems.get(obj.ser.name) ?? 0) + 1);

      this.gameState.savable.backpack.add(obj.ser.name, 1);
      changedState = true;
      return false;
    });

    if (pickedUpItems && pickedUpItems.size > 0) {
      Narrator.playSound('itemGiveSound');
      this.narrator.narrateFromAction(this.gameState, 'itemPickupSound', [...pickedUpItems.keys()], pickedUpItems);
    }

    this.gameState.savable.objects.filter((obj: GameObject) => !this.gameState.grabbedMovable && obj.properties.isPerson
          && this.pointInObjectPathChecker(obj, this.mouseX, this.mouseY)).forEach(() => {
      if (this.canvasFront.style.cursor !== 'grab' && this.canvasFront.style.cursor !== 'grabbing') {
        // When not able to grab something, change cursor when highlighting a person, to indicate they can be talked to.
        this.canvasFront.style.cursor = 'help';
      }
    });

    this.lastX = this.mouseX;
    this.lastY = this.mouseY;

    if (changedState) {
      this.onActionTaken();
    }
  }
}

export default PointerHandler;
