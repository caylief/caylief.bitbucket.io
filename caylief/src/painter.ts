import { GameObject } from './models/gameobject';
import GameObjectDefinition from './models/gameobjectdefinition';
import { EquipmentPiece } from './models/gameobjectstate';
import GameState from './models/gamestate';

// May be unnecessary but felt drawing the center and the cardinal directions was better, with the corners coming last.
const BG_COORDS = [
  // center
  [0, 0],

  // left of center
  [-1, 0],

  // right of center
  [1, 0],

  // top of center
  [0, -1],

  // bottom of center
  [0, 1],

  // corners
  [-1, -1],
  [-1, 1],
  [1, -1],
  [1, 1],
];

// Draw Priority is descending order -- higher should be picked first, so it can be drawn first.
const SORT_BY_DRAW_PRIORITY = (obja: GameObject, objb: GameObject) => objb.drawPriority - obja.drawPriority;

enum DrawType {
  Object,
  GivenName,
}

class Painter {
  readonly gameState: GameState;

  readonly canvas: HTMLCanvasElement;

  readonly ctx: CanvasRenderingContext2D;

  readonly mapCanvas: HTMLCanvasElement;

  readonly mapCtx: CanvasRenderingContext2D;

  readonly offscreenCanvas: OffscreenCanvas;

  readonly offscreenCtx: OffscreenCanvasRenderingContext2D;

  readonly emojisNeedFill: boolean;

  readonly cachedImages: Map<string, HTMLImageElement> = new Map();

  cachedBackgroundImage: HTMLImageElement | undefined;

  constructor(gameState: GameState, canvas: HTMLCanvasElement, ctx: CanvasRenderingContext2D, emojisNeedFill: boolean) {
    this.gameState = gameState;
    this.canvas = canvas;
    this.ctx = ctx;
    this.mapCanvas = document.querySelector('#map') as HTMLCanvasElement;
    this.mapCtx = this.mapCanvas.getContext('2d') as CanvasRenderingContext2D;
    this.emojisNeedFill = emojisNeedFill;

    this.offscreenCanvas = new OffscreenCanvas(50 * (window.devicePixelRatio / 1.5), 50 * (window.devicePixelRatio / 1.5));
    this.offscreenCtx = this.offscreenCanvas.getContext('2d') as OffscreenCanvasRenderingContext2D;
  }

  deleteFromImageCache(id: string) {
    this.cachedImages.delete(id);
  }

  clearImageCache() {
    this.cachedImages.clear();
  }

  drawBackground(backgroundImage?: HTMLImageElement) {
    this.ctx.save();
    this.ctx.globalAlpha = 1.0;
    this.ctx.fillStyle = '#2D600D';
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

    if (!backgroundImage || !backgroundImage.width) {
      this.ctx.restore();
      return;
    }

    this.ctx.globalAlpha = 0.4;

    // We're sliding around, with our perspective changing based on globalXOffset/globalYOffset skewing from 0,0
    // Thus, we should draw the background in tiles of backgroundImage.width x backgroundImage.height (to be seamless), while global offsetting accordingly.

    BG_COORDS.forEach(([xTile, yTile]) => {
      const xCoord = (xTile * backgroundImage.width) + (this.gameState.savable.globalXOffset % backgroundImage.width);
      const yCoord = (yTile * backgroundImage.height) + (this.gameState.savable.globalYOffset % backgroundImage.height);

      this.ctx.drawImage(backgroundImage, xCoord, yCoord);
    });

    this.ctx.globalAlpha = 1.0;
    this.ctx.restore();
  }

  // NOTE: Map is currently zoomed out to 10% scale. So a 3X scale up makes it 30% instead.
  static readonly MAP_TOOL_ICON_SCALE = 3.0;

  static readonly MAP_ICON_SCALE = 1.5;

  static readonly MAP_NAME_SCALE = 5.0;

  resolveScale(ctx: CanvasRenderingContext2D, object: GameObject, drawType: DrawType) {
    // drawing to map means we'll streamline how we draw some of this.
    const isMap = ctx === this.mapCtx;

    if (isMap && drawType === DrawType.GivenName) {
      return Painter.MAP_NAME_SCALE;
    }

    if (isMap && object.properties.isTool) {
      return Painter.MAP_TOOL_ICON_SCALE;
    }

    if (isMap) {
      return Painter.MAP_ICON_SCALE;
    }

    // 1.0 is default scale -- no change.
    return 1.0;
  }

  drawImage(ctx: CanvasRenderingContext2D, object: GameObject, src: string, shadowed?: boolean) {
    // drawing to map means we'll streamline how we draw some of this.
    const isMap = ctx === this.mapCtx;

    const cachedImage = this.cachedImages.get(object.ser.customState.id);

    const imageWidth = 50 * this.resolveScale(ctx, object, DrawType.Object) * (window.devicePixelRatio / 1.5);
    const imageHeight = imageWidth;

    if (cachedImage && cachedImage.getAttribute('src') === src) {
      ctx.globalAlpha = 1.0;

      if (shadowed) {
        ctx.shadowColor = '#000000';
        ctx.shadowBlur = 1;
        ctx.shadowOffsetX = (isMap ? 1 : 3);
        ctx.shadowOffsetY = (isMap ? 1 : 3);
      }

      ctx.drawImage(cachedImage, object.ser.x, object.ser.y, imageWidth, imageHeight);
      return;
    }

    const img = new window.Image();
    img.addEventListener('load', () => {
      ctx.globalAlpha = 1.0;

      if (shadowed) {
        ctx.shadowColor = '#000000';
        ctx.shadowBlur = 1;
        ctx.shadowOffsetX = (isMap ? 1 : 3);
        ctx.shadowOffsetY = (isMap ? 1 : 3);
      }

      ctx.drawImage(img, object.ser.x, object.ser.y, imageWidth, imageHeight);
    });
    img.setAttribute('src', src);
    this.cachedImages.set(object.ser.customState.id, img);
  }

  static drawTextRepresentation(
    someCtx: CanvasRenderingContext2D | OffscreenCanvasRenderingContext2D,
    textRepresentation: string | string[],
    x: number,
    y: number,
  ) {
    // Layer emojis if there are multiple ones (e.g. fish on frying pan to indicate it's cooked)
    if (textRepresentation.length > 1) {
      const textArr = [...textRepresentation];
      textArr.forEach((text) => {
        someCtx.fillText(text, x, y);
        someCtx.strokeText(text, x, y);
      });
    } else {
      someCtx.fillText(textRepresentation as string, x, y);
      someCtx.strokeText(textRepresentation as string, x, y);
    }
  }

  drawNameWhenPerson(ctx: CanvasRenderingContext2D, object: GameObject) {
    if (!object.properties.isPerson || !object.ser.givenName) {
      return;
    }
    // drawing to map means we'll streamline how we draw some of this.
    const isMap = ctx === this.mapCtx;

    const givenNameTextFontSize = (GameObjectDefinition.getTextFontSize(object.definition) / 2)
      * this.resolveScale(ctx, object, DrawType.GivenName);
    ctx.font = `bold ${givenNameTextFontSize}px Apple Color Emoji,Segoe UI Emoji,Noto Color Emoji,Android Emoji,EmojiSymbols,EmojiOne Mozilla,Twemoji Mozilla,Segoe UI Symbol,Noto Color Emoji Compat,emoji,noto-emojipedia-fallback`;

    ctx.shadowColor = 'black';
    ctx.shadowBlur = (isMap ? 5 : 12);
    ctx.strokeStyle = '#000000';
    ctx.fillText(object.ser.givenName, object.ser.x, object.ser.y);
    ctx.strokeText(object.ser.givenName, object.ser.x, object.ser.y);

    ctx.strokeStyle = '#FFFFFF';
    ctx.fillText(object.ser.givenName, object.ser.x, object.ser.y);
    ctx.strokeText(object.ser.givenName, object.ser.x, object.ser.y);
  }

  static readonly EQUIP_ADJ = new Map<string, number[]>(Object.entries({
    head: [-8, 15],
    rightHand: [-40, 80],
    leftHand: [40, 80],
    chest: [-8, 80],
    // legs look better at Xadj=0 when on Windows 10, hmm
    legs: [-8, 120],
    feet: [-8, 160],
  }));

  drawEquipmentPiece(ctx: CanvasRenderingContext2D, object: GameObject, equipment: EquipmentPiece, position: string) {
    const equipDefinition = this.gameState.objectDictionary.get(equipment.type) as GameObjectDefinition;
    const textRepresentation = equipDefinition.textRepresentation ?? '';

    const textFontSize = GameObjectDefinition.getTextFontSize(object.definition) * this.resolveScale(ctx, object, DrawType.Object);
    ctx.font = `${textFontSize}px Apple Color Emoji,Segoe UI Emoji,Noto Color Emoji,Android Emoji,EmojiSymbols,EmojiOne Mozilla,Twemoji Mozilla,Segoe UI Symbol,Noto Color Emoji Compat,emoji,noto-emojipedia-fallback`;

    const [drawAdjX, drawAdjY] = Painter.EQUIP_ADJ.get(position) as number[];
    Painter.drawTextRepresentation(ctx, textRepresentation, object.ser.x + drawAdjX, object.ser.y + drawAdjY);
  }

  drawEquipment(ctx: CanvasRenderingContext2D, object: GameObject) {
    if (!object.ser.customState.equipment) {
      return;
    }

    if (object.ser.customState.equipment.head) {
      this.drawEquipmentPiece(ctx, object, object.ser.customState.equipment.head, 'head');
    }
    if (object.ser.customState.equipment.rightHand) {
      this.drawEquipmentPiece(ctx, object, object.ser.customState.equipment.rightHand, 'rightHand');
    }
    if (object.ser.customState.equipment.leftHand) {
      this.drawEquipmentPiece(ctx, object, object.ser.customState.equipment.leftHand, 'leftHand');
    }
    if (object.ser.customState.equipment.legs) {
      this.drawEquipmentPiece(ctx, object, object.ser.customState.equipment.legs, 'legs');
    }
    if (object.ser.customState.equipment.chest) {
      this.drawEquipmentPiece(ctx, object, object.ser.customState.equipment.chest, 'chest');
    }
    if (object.ser.customState.equipment.feet) {
      this.drawEquipmentPiece(ctx, object, object.ser.customState.equipment.feet, 'feet');
    }
  }

  drawObject(ctx: CanvasRenderingContext2D, object: GameObject) {
    // drawing to map means we'll streamline how we draw some of this.
    const isMap = ctx === this.mapCtx;

    ctx.save();

    if (!isMap) {
      this.drawEquipment(ctx, object);
    }

    if (object.ser.customState.paintingImageUrl) {
      this.drawImage(ctx, object, object.ser.customState.paintingImageUrl);
    } else if (object.definition.imageRepresentation) {
      this.drawImage(ctx, object, object.definition.imageRepresentation, true);
    } else if (object.definition.textRepresentation) {
      if (object.definition.fill && this.emojisNeedFill) {
        ctx.fillStyle = object.definition.fill;
        ctx.shadowColor = object.definition.fill;
        ctx.shadowBlur = 7;
        ctx.fill(object.path);
      }

      ctx.shadowColor = (object.definition.fill ? object.definition.fill : 'black');
      ctx.fillStyle = object.definition.fill ?? '';
      ctx.shadowBlur = (object.definition.fill ? 10 : 1);

      ctx.strokeStyle = '#000000';

      if (object.properties.isMovable) {
        ctx.shadowOffsetX = isMap ? 1 : 3;
        ctx.shadowOffsetY = isMap ? 1 : 3;
      }

      const textFontSize = GameObjectDefinition.getTextFontSize(object.definition) * this.resolveScale(ctx, object, DrawType.Object);
      ctx.font = `${object.definition.textFontWeight ? object.definition.textFontWeight : ''} ${textFontSize}px Apple Color Emoji,Segoe UI Emoji,Noto Color Emoji,Android Emoji,EmojiSymbols,EmojiOne Mozilla,Twemoji Mozilla,Segoe UI Symbol,Noto Color Emoji Compat,emoji,noto-emojipedia-fallback`;

      Painter.drawTextRepresentation(ctx, object.definition.textRepresentation, object.ser.x - 8, object.ser.y + 42);
    }

    // Drawing past this point is more detail that we only draw on main canvas and leave out of the map view
    if (isMap) {
      this.drawNameWhenPerson(ctx, object);

      ctx.restore();
      return;
    }

    if (object.ser.quests && object.ser.quests.length > 0) {
      const questScrollTextFontSize = GameObjectDefinition.getTextFontSize(object.definition) / 2;
      ctx.font = `bold ${questScrollTextFontSize}px Apple Color Emoji,Segoe UI Emoji,Noto Color Emoji,Android Emoji,EmojiSymbols,EmojiOne Mozilla,Twemoji Mozilla,Segoe UI Symbol,Noto Color Emoji Compat,emoji,noto-emojipedia-fallback`;
      Painter.drawTextRepresentation(ctx, '📜', object.ser.x + 8, object.ser.y + 60);
    }

    if (object.ser.holding) {
      // Good enough for now; we want to spread these out visually later, as we may load a lot of ingredients in.
      const textRepresentation = [...object.ser.holding.map((objName) => this.gameState.objectDictionary.get(objName)?.textRepresentation ?? '')];

      const holdingTextFontSize = GameObjectDefinition.getTextFontSize(object.definition) * (2 / 3);
      ctx.font = `bold ${holdingTextFontSize}px Apple Color Emoji,Segoe UI Emoji,Noto Color Emoji,Android Emoji,EmojiSymbols,EmojiOne Mozilla,Twemoji Mozilla,Segoe UI Symbol,Noto Color Emoji Compat,emoji,noto-emojipedia-fallback`;
      Painter.drawTextRepresentation(ctx, textRepresentation, object.ser.x + 8, object.ser.y + 42);
    }

    this.drawNameWhenPerson(ctx, object);

    ctx.restore();
  }

  drawAllObjects(backgroundImage?: HTMLImageElement) {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.shadowColor = '';
    this.ctx.shadowBlur = 0;
    this.ctx.shadowOffsetX = 0;
    this.ctx.shadowOffsetY = 0;

    // Currently it's easier to reason how to draw background if restarting from 0,0 origin. To eliminate this transform, I should think through how to reframe this function accordingly.
    this.ctx.setTransform(1, 0, 0, 1, 0, 0);

    if (!this.cachedBackgroundImage && backgroundImage && backgroundImage.width) {
      this.cachedBackgroundImage = backgroundImage;
    }

    this.drawBackground(this.cachedBackgroundImage);

    this.ctx.setTransform(1, 0, 0, 1, this.gameState.savable.globalXOffset, this.gameState.savable.globalYOffset);

    this.gameState.savable.objects.filter((obj) => obj !== this.gameState.grabbedMovable)
      .sort(SORT_BY_DRAW_PRIORITY).forEach((obj: GameObject) => this.drawObject(this.ctx, obj));

    if (this.gameState.grabbedMovable) {
      this.drawObject(this.ctx, this.gameState.grabbedMovable);
    }

    this.drawMap();
  }

  static readonly MAP_SCALE = 0.1;

  // how much to 'horizontally center' the current view in the map.
  // if zero, then the current view is 'glued' to the lefthand side of the map and the map will only show what is to the right.
  static readonly MAP_OFFSET_X = 60;

  // how much to 'vertically center' the current view in the map.
  // if zero, then the current view is 'glued' to the top of the map and the map will only show what is to the bottom.
  static readonly MAP_OFFSET_Y = 80;

  drawMap() {
    this.mapCtx.setTransform(1, 0, 0, 1, 0, 0);
    this.mapCtx.clearRect(0, 0, this.mapCanvas.width, this.mapCanvas.height);
    this.mapCtx.fillStyle = '#2D600D';
    this.mapCtx.fillRect(0, 0, this.mapCanvas.width, this.mapCanvas.height);

    // Draw an outline that shows approximately what the current view looks like vs. the zoomed out map
    this.mapCtx.strokeStyle = '#888';
    this.mapCtx.lineWidth = 1;
    this.mapCtx.strokeRect(
      Painter.MAP_OFFSET_X,
      Painter.MAP_OFFSET_Y,
      Painter.MAP_SCALE * this.canvas.width,
      Painter.MAP_SCALE * this.canvas.height,
    );

    const relativeX = (this.gameState.savable.globalXOffset * Painter.MAP_SCALE) + Painter.MAP_OFFSET_X;
    const relativeY = (this.gameState.savable.globalYOffset * Painter.MAP_SCALE) + Painter.MAP_OFFSET_Y;
    this.mapCtx.setTransform(Painter.MAP_SCALE, 0, 0, Painter.MAP_SCALE, relativeX, relativeY);

    this.gameState.savable.objects.filter((obj) => obj !== this.gameState.grabbedMovable)
      .sort(SORT_BY_DRAW_PRIORITY).forEach((obj: GameObject) => this.drawObject(this.mapCtx, obj));
  }

  drawForArtCanvas(object: GameObject): ImageBitmap {
    if (object.definition.imageRepresentation) {
      const cachedImage = this.cachedImages.get(object.ser.customState.id);

      if (cachedImage) {
        this.offscreenCtx.drawImage(cachedImage, 0, 0, this.offscreenCanvas.width, this.offscreenCanvas.height);
      }
    } else if (object.definition.textRepresentation) {
      const textFontSize = GameObjectDefinition.getTextFontSize(object.definition);
      this.offscreenCtx.font = `${object.definition.textFontWeight ? object.definition.textFontWeight : ''} ${textFontSize}px Apple Color Emoji,Segoe UI Emoji,Noto Color Emoji,Android Emoji,EmojiSymbols,EmojiOne Mozilla,Twemoji Mozilla,Segoe UI Symbol,Noto Color Emoji Compat,emoji,noto-emojipedia-fallback`;

      Painter.drawTextRepresentation(this.offscreenCtx, object.definition.textRepresentation, -8, 42);
    }
    return this.offscreenCanvas.transferToImageBitmap();
  }
}

export default Painter;
