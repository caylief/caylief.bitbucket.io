import CountingPuzzle from './models/puzzles/countingpuzzle';
import GameObjectDefinition from './models/gameobjectdefinition';
import GameState from './models/gamestate';
import Puzzle from './models/puzzles/puzzle';
import Narrator from './narrator';
import Painter from './painter';
import ReadingPuzzle from './models/puzzles/readingpuzzle';

class PuzzleHandler {
  static readonly SUPPORTED_PUZZLES = [
    'countingPuzzle',
    'readingPuzzle',
  ];

  readonly narrator: Narrator;

  readonly peopleNames: string[];

  readonly gameState: GameState;

  readonly apps: HTMLElement;

  readonly puzzleApp: HTMLElement;

  readonly puzzleView: HTMLElement;

  readonly puzzleCanvas: HTMLCanvasElement;

  readonly puzzleCtx: CanvasRenderingContext2D;

  readonly answer: HTMLInputElement;

  readonly submitAnswer: HTMLElement;

  readonly randomizerInt: (max: number) => number;

  readonly quitPuzzle: () => void;

  puzzleGiver: string | undefined;

  answersMade = 0;

  puzzleAppAnswerListener: EventListener | undefined;

  constructor(
    narrator: Narrator,
    peopleNames: string[],
    gameState: GameState,
    apps: HTMLElement,
    puzzleApp: HTMLElement,
    randomizerInt: (max: number) => number,
    quitPuzzle: () => void,
  ) {
    this.narrator = narrator;
    this.peopleNames = peopleNames;
    this.gameState = gameState;
    this.apps = apps;
    this.puzzleApp = puzzleApp;
    this.puzzleView = this.puzzleApp.querySelector('#puzzleView') as HTMLElement;
    this.puzzleCanvas = this.puzzleApp.querySelector('#puzzleCanvas') as HTMLCanvasElement;
    this.puzzleCtx = this.puzzleCanvas.getContext('2d') as CanvasRenderingContext2D;
    this.answer = this.puzzleApp.querySelector('#answer') as HTMLInputElement;
    this.submitAnswer = this.puzzleApp.querySelector('#submitAnswer') as HTMLElement;
    this.randomizerInt = randomizerInt;
    this.quitPuzzle = quitPuzzle;
  }

  applyAnswerListener(answerListener: () => void) {
    if (this.puzzleAppAnswerListener) {
      this.submitAnswer.removeEventListener('click', this.puzzleAppAnswerListener);
    }

    this.puzzleAppAnswerListener = answerListener;
    this.submitAnswer.addEventListener('click', this.puzzleAppAnswerListener);
  }

  drawObject(object: GameObjectDefinition, fontSize: number, xPos: number, yPos: number) {
    this.puzzleCtx.shadowColor = 'black';
    this.puzzleCtx.shadowBlur = 1;
    this.puzzleCtx.strokeStyle = '#000000';
    this.puzzleCtx.font = `${fontSize}px Apple Color Emoji,Segoe UI Emoji,Noto Color Emoji,Android Emoji,EmojiSymbols,EmojiOne Mozilla,Twemoji Mozilla,Segoe UI Symbol,Noto Color Emoji Compat,emoji,noto-emojipedia-fallback`;

    Painter.drawTextRepresentation(this.puzzleCtx, object.textRepresentation, xPos, yPos);
  }

  // expecting we'll draw objects on the canvas, this is returns a grid of all valid coords we can use which should draw relatively spaced out.
  getAllCoords(fontSize: number) {
    const maxX = this.puzzleCanvas.width - fontSize;
    const maxY = this.puzzleCanvas.height - fontSize;

    const totalObjectsX = Math.floor(maxX / fontSize);
    const totalObjectsY = Math.floor(maxY / fontSize);

    const allCoords = [];
    for (let i = 0; i < totalObjectsX - 2; i += 1) {
      for (let j = 0; j < totalObjectsY - 2; j += 1) {
        allCoords.push([i, j]);
      }
    }

    return allCoords;
  }

  activatePuzzle(puzzle: Puzzle<unknown>, questGiver: string, onPuzzleSolved?: () => void) {
    if (!PuzzleHandler.SUPPORTED_PUZZLES.includes(puzzle.name)) {
      this.apps.style.display = 'none';
      return false;
    }

    this.answer.value = '';
    this.answersMade = 0;
    this.puzzleGiver = questGiver;
    this.puzzleApp.style.display = 'block';

    switch (puzzle.name) {
      case 'countingPuzzle':
        // puzzles that depend on canvas need to be made visible before activation
        this.apps.style.display = 'block';

        this.applyCountingPuzzle(puzzle as CountingPuzzle, onPuzzleSolved);
        return true;
      case 'readingPuzzle':
        // puzzles that depend on canvas need to be made visible before activation
        this.apps.style.display = 'block';

        this.applyReadingPuzzle(puzzle as ReadingPuzzle, onPuzzleSolved);
        return true;
      default:
        this.apps.style.display = 'none';
        return false;
    }
  }

  applyCountingPuzzle(puzzle: CountingPuzzle, onPuzzleSolved?: () => void) {
    this.puzzleView.style.display = 'none';
    this.puzzleCanvas.style.display = 'block';
    this.puzzleCanvas.width = this.puzzleApp.clientWidth;
    this.puzzleCanvas.height = this.puzzleApp.clientHeight;

    const { config } = puzzle;
    const object = this.gameState.objectDictionary.get(config.objectToCount) as GameObjectDefinition;

    const fontSize = GameObjectDefinition.getTextFontSize(object);
    const fenceImage = document.querySelector('#fence') as HTMLImageElement;

    this.puzzleCtx.clearRect(0, 0, this.puzzleCanvas.width, this.puzzleCanvas.height);

    this.puzzleCtx.fillStyle = '#2D600D';
    this.puzzleCtx.fillRect(0, 0, this.puzzleCanvas.width, this.puzzleCanvas.height);

    for (let i = 0; i < this.puzzleCanvas.width; i += fontSize) {
      this.puzzleCtx.drawImage(fenceImage, i, 0, fontSize, fontSize);
      this.puzzleCtx.drawImage(fenceImage, i, this.puzzleCanvas.height - (fontSize), fontSize, fontSize);
    }

    if (!config.countMin) {
      config.countMin = 3;
    }
    if (!config.answersMaxAllowed) {
      config.answersMaxAllowed = 3;
    }

    const allCoords = this.getAllCoords(fontSize);
    let countTo = config.countMin + this.randomizerInt(config.countMax - config.countMin);
    let i;
    for (i = 1; i <= countTo && allCoords.length > 0; i += 1) {
      const [coords] = allCoords.splice(this.randomizerInt(allCoords.length), 1);
      const xPos = fontSize + coords[0] * fontSize + this.randomizerInt(fontSize);
      const yPos = fontSize + coords[1] * fontSize + this.randomizerInt(fontSize);

      this.drawObject(object, fontSize, xPos, yPos);
    }

    // if we ran out of room on the canvas early, stop the count to how many we placed.
    if (countTo <= i) {
      countTo = i - 1;
    }

    this.applyAnswerListener(() => {
      const submittedAnswer = this.answer.value.trim();
      let puzzleMessage;

      if (submittedAnswer === `${countTo}`) {
        puzzleMessage = `${this.gameState.savable.playerName} told ${this.puzzleGiver ?? 'the person'} the count is ${countTo}, which is correct.`;

        const thePuzzle = puzzle;
        thePuzzle.isDone = true;

        this.quitPuzzle();
        if (onPuzzleSolved) {
          onPuzzleSolved();
        }
      } else {
        puzzleMessage = `${this.gameState.savable.playerName} told ${this.puzzleGiver ?? 'the person'} the count is ${submittedAnswer === '' ? 'nothing' : submittedAnswer}, which is not correct. Try again.`;

        this.answersMade += 1;

        // after enough wrong answers, reset the puzzle to encourage solving it the correct way instead of with guessing.
        if (this.answersMade >= config.answersMaxAllowed) {
          this.quitPuzzle();
        }
      }

      this.narrator.configureNarratorBox(puzzleMessage);
    });
  }

  static parseStringTemplate(str: string, obj: Map<string, string>): string {
    const parts = str.split(/\$\{(?!\d)[\wæøåÆØÅ]*\}/);
    const args = str.match(/[^{}]+(?=})/g) || [];
    const parameters = args.map((argument) => (obj.get(argument) ?? ''));
    return String.raw({ raw: parts }, ...parameters);
  }

  static shuffleArray = (array: string[]) => {
    const theArray = array;
    for (let i = array.length - 1; i > 0; i -= 1) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = theArray[i];
      theArray[i] = theArray[j];
      theArray[j] = temp;
    }
  };

  applyReadingPuzzle(puzzle: ReadingPuzzle, onPuzzleSolved?: () => void) {
    this.puzzleView.style.display = 'block';
    this.puzzleCanvas.style.display = 'none';

    const { config } = puzzle;
    const chosenQuestionAnswer = config.possibleQuestionAnswers[this.randomizerInt(config.possibleQuestionAnswers.length)];

    const peopleNamesSnap = [...this.peopleNames];
    PuzzleHandler.shuffleArray(peopleNamesSnap);
    const peopleNames = new Map<string, string>();

    peopleNamesSnap.forEach((nm, i) => {
      peopleNames.set(`peopleName${i + 1}`, nm);
    });

    const chosenAnswer = PuzzleHandler.parseStringTemplate(chosenQuestionAnswer.answer, peopleNames);

    if (!config.answersMaxAllowed) {
      config.answersMaxAllowed = 3;
    }

    const puzzleHTML = `${config.phrases.map((p) => `<p>${PuzzleHandler.parseStringTemplate(p, peopleNames)}</p>`).join('')}<p class="puzzleQuestion">Question: ${PuzzleHandler.parseStringTemplate(chosenQuestionAnswer.question, peopleNames)}</p>`;

    this.narrator.configureForReading(this.puzzleApp, puzzleHTML, '#puzzleView', '#puzzleView p');

    this.applyAnswerListener(() => {
      const submittedAnswer = this.answer.value.trim().toLowerCase();
      let puzzleMessage;

      if (submittedAnswer === chosenAnswer.toLowerCase()) {
        puzzleMessage = `${this.gameState.savable.playerName} told ${this.puzzleGiver ?? 'the person'} the answer is ${chosenAnswer}, which is correct.`;

        const thePuzzle = puzzle;
        thePuzzle.isDone = true;

        this.quitPuzzle();
        if (onPuzzleSolved) {
          onPuzzleSolved();
        }
      } else {
        puzzleMessage = `${this.gameState.savable.playerName} told ${this.puzzleGiver ?? 'the person'} the answer is ${submittedAnswer === '' ? 'nothing' : submittedAnswer}, which is not correct. Try again.`;

        this.answersMade += 1;

        // after enough wrong answers, reset the puzzle to encourage solving it the correct way instead of with guessing.
        if (this.answersMade >= config.answersMaxAllowed) {
          this.quitPuzzle();
        }
      }

      this.narrator.configureNarratorBox(puzzleMessage);
    });
  }
}

export default PuzzleHandler;
