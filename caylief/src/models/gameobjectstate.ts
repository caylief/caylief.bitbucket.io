// Indicates that a GameObject or other entity can be painted -- i.e. that we can let the player make it look like anything they want.
interface Paintable {
  // The ID is required in order to uniquely identify the canvas & disk persistence of this paintable
  id: string;

  // Data URI as obtained from the art app canvas
  paintingImageUrl?: string;
}

// NOTE: Equipment must avoid a dependency cycle because certain GameObjects (people) have equipment. Decoupling based on property interfaces mitigates this.
// So Equipment (besides GameObjects) are Paintable. We could propagate other GameObject properties, such as the width/height, but these are derivable just by looking it up from the dictionary. May be faster to store them in the interface later though.
interface EquipmentPiece extends Paintable {
  type: string;
}

interface Equipment {
  head?: EquipmentPiece;

  rightHand?: EquipmentPiece;

  leftHand?: EquipmentPiece;

  chest?: EquipmentPiece;

  legs?: EquipmentPiece;

  feet?: EquipmentPiece;
}

// Unique properties that should stay persisted with a GameObject (e.g. serialized to storage, persisted into backpack, etc.) and can't be derived when creating a brand new instance of the object from configuration.
// We will keep position (x/y & path) out of this as these have no meaning in certain persistence -- e.g. putting an object into the backpack it no longer needs a position until it receives a new one when removed.
// TODO: should we move givenName, holding, and quests here? they fit the same category.
class GameObjectState implements Paintable {
  // TODO: this should be readonly, but for backwards compatibility we have to port it from the GameObject from old saves.
  id: string;

  paintingImageUrl?: string;

  equipment: Equipment = {};

  constructor(id: string) {
    this.id = id;
  }
}

export type {
  Equipment,
  EquipmentPiece,
  Paintable,
};

export default GameObjectState;
