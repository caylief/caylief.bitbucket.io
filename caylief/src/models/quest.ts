import Drop from './drop';
import DoneCriteria from './donecriteria';

class Quest {
  readonly name: string;

  readonly narrative: string;

  readonly dialogueStart: string[];

  readonly dialogueComplete: string;

  readonly doneCriteria: DoneCriteria;

  readonly drops: Map<string, Drop[]>;

  state?: string;

  constructor(
    name: string,
    narrative: string,
    dialogueStart: string[],
    dialogueComplete: string,
    doneCriteria: DoneCriteria,
    drops: Map<string, Drop[]>,
  ) {
    this.name = name;
    this.narrative = narrative;
    this.dialogueStart = dialogueStart;
    this.dialogueComplete = dialogueComplete;
    this.doneCriteria = doneCriteria;
    this.drops = drops;
  }
}

export default Quest;
