class CraftEvaluation {
  readonly objName: string;

  readonly recipe: string[];

  readonly resourceCosts: Map<string, number>;

  enoughMaterials: boolean;

  constructor(
    objName: string,
    recipe: string[],
    resourceCosts: Map<string, number>,
    enoughMaterials: boolean,
  ) {
    this.objName = objName;
    this.recipe = recipe;
    this.resourceCosts = resourceCosts;
    this.enoughMaterials = enoughMaterials;
  }
}

export default CraftEvaluation;
