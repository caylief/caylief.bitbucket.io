import { GameObject } from './gameobject';
import GameObjectDefinition from './gameobjectdefinition';
import GameObjectProperties from './gameobjectproperties';
import Recipe from './recipe';
import { SavableGameState } from './savablegamestate';

class GameState {
  readonly savable: SavableGameState = new SavableGameState();

  // Temporal state (active dragging); wouldn't make sense to save
  grabbedMovable: GameObject | undefined;

  // Game State derivable from config (no need to save these)
  readonly objectDictionary: Map<string, GameObjectDefinition> = new Map();

  readonly objectRecipes: Map<string, Recipe> = new Map();

  readonly objectClassProperties: Map<string, GameObjectProperties> = new Map();

  readonly shopInventories: Map<string, string[]> = new Map();
}

export default GameState;
