import { SerializableGameObject } from './gameobject';

class Backpack {
  readonly resources: Map<string, number>;

  readonly statefulObjects: SerializableGameObject[];

  constructor(resources: Map<string, number> = new Map<string, number>(), statefulObjects: SerializableGameObject[] = []) {
    this.resources = resources;
    this.statefulObjects = statefulObjects;
  }

  totalInBackpack(objName: string) {
    return (this.resources.get(objName) ?? 0) + this.statefulObjects.filter((object) => object.name === objName).length;
  }

  add(resourceName: string, resourceCount: number) {
    this.resources.set(resourceName, (this.resources.get(resourceName) ?? 0) + resourceCount);
  }

  // returns true if this was a valid removal / we had enough items to satisfy the request. Otherwise returns false and no removal will occur.
  remove(resourceName: string, resourceCount: number) {
    if ((this.resources.get(resourceName) ?? 0) < resourceCount) {
      return false;
    }

    this.resources.set(resourceName, (this.resources.get(resourceName) ?? 0) - resourceCount);
    return true;
  }
}

export default Backpack;
