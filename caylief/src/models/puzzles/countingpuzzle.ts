import Puzzle from './puzzle';

interface CountingPuzzleConfig {
  readonly objectToCount: string;

  countMin: number;

  readonly countMax: number;

  answersMaxAllowed: number;
}

class CountingPuzzle implements Puzzle<CountingPuzzleConfig> {
  readonly name: string;

  readonly config: CountingPuzzleConfig;

  isDone: boolean;

  constructor(name: string, config: CountingPuzzleConfig) {
    this.name = name;
    this.config = config;
    this.isDone = false;
  }
}

export default CountingPuzzle;
