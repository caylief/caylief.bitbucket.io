import Puzzle from './puzzle';

interface QuestionAnswer {
  readonly question: string;
  readonly answer: string;
}

interface ReadingPuzzleConfig {
  readonly phrases: string[];

  readonly possibleQuestionAnswers: QuestionAnswer[];

  answersMaxAllowed: number;
}

class ReadingPuzzle implements Puzzle<ReadingPuzzleConfig> {
  readonly name: string;

  readonly config: ReadingPuzzleConfig;

  isDone: boolean;

  constructor(name: string, config: ReadingPuzzleConfig) {
    this.name = name;
    this.config = config;
    this.isDone = false;
  }
}

export default ReadingPuzzle;
