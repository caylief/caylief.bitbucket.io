interface Puzzle<T> {
  readonly name: string;
  readonly config: T;
  isDone?: boolean;
}

export default Puzzle;
