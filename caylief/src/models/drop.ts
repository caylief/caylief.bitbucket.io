class Drop {
  readonly chance: number;

  readonly amount: number;

  constructor(chance: number, amount: number) {
    this.chance = chance;
    this.amount = amount;
  }
}

export default Drop;
