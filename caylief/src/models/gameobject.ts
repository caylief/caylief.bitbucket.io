import GameObjectDefinition from './gameobjectdefinition';
import GameObjectProperties from './gameobjectproperties';
import GameObjectState from './gameobjectstate';
import Quest from './quest';

interface SerializableGameObject {
  readonly id: string;

  type: string;

  // TODO: refactor this out, this is a dupe of type.
  name: string;

  x: number;

  y: number;

  givenName?: string;

  holding: string[];

  quests: Quest[];

  readonly customState: GameObjectState;
}

class GameObject {
  readonly ser: SerializableGameObject;

  readonly definition: GameObjectDefinition;

  readonly properties: GameObjectProperties;

  path: Path2D;

  readonly drawPriority: number;

  constructor(
    serializableGameObject: SerializableGameObject,
    definition: GameObjectDefinition,
    properties: GameObjectProperties,
  ) {
    this.ser = serializableGameObject;
    this.definition = definition;
    this.properties = properties;
    this.drawPriority = this.calculateDrawPriority();
    this.path = this.calculatePath();
  }

  calculatePath() {
    const width = (this.definition.width / 1.5) * window.devicePixelRatio;
    const height = (this.definition.height / 1.5) * window.devicePixelRatio;
    const path = new Path2D();
    path.rect(this.ser.x, this.ser.y, width, height);
    return path;
  }

  // The reasoning below outlines intent -- the priority for drawing will count up from 0 accordingly. Grabbed Tool will receive 0 priority elsewhere.
  // This is because we use the default canvas settings and we want the highest-number priority drawn first, so it is below everything drawn after it.
  // Grabbed Movable over Tools over Enemies over People/Villagers over Growables over Furniture over other Movables over everything else.
  // * First, Player should be able to find what they've grabbed and their tools.
  // * Second, Player should be able to see enemies for finishing related quests.
  // * Third, Player should be able to see and talk to villagers for starting and turning in finished quests. They will be behind enemies in order to make enemies more annoying.
  // * Fourth, Grown plants and food can be showcased decoratively
  // * Fifth, Furniture is showcased decoratively, usually below plants.
  // * Sixth, Other movables the Player will want to grab, so they should be more visible than things that are not movable, e.g. natural resources like trees and mountains.
  // * Seventh, Water resources look better underneath other resources, since they are "in the ground" vs. other resources are "above the ground".
  // * Everything else is left disorganized, likely to be reorganized later.
  calculateDrawPriority(): number {
    const { properties } = this;
    const { definition } = this;
    if (!properties || !definition) {
      window.console.error(`Missing required fields: ${this.toString()}`);
      return 8;
    }

    if (properties.isTool) {
      return 1;
    } if (properties.isEnemy) {
      return 2;
    } if (properties.isPerson) {
      return 3;
    } if (properties.isGrowable || properties.isEdible) {
      return 4;
    } if (properties.isFurniture) {
      return 5;
    } if (properties.isMovable) {
      return 6;
    } if (properties.isAnimal) {
      return 7;
    } if (definition.provides === 'water') {
      return 9;
    }
    return 8;
  }

  getDisplayName(): string {
    return GameObjectDefinition.getDisplayName(this.definition, this.ser.name);
  }

  moveObject(offsetX: number, offsetY: number, pointerType: string) {
    this.ser.x += offsetX * (this.definition.weight ? this.definition.weight : 1) * (pointerType === 'mouse' ? 1 : 3);
    this.ser.y += offsetY * (this.definition.weight ? this.definition.weight : 1) * (pointerType === 'mouse' ? 1 : 3);
    this.path = this.calculatePath();
  }

  removeHolding(shouldKeep: (h: string) => boolean) {
    this.ser.holding = this.ser.holding.filter(shouldKeep);
  }
}

export {
  GameObject,
};

export type { SerializableGameObject };
