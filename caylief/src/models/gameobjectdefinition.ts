import DropEntry from './dropentry';

class GameObjectDefinition {
  readonly name: string;

  readonly displayName?: string;

  readonly plural?: string;

  // Required Rendering configuration
  readonly textRepresentation: string;

  readonly width: number;

  readonly height: number;

  // Optional Rendering configuration
  readonly imageRepresentation?: string;

  readonly fill?: string;

  readonly textFontWeight?: string;

  readonly textFontSize?: number;

  // Optional properties that concern whether the object can be purchased, crafted, cooked, interacted with a tool, whether it consumes or provides certain resources, what drops it has, etc.
  readonly price?: number;

  readonly purchasedAt?: string[];

  readonly pickup?: string[];

  readonly weight?: number;

  readonly recipe?: string[];

  readonly recipeHints?: string[];

  readonly madeIn?: string[];

  readonly tools?: string[];

  readonly consumes?: string;

  readonly provides?: string;

  readonly fuelRequired?: string[];

  readonly defeatedBy?: string[];

  readonly drops?: DropEntry[];

  readonly isConsumed?: boolean;

  readonly isIngredient?: boolean;

  readonly isCookingImplement?: boolean;

  readonly isGrowable?: boolean;

  // Sounds. Could be pulled into a nested definition.
  readonly loadingSound?: string;

  readonly useSound?: string;

  readonly eatSound?: string;

  readonly cookingSound?: string;

  readonly fillSound?: string;

  readonly consumeSound?: string;

  readonly providesSound?: string;

  constructor(
    name: string,
    displayName: string,
    textRepresentation: string,
    imageRepresentation: string,
    width: number,
    height: number,
    weight: number,
    recipe: string[],
    recipeHints: string[],
    madeIn: string[],
  ) {
    this.name = name;
    this.displayName = displayName;
    this.textRepresentation = textRepresentation;
    this.imageRepresentation = imageRepresentation;
    this.width = width;
    this.height = height;
    this.weight = weight;
    this.recipe = recipe;
    this.recipeHints = recipeHints;
    this.madeIn = madeIn;
  }

  static getDisplayName(obj: GameObjectDefinition, backupName = ''): string {
    return obj.displayName ?? obj.name ?? backupName;
  }

  static DEFAULT_TEXT_FONT_SIZE = (48 / 1.5) * window.devicePixelRatio;

  static getTextFontSize(obj: GameObjectDefinition): number {
    return obj.textFontSize ? (obj.textFontSize / 1.5) * window.devicePixelRatio : GameObjectDefinition.DEFAULT_TEXT_FONT_SIZE;
  }
}

export default GameObjectDefinition;
