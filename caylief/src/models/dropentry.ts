import Drop from './drop';

class DropEntry {
  readonly key: string;

  readonly value: Drop[];

  constructor(key: string, value: Drop[]) {
    this.key = key;
    this.value = value;
  }
}

export default DropEntry;
