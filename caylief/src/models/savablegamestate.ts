import Backpack from './backpack';
import DiscoveredRecipes from './discoveredrecipes';
import { GameObject, SerializableGameObject } from './gameobject';

interface RestoredGameState {
  // Player State
  playerName: string;

  // How far we'll shift from the original origin 0,0. Used for manuevering and giving ourselves a map.
  globalXOffset: number;

  // How far we'll shift from the original origin 0,0. Used for manuevering and giving ourselves a map.
  globalYOffset: number;

  // We can store two types of things in the backpack currently.
  // 1. Resources: X amount of 'coal', 'stone', 'wood', etc.
  // 2. Stateful Objects: Paintings.
  backpack: Backpack;

  discoveredRecipes: DiscoveredRecipes;

  defeatedCounts: Map<string, number>;

  questCounts: Map<string, number>;

  cookedCounts: Map<string, number>;

  craftedCounts: Map<string, number>;

  wandWishCount: number;

  // Savable Game State
  peopleNamesBag: string[];

  objects: SerializableGameObject[];

  // if we want to save space we could recalculate this manually by inspecting objects. But this should be a pretty small footprint.
  counts: Map<string, number>;
}

enum CountType {
  Defeated,
  Quests,
  Cooked,
  Crafted,
  Objects,
}

// Technically some of this is Player state vs. Game state, but both need to be persisted to return to where we were when refreshing.
class SavableGameState {
  // Player State
  playerName = 'You';

  // How far we'll shift from the original origin 0,0. Used for manuevering and giving ourselves a map.
  globalXOffset = 0;

  // How far we'll shift from the original origin 0,0. Used for manuevering and giving ourselves a map.
  globalYOffset = 0;

  // We can store two types of things in the backpack currently.
  // 1. Resources: X amount of 'coal', 'stone', 'wood', etc.
  // 2. Stateful Objects: Paintings.
  backpack: Backpack = new Backpack();

  discoveredRecipes: DiscoveredRecipes = new DiscoveredRecipes();

  defeatedCounts: Map<string, number> = new Map();

  questCounts: Map<string, number> = new Map();

  cookedCounts: Map<string, number> = new Map();

  craftedCounts: Map<string, number> = new Map();

  wandWishCount = 0;

  // Savable Game State
  peopleNamesBag: string[] = [];

  objects: GameObject[] = [];

  // if we want to save space we could recalculate this manually by inspecting objects. But this should be a pretty small footprint.
  counts: Map<string, number> = new Map();

  // Increments a counter by 1.
  addCount(countType: CountType, counted: string) {
    switch (countType) {
      case CountType.Defeated:
        this.defeatedCounts.set(counted, (this.defeatedCounts.get(counted) ?? 0) + 1);
        break;
      case CountType.Quests:
        this.questCounts.set(counted, (this.questCounts.get(counted) ?? 0) + 1);
        break;
      case CountType.Cooked:
        this.cookedCounts.set(counted, (this.cookedCounts.get(counted) ?? 0) + 1);
        break;
      case CountType.Crafted:
        this.craftedCounts.set(counted, (this.craftedCounts.get(counted) ?? 0) + 1);
        break;
      case CountType.Objects:
        this.counts.set(counted, (this.counts.get(counted) ?? 0) + 1);
        break;
      default:
    }
  }

  // Reduces a count down by 1 to 0. Doesn't drop beyond 0. Currently only needed for Objects counting.
  removeCount(countType: CountType, counted: string) {
    switch (countType) {
      case CountType.Objects:
        this.counts.set(counted, (this.counts.get(counted) ?? 1) - 1);
        break;
      default:
    }
  }

  resetCounts() {
    this.counts.forEach((__v, key, map) => {
      map.set(key, 0);
    });
    this.defeatedCounts.forEach((__v, key, map) => {
      map.set(key, 0);
    });
    this.questCounts.forEach((__v, key, map) => {
      map.set(key, 0);
    });
    this.cookedCounts.forEach((__v, key, map) => {
      map.set(key, 0);
    });
    this.craftedCounts.forEach((__v, key, map) => {
      map.set(key, 0);
    });
  }
}

export {
  CountType,
  SavableGameState,
};

export type { RestoredGameState };
