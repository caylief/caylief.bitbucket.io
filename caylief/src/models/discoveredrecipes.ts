class DiscoveredRecipes {
  readonly cooking: string[];

  readonly crafting: string[];

  constructor(cooking: string[] = [], crafting: string[] = []) {
    this.cooking = cooking;
    this.crafting = crafting;
  }
}

export default DiscoveredRecipes;
