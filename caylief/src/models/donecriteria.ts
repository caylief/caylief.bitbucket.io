import Puzzle from './puzzles/puzzle';

class DoneCriteria {
  readonly bring?: Map<string, number>;

  readonly defeated?: Map<string, number>;

  readonly puzzle?: Puzzle<unknown>;

  constructor(bring?: Map<string, number>, defeated?: Map<string, number>, puzzle?: Puzzle<unknown>) {
    this.bring = bring;
    this.defeated = defeated;
    this.puzzle = puzzle;
  }
}

export default DoneCriteria;
