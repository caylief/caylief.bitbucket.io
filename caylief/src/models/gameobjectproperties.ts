class GameObjectProperties {
  readonly isMovable?: boolean;

  readonly isPerson?: boolean;

  readonly isTool?: boolean;

  readonly isGrowable?: boolean;

  readonly isFurniture?: boolean;

  readonly isEdible?: boolean;

  readonly isEnemy?: boolean;

  readonly isAnimal?: boolean;

  readonly isHeld?: boolean;

  readonly isShop?: boolean;
}

export default GameObjectProperties;
