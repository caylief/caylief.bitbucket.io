class Recipe {
  readonly recipe: string[];

  readonly madeIn: string[];

  readonly hints: string[];

  constructor(recipe: string[], madeIn: string[], hints: string[] = []) {
    this.recipe = recipe;
    this.madeIn = madeIn;
    this.hints = hints;
  }
}

export default Recipe;
