import GameState from './models/gamestate';

const SCROLL_SPEED = 20;
const MOVEMENT_KEYS = ['KeyS', 'ArrowDown', 'KeyW', 'ArrowUp', 'KeyA', 'ArrowLeft', 'KeyD', 'ArrowRight'];

class NavigationHandler {
  lastGlobalXOffset: number | undefined;

  lastGlobalYOffset: number | undefined;

  keysDown: string[] = [];

  navigateAnim: number | undefined;

  readonly gameState: GameState;

  readonly talkMessageBox: HTMLElement;

  readonly shouldDisableNavigation: () => boolean;

  readonly onNavigationApplied: () => void;

  constructor(gameState: GameState, talkMessageBox: HTMLElement, shouldDisableNavigation: () => boolean, onNavigationApplied: () => void) {
    this.gameState = gameState;
    this.talkMessageBox = talkMessageBox;
    this.shouldDisableNavigation = shouldDisableNavigation;
    this.onNavigationApplied = onNavigationApplied;
  }

  animateNavigation() {
    const talkMessageBoxVisible = (this.talkMessageBox ? this.talkMessageBox.style.visibility === 'visible' : false);
    this.keysDown.forEach((k) => {
      switch (k) {
        case 'KeyS':
        case 'ArrowDown':
          this.gameState.savable.globalYOffset -= SCROLL_SPEED;
          if (talkMessageBoxVisible) this.talkMessageBox.style.top = `${parseInt(this.talkMessageBox.style.top, 10) - SCROLL_SPEED}px`;
          break;
        case 'KeyW':
        case 'ArrowUp':
          this.gameState.savable.globalYOffset += SCROLL_SPEED;
          if (talkMessageBoxVisible) this.talkMessageBox.style.top = `${parseInt(this.talkMessageBox.style.top, 10) + SCROLL_SPEED}px`;
          break;
        case 'KeyA':
        case 'ArrowLeft':
          this.gameState.savable.globalXOffset += SCROLL_SPEED;
          if (talkMessageBoxVisible) this.talkMessageBox.style.left = `${parseInt(this.talkMessageBox.style.left, 10) + SCROLL_SPEED}px`;
          break;
        case 'KeyD':
        case 'ArrowRight':
          this.gameState.savable.globalXOffset -= SCROLL_SPEED;
          if (talkMessageBoxVisible) this.talkMessageBox.style.left = `${parseInt(this.talkMessageBox.style.left, 10) - SCROLL_SPEED}px`;
          break;
        default:
          break;
      }
    });

    if (this.lastGlobalXOffset !== this.gameState.savable.globalXOffset
        || this.lastGlobalYOffset !== this.gameState.savable.globalYOffset) {
      this.onNavigationApplied();
    }

    this.lastGlobalXOffset = this.gameState.savable.globalXOffset;
    this.lastGlobalYOffset = this.gameState.savable.globalYOffset;

    if (this.navigateAnim) {
      this.navigateAnim = window.requestAnimationFrame(() => this.animateNavigation());
    }
  }

  handleKeyDown(e: KeyboardEvent) {
    if (this.shouldDisableNavigation()) {
      return;
    }

    // If we started pushing a navigation key, track it.
    if (MOVEMENT_KEYS.includes(e.code) && !this.keysDown.includes(e.code)) {
      this.keysDown.push(e.code);

      if (!this.navigateAnim) {
        this.navigateAnim = window.requestAnimationFrame(() => this.animateNavigation());
      }
    }
  }

  handleKeyUp(e: KeyboardEvent) {
    // Stop tracking a key if we were tracking it earlier.
    const i = this.keysDown.indexOf(e.code);
    if (i !== -1) {
      this.keysDown.splice(i, 1);
    }

    // We've stopped pushing any navigation keys, so we should stop navigating.
    if (MOVEMENT_KEYS.every((k) => !this.keysDown.includes(k))) {
      this.navigateAnim = undefined;
    }
  }
}

export default NavigationHandler;
