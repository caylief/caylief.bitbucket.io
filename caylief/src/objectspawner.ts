import BoundingBox from './models/boundingbox';
import Drop from './models/drop';
import { GameObject, SerializableGameObject } from './models/gameobject';
import GameObjectDefinition from './models/gameobjectdefinition';
import GameObjectState, { EquipmentPiece } from './models/gameobjectstate';
import GameState from './models/gamestate';
import Quest from './models/quest';
import { CountType } from './models/savablegamestate';

interface ObjectSpawnConfig {
  readonly name: string;
  readonly max: number;
  readonly maxGlobal?: number;
}

interface SpawnConfig {
  readonly chanceToSpawnObject: number,
  readonly chanceToSpawnObjectIncrement: number,
  readonly chanceToSpawnEnemy: number,
  readonly chanceToSpawnEnemyIncrement: number,
  readonly chanceToSpawnQuest: number,
  readonly chanceToSpawnQuestIncrement: number,
  readonly objects: ObjectSpawnConfig[];
  readonly enemies: ObjectSpawnConfig[];
}

class ObjectSpawner {
  readonly spawnConfig: SpawnConfig;

  readonly gameState: GameState;

  // We will store the total table of names and then store a list (peopleNamesBag in the GameState) that we will randomly pop from.
  // When we run out of names in the bag, we will fill it back up with the original table of names.
  // This way, we maximize the # of unique names associated with villagers.
  readonly peopleNames: string[];

  readonly questTemplates: Quest[];

  // TODO: figure out best way to remove this coupling.
  readonly ctx: CanvasRenderingContext2D;

  readonly getValidSpawnArea: () => BoundingBox;

  readonly randomizerInt: (max: number) => number;

  readonly pointInPathChecker: (path: Path2D, x: number, y: number) => boolean;

  readonly pointInObjectPathChecker: (object: GameObject, x: number, y: number) => boolean;

  chanceToSpawn: number;

  chanceToSpawnEnemy: number;

  chanceToSpawnQuest: number;

  constructor(
    spawnConfig: SpawnConfig,
    gameState: GameState,
    peopleNames: string[],
    questTemplates: Quest[],
    ctx: CanvasRenderingContext2D,
    getValidSpawnArea: () => BoundingBox,
    randomizerInt: (max: number) => number,
    pointInPathChecker: (path: Path2D, x: number, y: number) => boolean,
    pointInObjectPathChecker: (object: GameObject, x: number, y: number) => boolean,
  ) {
    this.spawnConfig = spawnConfig;
    this.gameState = gameState;
    this.peopleNames = peopleNames;
    this.questTemplates = questTemplates;
    this.ctx = ctx;
    this.getValidSpawnArea = getValidSpawnArea;
    this.randomizerInt = randomizerInt;
    this.pointInPathChecker = pointInPathChecker;
    this.pointInObjectPathChecker = pointInObjectPathChecker;

    this.chanceToSpawn = this.spawnConfig.chanceToSpawnObject;
    this.chanceToSpawnEnemy = this.spawnConfig.chanceToSpawnEnemy;
    this.chanceToSpawnQuest = this.spawnConfig.chanceToSpawnQuest;
  }

  resetChancesToSpawn() {
    this.chanceToSpawn = this.spawnConfig.chanceToSpawnObject;
    this.chanceToSpawnEnemy = this.spawnConfig.chanceToSpawnEnemy;
    this.chanceToSpawnQuest = this.spawnConfig.chanceToSpawnQuest;
  }

  trySpawn(objname: string, spawnarea: BoundingBox) {
    const spawnX = spawnarea.x + this.randomizerInt(spawnarea.width - 200) + 100;
    const spawnY = spawnarea.y + this.randomizerInt(spawnarea.height - 200) + 100;

    const objProperties = this.gameState.objectDictionary.get(objname) as GameObjectDefinition;
    const middleX = spawnX + (objProperties.width / 2);
    const middleY = spawnY + (objProperties.height / 2);

    if (this.gameState.savable.objects.filter((obj: GameObject) => this.pointInObjectPathChecker(obj, middleX, middleY)).length === 0) {
      this.makeObject(objname, spawnX, spawnY);
      return true;
    }

    return false;
  }

  onSpawnTick(onSpawn = () => {}) {
    const spawnarea = this.getValidSpawnArea();

    let spawnedObjectsThisTick = 0;
    let spawnedEnemiesThisTick = 0;
    let spawnedQuestsThisTick = 0;

    // What the player is looking at, as a path. Any object in here is in the field of view.
    const currentViewPath = new Path2D();
    currentViewPath.rect(spawnarea.x, spawnarea.y, spawnarea.width, spawnarea.height);

    // To correctly compare view path, reset the transform so it's not shifted based on offset/relative player position.
    this.ctx.setTransform(1, 0, 0, 1, 0, 0);

    const spawnThisTick = Math.random() <= this.chanceToSpawn;
    if (!spawnThisTick) {
      this.chanceToSpawn += this.spawnConfig.chanceToSpawnObjectIncrement;
    } else {
      this.chanceToSpawn = this.spawnConfig.chanceToSpawnObject;

      this.spawnConfig.objects.forEach((objSpawnConfig) => {
        const spawnedInView = this.gameState.savable.objects.filter((obj: GameObject) => obj.ser.name === objSpawnConfig.name
        && this.pointInPathChecker(currentViewPath, obj.ser.x, obj.ser.y)).length;
        const spawnedGlobally = (this.gameState.savable.counts.get(objSpawnConfig.name) ?? 0);

        if (spawnedInView < objSpawnConfig.max
          && (!objSpawnConfig.maxGlobal || spawnedGlobally < objSpawnConfig.maxGlobal)) {
          if (this.trySpawn(objSpawnConfig.name, spawnarea)) {
            spawnedObjectsThisTick += 1;
          }
        }
      });
    }

    const spawnEnemiesThisTick = Math.random() <= this.chanceToSpawnEnemy;
    if (!spawnEnemiesThisTick) {
      this.chanceToSpawnEnemy += this.spawnConfig.chanceToSpawnEnemyIncrement;
    } else {
      this.chanceToSpawnEnemy = this.spawnConfig.chanceToSpawnEnemy;

      this.spawnConfig.enemies.forEach((objSpawnConfig) => {
        const spawnedInView = this.gameState.savable.objects.filter((obj: GameObject) => obj.ser.name === objSpawnConfig.name
        && this.pointInPathChecker(currentViewPath, obj.ser.x, obj.ser.y)).length;
        if (spawnedEnemiesThisTick) {
          // Only spawn one enemy per tick, for now.
          return;
        }

        if (spawnedInView < objSpawnConfig.max) {
          if (this.trySpawn(objSpawnConfig.name, spawnarea)) {
            spawnedEnemiesThisTick += 1;
          }
        }
      });
    }

    const spawnQuestsThisTick = Math.random() <= this.chanceToSpawnQuest;
    if (!spawnQuestsThisTick) {
      this.chanceToSpawnQuest += this.spawnConfig.chanceToSpawnQuestIncrement;
    } else {
      this.chanceToSpawnQuest = this.spawnConfig.chanceToSpawnQuest;

      // For now, don't give the same person more than one quest.
      const [foundPerson] = this.gameState.savable.objects.filter((obj: GameObject) => obj.properties.isPerson
      && obj.ser.quests.length === 0) as [GameObject];
      if (foundPerson) {
        // Deep copy; it's important that each local copy of a quest doesn't stomp on the done criteria of others.
        const questIndex = this.randomizerInt(this.questTemplates.length);
        const newQuest = structuredClone(this.questTemplates[questIndex]) as Quest;
        foundPerson.ser.quests.push(newQuest);

        spawnedQuestsThisTick += 1;
      }
    }

    if (spawnedObjectsThisTick || spawnedEnemiesThisTick || spawnedQuestsThisTick) {
      onSpawn();
    }

    // Reset transform to the relative position of the player as this ensures pointer events/interactions trigger in the right place.
    this.ctx.setTransform(1, 0, 0, 1, this.gameState.savable.globalXOffset, this.gameState.savable.globalYOffset);
  }

  makeObject(type: string, x: number, y: number, id?: string, givenName?: string): GameObject | undefined {
    const lookupProperties = this.gameState.objectDictionary.get(type);
    if (!lookupProperties) {
      console.error(`Tried to make an unknown object of type ${type}`);
      return undefined;
    }

    const lookupClassProperties = this.gameState.objectClassProperties.get(type);
    if (!lookupClassProperties) {
      console.error(`Tried to make an unknown object of type ${type}`);
      return undefined;
    }

    const idToAssign = id ?? window.self.crypto.randomUUID();

    let givenNameToAssign = givenName;
    if (!givenNameToAssign && type === 'person') {
      // Bag will start out empty initially, so fill it if we start in this state.
      if (this.gameState.savable.peopleNamesBag.length === 0) {
        this.gameState.savable.peopleNamesBag = [...this.peopleNames];
      }

      const randomPeopleNameIndex = this.randomizerInt(this.gameState.savable.peopleNamesBag.length);
      givenNameToAssign = this.gameState.savable.peopleNamesBag[randomPeopleNameIndex];
      this.gameState.savable.peopleNamesBag.splice(randomPeopleNameIndex, 1);

      // fill the bag back up again.
      if (this.gameState.savable.peopleNamesBag.length === 0) {
        this.gameState.savable.peopleNamesBag = [...this.peopleNames];
      }
    }

    return this.makeObjectFromSerialized({
      id: idToAssign,
      name: type,
      type,
      x,
      y,
      givenName: givenNameToAssign,
      holding: [],
      quests: [],
      customState: new GameObjectState(idToAssign),
    } as SerializableGameObject);
  }

  makeObjectFromSerialized(ser: SerializableGameObject): GameObject | undefined {
    const lookupProperties = this.gameState.objectDictionary.get(ser.type);
    if (!lookupProperties) {
      console.error(`Tried to make an unknown object of type ${ser.type}`);
      return undefined;
    }

    const lookupClassProperties = this.gameState.objectClassProperties.get(ser.type);
    if (!lookupClassProperties) {
      console.error(`Tried to make an unknown object of type ${ser.type}`);
      return undefined;
    }

    this.gameState.savable.addCount(CountType.Objects, ser.type);

    // TODO: for old saves, move the ID to its new location. We can make the customState ID readonly once we remove this.
    if (!ser.customState.id) {
      const overrideSer = ser;
      overrideSer.customState.id = ser.id;
    }

    const object = new GameObject(ser, lookupProperties, lookupClassProperties);

    // For now we'll give every person a default set of equipment. We'll make this configurable and finishing making them paintable later.
    if (object.ser.type === 'person') {
      if (!object.ser.customState.equipment) {
        object.ser.customState.equipment = {};

        object.ser.customState.equipment.head = {
          type: 'helmet',
          id: window.self.crypto.randomUUID(),
        } as EquipmentPiece;

        object.ser.customState.equipment.rightHand = {
          type: 'dagger',
          id: window.self.crypto.randomUUID(),
        } as EquipmentPiece;

        object.ser.customState.equipment.leftHand = {
          type: 'shield',
          id: window.self.crypto.randomUUID(),
        } as EquipmentPiece;

        object.ser.customState.equipment.chest = {
          type: 'shirt',
          id: window.self.crypto.randomUUID(),
        } as EquipmentPiece;

        object.ser.customState.equipment.legs = {
          type: 'pants',
          id: window.self.crypto.randomUUID(),
        } as EquipmentPiece;

        object.ser.customState.equipment.feet = {
          type: 'socks',
          id: window.self.crypto.randomUUID(),
        } as EquipmentPiece;
      }
    }

    this.gameState.savable.objects.push(object);
    return object;
  }

  rollDrops(x: number, y: number, drops: Map<string, Drop[]>): Map<string, number> {
    const actualDrops: Map<string, number> = new Map();
    drops.forEach((value, key) => {
      const chanceRoll = Math.random();

      // choose the rarest drop possible for the roll
      const [rarestDrop] = value
        .sort((a: Drop, b: Drop) => a.chance - b.chance)
        .filter((d: Drop) => chanceRoll <= d.chance);

      if (rarestDrop) {
        for (let i = 0; i < rarestDrop.amount; i += 1) {
          this.makeObject(key, x - 60 + this.randomizerInt(120), y - 60 + this.randomizerInt(120));
        }

        actualDrops.set(key, rarestDrop.amount);
      }
    });

    return actualDrops;
  }
}

export type {
  SpawnConfig,
};

export default ObjectSpawner;
