// TODO: This probably should be fixed; we're using prompt for easy text input, but we probably want a better UI for this?
/* eslint-disable no-alert */

import GameObjectProperties from './models/gameobjectproperties';
import GameState from './models/gamestate';
import Puzzle from './models/puzzles/puzzle';
import Narrator from './narrator';
import ObjectSpawner from './objectspawner';
import PuzzleHandler from './puzzlehandler';

class WandHandler {
  // to prevent getting too many coins in a single wish. May tune further.
  static readonly MAX_COINS_PER_WISH = 7;

  static readonly WAND_CHEATS = [
    'allrecipes',
  ];

  readonly gameState: GameState;

  readonly objectSpawner: ObjectSpawner;

  readonly narrator: Narrator;

  readonly puzzleHandler: PuzzleHandler;

  readonly wishingWandPuzzles: Puzzle<unknown>[];

  readonly randomizerInt: (max: number) => number;

  readonly onActionTaken: () => void;

  constructor(
    gameState: GameState,
    objectSpawner: ObjectSpawner,
    narrator: Narrator,
    puzzleHandler: PuzzleHandler,
    wishingWandPuzzles: Puzzle<unknown>[],
    randomizerInt: (max: number) => number,
    onActionTaken: () => void,
  ) {
    this.gameState = gameState;
    this.objectSpawner = objectSpawner;
    this.narrator = narrator;
    this.puzzleHandler = puzzleHandler;
    this.wishingWandPuzzles = wishingWandPuzzles;
    this.randomizerInt = randomizerInt;
    this.onActionTaken = onActionTaken;
  }

  wishFor(wandX: number, wandY: number, wishFor: string) {
    this.gameState.savable.wandWishCount += 1;

    if (wishFor === 'coin') {
      const howMany = parseInt(prompt('How many coins?') ?? '1', 10) ?? 1;

      if (howMany > 1) {
        const fulfilled = (howMany > WandHandler.MAX_COINS_PER_WISH ? WandHandler.MAX_COINS_PER_WISH : howMany);

        this.gameState.savable.backpack.add('coin', fulfilled);

        const drops = new Map<string, number>();
        drops.set('coin', fulfilled);
        this.narrator.narrateFromAction(this.gameState, 'wandWishSound', ['coin'], drops);
        this.onActionTaken();
        return;
      }
    }

    if (wishFor === 'allrecipes') {
      this.gameState.objectRecipes.forEach((__recipe, recipeName) => {
        const objClassProperties = this.gameState.objectClassProperties.get(recipeName) as GameObjectProperties;

        if (objClassProperties.isEdible) {
          if (!this.gameState.savable.discoveredRecipes.cooking.includes(recipeName)) {
            this.gameState.savable.discoveredRecipes.cooking.push(recipeName);
          }
        } else if (!this.gameState.savable.discoveredRecipes.crafting.includes(recipeName)) {
          this.gameState.savable.discoveredRecipes.crafting.push(recipeName);
        }
      });

      this.narrator.narrateFromAction(this.gameState, 'wandWishSound', ['all recipes']);
      this.onActionTaken();
      return;
    }

    const obj = this.objectSpawner.makeObject(
      wishFor,
      wandX + this.randomizerInt(60) - 30,
      wandY + this.randomizerInt(60) - 30,
    );

    this.narrator.narrateFromAction(this.gameState, 'wandWishSound', obj);
    this.onActionTaken();
  }

  activate(wandX: number, wandY: number) {
    let wishFor: string = prompt('What do you wish for?')?.toLowerCase() ?? 'nothing';
    if (!wishFor || wishFor.trim() === '') {
      wishFor = 'nothing';
    }

    // Map plural to valid object, at least for coins
    if (wishFor === 'coins') {
      wishFor = 'coin';
    }

    // Don't bother activating a puzzle if we wished for something that doesn't exist.
    if (!this.gameState.objectDictionary.has(wishFor) && !WandHandler.WAND_CHEATS.includes(wishFor)) {
      const targetStartsWithVowel = Narrator.VOWELS.includes(wishFor[0].toLowerCase());

      let wishForDescription: string;
      if (wishFor === 'nothing') {
        wishForDescription = 'nothing';
      } else {
        wishForDescription = `${targetStartsWithVowel ? 'an' : 'a'} ${wishFor}`;
      }

      const message = `${this.gameState.savable.playerName} wished for ${wishForDescription}, and nothing happened.`;
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      this.narrator.configureNarratorBox(message);
      return;
    }

    // We wished for something that exists, so we should do a puzzle first.
    const randomPuzzle = this.wishingWandPuzzles[this.randomizerInt(this.wishingWandPuzzles.length)];
    if (!this.puzzleHandler.activatePuzzle(randomPuzzle, 'the wishing wand', () => this.wishFor(wandX, wandY, wishFor))) {
      // If the puzzle didn't activate (invalid puzzle), activate the wish anyway.
      this.wishFor(wandX, wandY, wishFor);
    }
  }
}

export default WandHandler;
