// It's not clear why ESLint dislikes for await (const x of y) syntax; for consuming async iterable streams...
/* eslint-disable no-restricted-syntax */
// TODO: This probably should be fixed; we're using prompt for easy text input for player name, but we probably want a better UI for this?
/* eslint-disable no-alert */
import ldb from 'localdata';
import { streamText } from 'ai';
import { chromeai } from 'chrome-ai';

import './style.css';
import gameConfig from './objects.json';

import checkEnv from './checkSupport';
import DiscoveredRecipes from './models/discoveredrecipes';
import Recipe from './models/recipe';
import GameObjectDefinition from './models/gameobjectdefinition';
import GameObjectProperties from './models/gameobjectproperties';
import { GameObject, SerializableGameObject } from './models/gameobject';
import Drop from './models/drop';
import Quest from './models/quest';
import DoneCriteria from './models/donecriteria';
import Backpack from './models/backpack';
import Puzzle from './models/puzzles/puzzle';
import GameState from './models/gamestate';

import Narrator from './narrator';
import Painter from './painter';
import { CountType, RestoredGameState } from './models/savablegamestate';
import ObjectSpawner, { SpawnConfig } from './objectspawner';
import BoundingBox from './models/boundingbox';
import BackpackController, { RemoveType } from './backpackcontroller';
import NavigationHandler from './navigationhandler';
import PointerHandler from './pointerhandler.js';
import PuzzleHandler from './puzzlehandler';
import ShopHandler from './shophandler';
import WandHandler from './wandhandler';

interface ArtAppWindow extends Window {
  resizeCanvasToFitBounds(width: number, height: number): void;
  isEmptyCanvas(): boolean;
  drawOnCanvas(img: ImageBitmap): void;
}

// If we have a compatibility-breaking change we will rev this up. Rare, but the move to TS switched a few things over.
const STORAGE_KEY = 'playground_state_v2';

let spawnerInterval: number;
let saveInterval: number;

const gameState: GameState = new GameState();

// Details loaded from config. Could be turned into an object

let initialObjects: string[] = [];
let spawnConfig: SpawnConfig;
let questTemplates: Quest[] = [];

let helpTips: string[] = [];

let activeStateToSave = 0;
let currentSavedState = 0;

// Shorthands to important DOM elements.
let canvas: HTMLCanvasElement;
let ctx: CanvasRenderingContext2D;
let canvasBack: HTMLCanvasElement;
// let ctx_back: CanvasRenderingContext2D;
let canvasFront: HTMLCanvasElement;
// let ctx_front: CanvasRenderingContext2D;

let grassBackground: HTMLImageElement;
let saveSpinner: HTMLElement;
let canvasContainer: HTMLElement;
let narratorBox: HTMLElement;
let talkMessageBox: HTMLElement;
let closeapps: HTMLButtonElement;
let closeshop: HTMLButtonElement;
let togglebackpack: HTMLButtonElement;
let togglelog: HTMLButtonElement;
let backpackview: HTMLElement;
let shopview: HTMLElement;
let logbook: HTMLElement;
let apps: HTMLElement;
let musicApp: HTMLIFrameElement;
let artApp: HTMLIFrameElement;
let puzzleApp: HTMLElement;

// set to the painting object we're actively editing (when there is one), so once we close apps we'll update it.
let activePainting: GameObject | undefined;

// Record storage used for debugging purpoes, if we run into issues with recording local drawing state
// To make it less noisy we won't log unless it's above 50%.
navigator.storage.estimate().then((quota: StorageEstimate) => {
  if (!quota.usage || !quota.quota) {
    return;
  }

  const percentageUsed = (quota.usage / quota.quota) * 100;
  if (percentageUsed > 50) {
    console.log(`You've used ${percentageUsed}% of the available storage.`);
  }
}, () => {
  // Unable to determine how much storage we have left...
});

const canInvokeLLMCheck = async () => {
  try {
    await checkEnv();

    return true;
  } catch (e) {
    console.warn(e);
    return false;
  }
};

const invokeLLM = async (prompt: string, terse = false) => {
  const { textStream } = await streamText({
    model: chromeai('text', {}),
    system: terse ? 'respond in 10 words or less' : '',
    prompt,
  });

  let messages: string[] = [];
  for await (const textPart of textStream) {
    messages = [...messages, textPart];
  }

  return messages.join('');
};

let OSName = 'Unknown OS';
let OSVersion = '';
let resolvePlatform;
const isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

if (isFirefox) {
  resolvePlatform = Promise.resolve();
} else {
  resolvePlatform = navigator.userAgentData?.getHighEntropyValues(['platformVersion']).then((ua) => {
    OSName = ua.platform ?? 'Unknown OS';

    const [version] = ua.platformVersion?.split('.') ?? ['0'];
    const majorPlatformVersion = parseInt(version, 10);
    if (majorPlatformVersion >= 13) {
      OSVersion = '11+';
    } else {
      OSVersion = '10-';
    }
  }, () => {
    // Can't resolve the OS version on Windows...
  });
}

let narrator: Narrator;

function getRandomInt(max: number) {
  if (max <= 1) {
    return 0;
  }

  return Math.floor(Math.random() * max);
}

function pointInPath(object: GameObject, x: number, y: number) {
  return ctx.isPointInPath(object.path, x, y);
}

let objectSpawner: ObjectSpawner;
let painter: Painter;
let backpackController: BackpackController;
let pointerHandler: PointerHandler;
let navigationHandler: NavigationHandler;
let puzzleHandler: PuzzleHandler;
let shopHandler: ShopHandler;

function configureLogForReading(logHTML: string) {
  narrator.configureForReading(logbook, logHTML, '#log > #pattern > .content', '#log > #pattern > .content p');
}

interface SerializedMap {
  dataType: string;
  value: [unknown, unknown][];
}

function isSerializedMap(value: object | SerializedMap): value is SerializedMap {
  return (<SerializedMap>value).dataType === 'Map';
}

// ((this: any, key: string, value: any) => any)
function replacer(__key: string, value: object) {
  if (value instanceof Map) {
    return {
      dataType: 'Map',
      value: Array.from(value.entries()), // or with spread: value: [...value]
    } as SerializedMap;
  }
  return value;
}
function reviver(__key: string, value: object | SerializedMap) {
  if (value !== null && isSerializedMap(value)) {
    return new Map(value.value);
  }
  return value;
}

function save() {
  // if our active state id gets too high, reverse back to the beginning
  if (activeStateToSave >= Number.MAX_SAFE_INTEGER) {
    currentSavedState = 0;
    activeStateToSave = 1;
  }

  saveSpinner.style.visibility = 'hidden';
  if (activeStateToSave <= currentSavedState) {
    return;
  }

  console.log(`Saving state ${activeStateToSave}`);
  currentSavedState = activeStateToSave;

  const serializable = {
    ...gameState.savable,
    // Wipe fields that are not clonable or serializable, e.g. Path2D and other derived fields
    objects: gameState.savable.objects.map((obj: GameObject) => obj.ser),
  } as RestoredGameState;

  const snapshot = structuredClone(serializable) as RestoredGameState;

  const serializedState = JSON.stringify(snapshot, replacer);

  ldb.set(STORAGE_KEY, serializedState);
}

function load(noSavedStateCallback: () => Promise<void>, alwaysCallback: () => Promise<void>) {
  ldb.get(STORAGE_KEY, (value: string) => {
    const loadedState = JSON.parse(value, reviver) as RestoredGameState;
    if (!loadedState) {
      noSavedStateCallback().then(alwaysCallback).finally(() => {});
      return;
    }

    // Load these efore creating objects, otherwise the wrong paths will be associated.
    if (loadedState.globalXOffset) {
      gameState.savable.globalXOffset = loadedState.globalXOffset;
    }

    if (loadedState.globalYOffset) {
      gameState.savable.globalYOffset = loadedState.globalYOffset;
    }

    loadedState.objects.forEach((obj) => {
      objectSpawner.makeObjectFromSerialized(obj);
    });

    if (loadedState.backpack) {
      // the class isn't restored when JSON.parse casting is used; loadedState.backpack is an Object with these fields, instead of a Backpack with these fields. Maybe can be fixed using the reviver, but this is fine too.
      gameState.savable.backpack = new Backpack(loadedState.backpack.resources, loadedState.backpack.statefulObjects);
    }

    if (loadedState.discoveredRecipes) {
      gameState.savable.discoveredRecipes = loadedState.discoveredRecipes;
    }

    if (loadedState.playerName) {
      gameState.savable.playerName = loadedState.playerName;
    }
    if (loadedState.peopleNamesBag) {
      gameState.savable.peopleNamesBag = loadedState.peopleNamesBag;
    }

    if (loadedState.counts && loadedState.counts.size > 0) {
      // new object counts will be merged in at 0, loaded state will update counts of existing objects.
      gameState.savable.counts = new Map([...gameState.savable.counts, ...loadedState.counts]);
    }

    if (loadedState.defeatedCounts && loadedState.defeatedCounts.size > 0) {
      // new object defeated counts will be merged in at 0, loaded state will update defeated counts of existing enemies.
      gameState.savable.defeatedCounts = new Map([...gameState.savable.defeatedCounts, ...loadedState.defeatedCounts]);
    }

    if (loadedState.questCounts && loadedState.questCounts.size > 0) {
      // new quest counts will be merged in at 0, loaded state will update quest counts of existing quests.
      gameState.savable.questCounts = new Map([...gameState.savable.questCounts, ...loadedState.questCounts]);
    }

    if (loadedState.cookedCounts && loadedState.cookedCounts.size > 0) {
      // new cooked recipe counts will be merged in at 0, loaded state will update cooked recipe counts of existing recipes.
      gameState.savable.cookedCounts = new Map([...gameState.savable.cookedCounts, ...loadedState.cookedCounts]);

      // Populate discovered cooked recipes from cooked counts if they are missing.
      gameState.savable.cookedCounts.forEach((cookedCount, recipeName) => {
        if (cookedCount > 0 && !gameState.savable.discoveredRecipes.cooking.includes(recipeName)) {
          gameState.savable.discoveredRecipes.cooking.push(recipeName);
        }
      });
    }

    if (loadedState.craftedCounts && loadedState.craftedCounts.size > 0) {
      gameState.savable.craftedCounts = new Map([...gameState.savable.craftedCounts, ...loadedState.craftedCounts]);
    }

    if (loadedState.wandWishCount) {
      gameState.savable.wandWishCount = loadedState.wandWishCount;
    }

    alwaysCallback().finally(() => {});
  });
}

function ready(callback: () => Promise<void>) {
  // in case the document is already rendered
  if (document.readyState !== 'loading') callback().finally(() => {});
  // modern browsers
  else if (document.addEventListener) {
    document.addEventListener('DOMContentLoaded', () => {
      callback().finally(() => {});
    });
  }
}

function loadGameConfig() {
  const data = gameConfig;

  initialObjects = data.startConfig.initialObjects;
  spawnConfig = data.spawnConfig;
  questTemplates = data.questTemplates.filter((quest) => quest.doneCriteria.defeated || quest.doneCriteria.bring
  || (quest.doneCriteria.puzzle && PuzzleHandler.SUPPORTED_PUZZLES.includes(quest.doneCriteria.puzzle.name))).map((v) => new Quest(
    v.name,
    v.narrative,
    v.dialogueStart,
    v.dialogueComplete,
    new DoneCriteria(
      new Map(v.doneCriteria.bring?.map((obj) => [obj.key, obj.value])),
      new Map(v.doneCriteria.defeated?.map((obj) => [obj.key, obj.value])),
      v.doneCriteria.puzzle as Puzzle<unknown>,
    ),
    new Map<string, Drop[]>(Object.entries(v.drops)),
  ));
  helpTips = data.helpTips;

  questTemplates.forEach((quest) => {
    gameState.savable.questCounts.set(quest.name, 0);
  });

  data.tools.forEach((obj: GameObjectDefinition) => {
    gameState.savable.counts.set(obj.name, 0);
    gameState.objectDictionary.set(obj.name, obj);

    if (obj.recipe && obj.madeIn) {
      gameState.objectRecipes.set(obj.name, new Recipe(
        obj.recipe,
        obj.madeIn,
        obj.recipeHints ?? [],
      ));

      if (obj.madeIn?.includes('crafting')) {
        gameState.savable.craftedCounts.set(obj.name, 0);
      }
    }

    gameState.objectClassProperties.set(obj.name, {
      isMovable: true,
      isTool: true,
    } as GameObjectProperties);
  });

  data.weapons.forEach((obj: GameObjectDefinition) => {
    gameState.savable.counts.set(obj.name, 0);
    gameState.objectDictionary.set(obj.name, obj);

    if (obj.recipe && obj.madeIn) {
      gameState.objectRecipes.set(obj.name, new Recipe(
        obj.recipe,
        obj.madeIn,
        obj.recipeHints ?? [],
      ));

      if (obj.madeIn?.includes('crafting')) {
        gameState.savable.craftedCounts.set(obj.name, 0);
      }
    }

    gameState.objectClassProperties.set(obj.name, {
      isMovable: true,
      isTool: true,
    } as GameObjectProperties);
  });

  data.people.forEach((obj: GameObjectDefinition) => {
    gameState.savable.counts.set(obj.name, 0);
    gameState.objectDictionary.set(obj.name, obj);
    gameState.objectClassProperties.set(obj.name, {
      isPerson: true,
      isMovable: false,
    } as GameObjectProperties);
  });

  data.animals.forEach((obj: GameObjectDefinition) => {
    gameState.savable.counts.set(obj.name, 0);
    gameState.objectDictionary.set(obj.name, obj);
    gameState.objectClassProperties.set(obj.name, {
      isAnimal: true,
      isMovable: false,
    } as GameObjectProperties);
  });

  data.enemies.forEach((obj: GameObjectDefinition) => {
    gameState.savable.counts.set(obj.name, 0);
    gameState.savable.defeatedCounts.set(obj.name, 0);
    gameState.objectDictionary.set(obj.name, obj);
    gameState.objectClassProperties.set(obj.name, {
      isEnemy: true,
      isMovable: false,
    } as GameObjectProperties);
  });

  data.shops.forEach((obj: GameObjectDefinition) => {
    gameState.savable.counts.set(obj.name, 0);
    gameState.objectDictionary.set(obj.name, obj);

    gameState.objectClassProperties.set(obj.name, {
      isMovable: false,
      isShop: true,
    } as GameObjectProperties);
  });

  data.clothes.forEach((obj: GameObjectDefinition) => {
    gameState.savable.counts.set(obj.name, 0);
    gameState.objectDictionary.set(obj.name, obj);

    if (obj.recipe && obj.madeIn) {
      gameState.objectRecipes.set(obj.name, new Recipe(
        obj.recipe,
        obj.madeIn,
        obj.recipeHints ?? [],
      ));

      if (obj.madeIn?.includes('crafting')) {
        gameState.savable.craftedCounts.set(obj.name, 0);
      }
    }

    gameState.objectClassProperties.set(obj.name, {
      isMovable: true,
      isClothing: true,
    } as GameObjectProperties);
  });

  data.furniture.forEach((obj: GameObjectDefinition) => {
    gameState.savable.counts.set(obj.name, 0);
    gameState.objectDictionary.set(obj.name, obj);

    if (obj.recipe && obj.madeIn) {
      gameState.objectRecipes.set(obj.name, new Recipe(
        obj.recipe,
        obj.madeIn,
        obj.recipeHints ?? [],
      ));

      if (obj.madeIn?.includes('crafting')) {
        gameState.savable.craftedCounts.set(obj.name, 0);
      }
    }

    if (obj.purchasedAt && obj.price) {
      obj.purchasedAt.forEach((store) => {
        if (!gameState.shopInventories.has(store)) {
          gameState.shopInventories.set(store, []);
        }

        gameState.shopInventories.get(store)?.push(obj.name);
      });
    }

    gameState.objectClassProperties.set(obj.name, {
      isMovable: true,
      isFurniture: true,
    } as GameObjectProperties);
  });

  data.natural_resources.forEach((obj: GameObjectDefinition) => {
    gameState.savable.counts.set(obj.name, 0);
    gameState.objectDictionary.set(obj.name, obj);
    gameState.objectClassProperties.set(obj.name, {
      // not entirely true, but for now this is good enough; we aren't growing trees yet.
      isGrowable: obj.isGrowable,
      isMovable: false,
    } as GameObjectProperties);
  });

  data.plants.forEach((obj: GameObjectDefinition) => {
    gameState.savable.counts.set(obj.name, 0);
    gameState.objectDictionary.set(obj.name, obj);
    gameState.objectClassProperties.set(obj.name, {
      isGrowable: true,
      isMovable: true,
    } as GameObjectProperties);
  });

  data.food.forEach((obj: GameObjectDefinition) => {
    gameState.savable.counts.set(obj.name, 0);
    gameState.objectDictionary.set(obj.name, obj);

    if (obj.recipe && obj.madeIn) {
      gameState.objectRecipes.set(obj.name, new Recipe(
        obj.recipe,
        obj.madeIn,
        obj.recipeHints ?? [],
      ));

      gameState.savable.cookedCounts.set(obj.name, 0);
    }

    if (obj.purchasedAt && obj.price) {
      obj.purchasedAt.forEach((store) => {
        if (!gameState.shopInventories.has(store)) {
          gameState.shopInventories.set(store, []);
        }

        gameState.shopInventories.get(store)?.push(obj.name);
      });
    }

    gameState.objectClassProperties.set(obj.name, {
      isEdible: true,
      isMovable: true,
    } as GameObjectProperties);
  });

  // Liquids are unique in that they exist as an ingredient in cooking implements or held in buckets, but otherwise don't occupy a tangible object slot.
  // For a while I didn't model these as objects, but I've started doing so, so I can keep track of certain shared properties, e.g. their rendering text representation (or image)
  data.liquids.forEach((obj: GameObjectDefinition) => {
    gameState.objectDictionary.set(obj.name, obj);
    gameState.objectClassProperties.set(obj.name, {
      isHeld: true,
    } as GameObjectProperties);
  });

  data.resources.forEach((obj: GameObjectDefinition) => {
    gameState.savable.counts.set(obj.name, 0);
    gameState.objectDictionary.set(obj.name, obj);

    if (obj.recipe && obj.madeIn) {
      gameState.objectRecipes.set(obj.name, new Recipe(
        obj.recipe,
        obj.madeIn,
        obj.recipeHints ?? [],
      ));

      gameState.savable.cookedCounts.set(obj.name, 0);
    }

    if (obj.purchasedAt && obj.price) {
      obj.purchasedAt.forEach((store) => {
        if (!gameState.shopInventories.has(store)) {
          gameState.shopInventories.set(store, []);
        }

        gameState.shopInventories.get(store)?.push(obj.name);
      });
    }

    gameState.objectClassProperties.set(obj.name, {
      isGrowable: false,
      isMovable: true,
    });
  });
}

function beginningObjects() {
  let spawnX = 20;
  let spawnY = 50;
  let totalObjects = 0;

  initialObjects.forEach((objName: string) => {
    objectSpawner.makeObject(objName, spawnX, spawnY);

    totalObjects += 1;
    spawnY += 70;

    // add another 'column' to spawn objects for every 5 we see, currently.
    if (totalObjects % 5 === 0 && totalObjects > 0) {
      spawnX += 100;
      spawnY = 50;
    }
  });
}

function showSaveSpinnerIfNecessary() {
  if (activeStateToSave > currentSavedState) {
    saveSpinner.style.visibility = 'visible';
  }
}

function resizeCanvasToFitScreen() {
  canvas.width = window.innerWidth - 20;
  canvas.height = window.innerHeight - 40;
  canvasBack.width = canvas.width;
  canvasBack.height = canvas.height;
  canvasFront.width = canvas.width;
  canvasFront.height = canvas.height;

  canvasContainer.style.width = `${canvas.width}px`;
  canvasContainer.style.height = `${canvas.height}px`;

  painter.drawAllObjects(grassBackground);
}

function objectSpawnHandler() {
  return objectSpawner.onSpawnTick(() => {
    activeStateToSave += 1;
    showSaveSpinnerIfNecessary();
    painter.drawAllObjects();
  });
}

function setMoneyAvailable() {
  const moneyAvailable = gameState.savable.backpack.resources.get('coin') ?? 0;
  const moneyAvailableText = `<p>Money available: ${moneyAvailable} 🪙</p>`;
  narrator.configureForReading(document.body, moneyAvailableText, '.moneyavailable', '.moneyavailable p');
}

// eslint-disable-next-line @typescript-eslint/require-await
async function onReady() {
  grassBackground = document.querySelector('#grassBackground') as HTMLImageElement;
  saveSpinner = document.querySelector('#saveTracker') as HTMLElement;
  canvasContainer = document.querySelector('#canvas_container') as HTMLElement;
  canvas = document.querySelector('#canvas') as HTMLCanvasElement;
  canvasFront = document.querySelector('#canvas_front') as HTMLCanvasElement;
  canvasBack = document.querySelector('#canvas_back') as HTMLCanvasElement;
  ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
  // ctx_front = canvas_front.getContext("2d")!;
  // ctx_back = canvas_back.getContext("2d")!;

  // For Earlier Windows versions, make up for the lack of up-to-date emoji.
  const emojisNeedFill = (OSName === 'Windows' && OSVersion !== '11+' && !isFirefox);
  painter = new Painter(gameState, canvas, ctx, emojisNeedFill);

  resizeCanvasToFitScreen();

  window.addEventListener('resize', () => {
    resizeCanvasToFitScreen();
    shopHandler.organizeShopIfActive();
  });

  narratorBox = document.querySelector('#narrator') as HTMLElement;

  // The Windows default TTS appears to be a bit fast so we'll slow it down.
  // On Chromebooks, the default TTS is slower and 40% makes it too slow. But 100% appears to be too fast. We'll try 75% next.
  // Haven't tested with Apple's OSs yet.
  const utterrate = (OSName === 'Windows' && !isFirefox ? 0.40 : 0.75);
  narrator = new Narrator(utterrate, narratorBox);

  talkMessageBox = document.querySelector('#talkMessageBox') as HTMLElement;
  closeapps = document.querySelector('#closeapps') as HTMLButtonElement;
  closeshop = document.querySelector('#closeshop') as HTMLButtonElement;
  togglebackpack = document.querySelector('#togglebackpack') as HTMLButtonElement;
  togglelog = document.querySelector('#togglelog') as HTMLButtonElement;
  backpackview = document.querySelector('#backpackview') as HTMLElement;
  shopview = document.querySelector('#shopview') as HTMLElement;
  logbook = document.querySelector('#log') as HTMLElement;

  talkMessageBox.querySelector('.closeMessage')?.addEventListener('click', (e) => {
    pointerHandler.talkingToSomeone = undefined;
    pointerHandler.activeConversation = undefined;
    narrator.configureMessageBox(talkMessageBox, '');
    e.preventDefault();
  });

  saveSpinner.style.visibility = 'hidden';
  logbook.style.display = 'none';
  backpackview.style.display = 'none';
  shopview.style.display = 'none';

  apps = document.querySelector('#apps') as HTMLElement;
  apps.style.display = 'none';

  musicApp = document.querySelector('#music') as HTMLIFrameElement;
  artApp = document.querySelector('#art') as HTMLIFrameElement;
  puzzleApp = document.querySelector('#puzzle') as HTMLElement;

  puzzleHandler = new PuzzleHandler(
    narrator,
    gameConfig.peoplenames,
    gameState,
    apps,
    puzzleApp,
    getRandomInt,
    () => {
      closeapps.click();
    },
  );

  shopHandler = new ShopHandler(
    narrator,
    gameState,
    shopview,
    () => {
      setMoneyAvailable();
      activeStateToSave += 1;
      showSaveSpinnerIfNecessary();
    },
    getRandomInt,
  );

  closeapps.addEventListener('click', () => {
    apps.style.display = 'none';
    musicApp.style.display = 'none';
    puzzleApp.style.display = 'none';

    if (activePainting && artApp.contentWindow) {
      const artDoc = artApp.contentDocument ?? artApp.contentWindow.document;
      const artCanvas = artDoc.querySelector('#sketchpad') as HTMLCanvasElement;

      // If clearing the art canvas, clear the custom state as well (will revert to the default presentation of the object)
      if ((artApp.contentWindow as ArtAppWindow).isEmptyCanvas()) {
        painter.deleteFromImageCache(activePainting.ser.customState.id);
        activePainting.ser.customState.paintingImageUrl = undefined;
        ldb.delete(`playground-${activePainting.ser.customState.id}states`);
        ldb.delete(`playground-${activePainting.ser.customState.id}penState`);
      } else {
        activePainting.ser.customState.paintingImageUrl = artCanvas.toDataURL();
      }

      painter.drawAllObjects();
    }

    activePainting = undefined;
    artApp.src = '';
    artApp.style.display = 'none';
  });

  closeshop.addEventListener('click', () => {
    if (!shopHandler.activeShop) {
      return;
    }

    Narrator.playSound('shopVisitSound');

    const message = `${gameState.savable.playerName} left the ${shopHandler.activeShop.getDisplayName()}.`;
    narrator.configureNarratorBox(message);

    shopHandler.activeShop = undefined;
    shopview.style.display = 'none';
  });

  (document.querySelector('#quests') as HTMLButtonElement).addEventListener('click', (e) => {
    // We'll skip narration for the synthetic click event we use to start at this page when we open the logbook. Unique to this tab.
    if (e.isTrusted) {
      Narrator.playSound('toggleLogSound');

      const message = `${gameState.savable.playerName} opened the quest log in the logbook.`;
      narrator.configureNarratorBox(message);
    }

    let logHTML = '';

    let haveAQuest = false;
    logHTML += '<p>Current quests:</p>';
    gameState.savable.objects.filter((obj: GameObject) => obj.properties.isPerson
      && obj.ser.quests && obj.ser.quests.length > 0).forEach((obj: GameObject) => {
      const [theQuest] = obj.ser.quests;

      if (theQuest.state === 'given') {
        haveAQuest = true;

        logHTML += `<p style='margin-left:30px;'>Active quest from ${obj.ser.givenName ?? 'person'}: ${theQuest.narrative}<br />\
          Current State: `;

        let allBrought = true;
        if (theQuest.doneCriteria.bring && theQuest.doneCriteria.bring.size > 0) {
          theQuest.doneCriteria.bring.forEach((count, objName) => {
            if (count > 0) {
              allBrought = false;
              logHTML += `Bring ${count} more ${objName} to the villager.`;
            }
          });
        }

        let allDefeated = true;
        if (theQuest.doneCriteria.defeated && theQuest.doneCriteria.defeated.size > 0) {
          theQuest.doneCriteria.defeated.forEach((count, enemyName) => {
            const enemiesLeft = count - (gameState.savable.defeatedCounts.get(enemyName) ?? 0);
            if (enemiesLeft > 0) {
              allDefeated = false;
              logHTML += `Defeat ${enemiesLeft} more ${enemyName}`;
            }
          });
        }

        if (allBrought && allDefeated) {
          logHTML += 'Done. Talk to the villager to finish.';
        }

        logHTML += '</p>';
      }
    });

    if (!haveAQuest) {
      logHTML += '<p>You currently have no active quests. Some villagers may need help!</p>';
    }

    questTemplates.forEach((questTemplate) => {
      logHTML += `<p>Completed ${gameState.savable.questCounts.get(questTemplate.name) ?? 0}: ${questTemplate.narrative}</p>`;
    });

    logHTML += `<p>Completed Wand Wishes: ${gameState.savable.wandWishCount}</p>`;

    configureLogForReading(logHTML);
  });
  (document.querySelector('#cooking') as HTMLButtonElement).addEventListener('click', () => {
    Narrator.playSound('toggleLogSound');

    const message = `${gameState.savable.playerName} opened the cookbook in the logbook.`;
    narrator.configureNarratorBox(message);

    let logHTML = '';
    gameState.savable.cookedCounts.forEach((cookedCount, recipeName) => {
      const objectProperties = gameState.objectDictionary.get(recipeName) as GameObjectDefinition;
      const recipeDisplayName = GameObjectDefinition.getDisplayName(objectProperties, recipeName);
      const recipeRepresentation = (objectProperties.imageRepresentation ? `<img src='${objectProperties.imageRepresentation}' width='18' height='18' alt='${recipeDisplayName}' />` : objectProperties.textRepresentation);

      const fullRecipe = gameState.objectRecipes.get(recipeName) as Recipe;

      const recipe = !gameState.savable.discoveredRecipes.cooking.includes(recipeName) ? 'Unknown'
        : `${fullRecipe.recipe.filter((i) => i && i !== '').map((ingredientName) => {
          const ingredientProperties = gameState.objectDictionary.get(ingredientName) as GameObjectDefinition;
          const ingredientDisplayName = GameObjectDefinition.getDisplayName(ingredientProperties, ingredientName);
          const ingredientRepresentation = (ingredientProperties.imageRepresentation ? `<img src='${ingredientProperties.imageRepresentation}' width='18' height='18' alt='${ingredientDisplayName}' />` : ingredientProperties.textRepresentation);

          return `${ingredientDisplayName} ${ingredientRepresentation}`;
        }).join(' + ')} +\
        ${fullRecipe.madeIn.map((cookingImplement) => {
    if (cookingImplement === 'crafting') {
      return 'crafting 🔨';
    }

    const cookingImplProperties = gameState.objectDictionary.get(cookingImplement) as GameObjectDefinition;
    const cookingImplDisplayName = GameObjectDefinition.getDisplayName(cookingImplProperties, cookingImplement);
    const cookingImplRepresentation = (cookingImplProperties.imageRepresentation ? `<img src='${cookingImplProperties.imageRepresentation}' width='18' height='18' alt='${cookingImplDisplayName}' />` : cookingImplProperties.textRepresentation);

    return `${cookingImplDisplayName} ${cookingImplRepresentation}`;
  }).join(' or ')} = ${recipeDisplayName} ${recipeRepresentation}`;

      logHTML += `<p>Made ${cookedCount}: ${recipeDisplayName} ${recipeRepresentation}. Recipe: ${recipe}</p>`;
    });

    configureLogForReading(logHTML);
  });
  (document.querySelector('#craftinglog') as HTMLButtonElement).addEventListener('click', () => {
    Narrator.playSound('toggleLogSound');

    const message = `${gameState.savable.playerName} opened the crafting record in the logbook.`;
    narrator.configureNarratorBox(message);

    let logHTML = '';
    gameState.savable.craftedCounts.forEach((craftedCount, recipeName) => {
      const objectProperties = gameState.objectDictionary.get(recipeName) as GameObjectDefinition;
      const recipeDisplayName = GameObjectDefinition.getDisplayName(objectProperties, recipeName);

      const recipe = !gameState.savable.discoveredRecipes.crafting.includes(recipeName) ? 'Unknown (see hints when crafting)' : 'Known (view when crafting)';
      const recipeRepresentation = (objectProperties.imageRepresentation ? `<img src='${objectProperties.imageRepresentation}' width='18' height='18' alt='${recipeDisplayName}' />` : objectProperties.textRepresentation);

      logHTML += `<p>Made ${craftedCount}: ${recipeDisplayName} ${recipeRepresentation}. Recipe: ${recipe}</p>`;
    });

    configureLogForReading(logHTML);
  });

  (document.querySelector('#enemies') as HTMLButtonElement).addEventListener('click', () => {
    Narrator.playSound('toggleLogSound');

    const message = `${gameState.savable.playerName} opened the bestiary in the logbook.`;
    narrator.configureNarratorBox(message);

    let logHTML = '';
    gameState.savable.defeatedCounts.forEach((defeatedCount, enemyName) => {
      const objectProperties = gameState.objectDictionary.get(enemyName) as GameObjectDefinition;
      const enemyDisplayName = GameObjectDefinition.getDisplayName(objectProperties, enemyName);

      logHTML += `<p>Defeated ${defeatedCount}: ${enemyDisplayName} ${objectProperties.textRepresentation}</p>`;
    });

    configureLogForReading(logHTML);
  });
  (document.querySelector('#help') as HTMLButtonElement).addEventListener('click', () => {
    Narrator.playSound('toggleLogSound');

    const message = `${gameState.savable.playerName} turned to the help page in the logbook.`;
    narrator.configureNarratorBox(message);

    configureLogForReading(helpTips.map((tip) => `<p>${tip}</p>`).join(''));
  });

  (document.querySelector('#items') as HTMLButtonElement).addEventListener('click', (e) => {
    // We'll skip narration for the synthetic click event we use to start at this page when we open the backpack. Unique to this tab.
    if (e.isTrusted) {
      Narrator.playSound('toggleBackpackSound');

      const message = `${gameState.savable.playerName === 'You' ? 'You are' : `${gameState.savable.playerName} is`} looking at items in the backpack.`;
      narrator.configureNarratorBox(message);
    }

    (document.querySelector('.craftingview') as HTMLElement).style.display = 'none';

    backpackController.updateBackpackView(true, false);

    backpackview.querySelectorAll('#backpackview button.eatItemButton').forEach((button) => {
      button.addEventListener('click', (ev) => backpackController.removeItemFromBackpack(ev, RemoveType.Eat));
    });

    backpackview.querySelectorAll('#backpackview button.dropItemButton').forEach((button) => {
      button.addEventListener('click', (ev) => backpackController.removeItemFromBackpack(ev, RemoveType.Drop));
    });

    backpackview.querySelectorAll('#backpackview button.dropStatefulItemButton').forEach((button) => {
      button.addEventListener('click', (ev) => backpackController.removeItemFromBackpack(ev, RemoveType.DropStateful));
    });
  });

  (document.querySelector('#crafting') as HTMLButtonElement).addEventListener('click', () => {
    Narrator.playSound('toggleBackpackSound');

    const message = `${gameState.savable.playerName === 'You' ? 'You are' : `${gameState.savable.playerName} is`} considering crafting from the backpack.`;
    narrator.configureNarratorBox(message);

    (document.querySelector('.craftingview') as HTMLElement).style.display = 'flex';
    (document.querySelector('.craftingview') as HTMLElement).style.height = '400px';

    let craftingHTML = '';
    for (let i = 1; i <= 20; i += 1) {
      craftingHTML += `<div class="craftingslot" id="slot${i}" draggable="true">&nbsp;</div>`;

      if (i % 5 === 0) {
        craftingHTML += '<br />';
      }
    }

    craftingHTML += '<button id=\'clearcraftingslot\'>❌</button>\
      <div id=\'craftingresults\'>...</div>\
      <button id=\'craftbutton\'><span class=\'icon\'>🔨</span></button>';

    backpackController.configureCraftingRecipes();

    const craftingslots = document.querySelector('.craftingslots') as HTMLElement;
    craftingslots.innerHTML = craftingHTML;
    (document.querySelector('#craftbutton') as HTMLButtonElement).disabled = true;

    craftingslots.querySelectorAll('.craftingslot').forEach((el) => {
      const ele = el as HTMLElement;
      ele.addEventListener('drop', (ev) => backpackController.drop(ev));
      ele.addEventListener('dragover', (ev) => BackpackController.allowDrop(ev));
      ele.addEventListener('dragstart', (ev) => BackpackController.dragStart(ev));
    });

    const clearcraftingslot = document.querySelector('#clearcraftingslot') as HTMLElement;
    clearcraftingslot.addEventListener('click', () => {
      for (let i = 0; i < 20; i += 1) {
        const slot = document.getElementById(`slot${i + 1}`) as Element;
        slot.setAttribute('itemName', '');
        slot.innerHTML = '&nbsp;';
      }

      backpackController.evaluateCrafting();
    });
    clearcraftingslot.addEventListener('drop', (ev) => backpackController.drop(ev));
    clearcraftingslot.addEventListener('dragover', (ev) => BackpackController.allowDrop(ev));

    (document.querySelector('#craftbutton') as HTMLButtonElement).addEventListener('click', () => {
      // A little wasteful but we'll do a final sanity check to ensure we have a compatible recipe and enough resources for it (we could otherwise cache the last evaluation)
      const matchedRecipe = backpackController.evaluateCrafting();

      if (!matchedRecipe || !matchedRecipe.enoughMaterials) {
        return;
      }

      matchedRecipe.resourceCosts.forEach((resourceCount, resourceName) => {
        const removeSuccessful = gameState.savable.backpack.remove(resourceName, resourceCount);
        if (!removeSuccessful) {
          // shouldn't occur because we performed a crafting evaluation beforehand, but sanity to avoid putting the backpack in an incorrect state.
          matchedRecipe.enoughMaterials = false;
        }
      });

      // Unexpected case. Should not occur.
      if (!matchedRecipe.enoughMaterials) {
        console.error("Detected there wasn't enough materials after confirming a matched recipe. Backpack might have had some resources removed already.");
        return;
      }

      const { objName } = matchedRecipe;
      const objClassProperties = gameState.objectClassProperties.get(objName);
      Narrator.playSound('craftingSound');

      const craftedMessage = `${gameState.savable.playerName} crafted a ${objName}.`;
      narrator.configureNarratorBox(craftedMessage);

      // pedantic, but we'll call crafting food from the backpack cooking still. "hands" are the cooking implement here.
      if (objClassProperties?.isEdible) {
        gameState.savable.addCount(CountType.Cooked, objName);

        if (!gameState.savable.discoveredRecipes.cooking.includes(objName)) {
          gameState.savable.discoveredRecipes.cooking.push(objName);
        }
      } else {
        gameState.savable.addCount(CountType.Crafted, objName);

        if (!gameState.savable.discoveredRecipes.crafting.includes(objName)) {
          gameState.savable.discoveredRecipes.crafting.push(objName);

          // reset crafting recipes to recognize the newly learned recipe.
          backpackController.configureCraftingRecipes();
        }
      }

      gameState.savable.backpack.add(objName, 1);

      // Re-evaluate crafting again, to see if we still have resources for another.
      backpackController.evaluateCrafting();

      // new state to save, but nothing new to draw.
      activeStateToSave += 1;
      showSaveSpinnerIfNecessary();

      // (because it's in the backpack view which is separate from canvas)
      backpackController.updateBackpackView(false, true);
    });

    backpackController.updateBackpackView(false, true);
  });

  togglebackpack.addEventListener('click', () => {
    if (apps.style.display === 'block') {
      closeapps.click();
    } else if (logbook.style.display === 'flex') {
      togglelog.click();
    } else if (shopview.style.display === 'block') {
      closeshop.click();
    }

    let message;
    if (backpackview.style.display === 'none') {
      message = `${gameState.savable.playerName} opened the backpack.`;
      backpackview.style.display = 'flex';

      (document.querySelector('#items') as HTMLButtonElement).click();
    } else {
      message = `${gameState.savable.playerName} closed the backpack.`;
      backpackview.style.display = 'none';
    }

    setMoneyAvailable();

    Narrator.playSound('toggleBackpackSound');
    narrator.configureNarratorBox(message);
  });

  togglelog.addEventListener('click', () => {
    if (apps.style.display === 'block') {
      closeapps.click();
    } else if (backpackview.style.display === 'flex') {
      togglebackpack.click();
    } else if (shopview.style.display === 'block') {
      closeshop.click();
    }

    let message;
    if (logbook.style.display === 'none') {
      message = `${gameState.savable.playerName} opened the logbook.`;
      togglelog.textContent = '📕';

      logbook.style.display = 'flex';
      (document.querySelector('#quests') as HTMLButtonElement).click();
    } else {
      message = `${gameState.savable.playerName} closed the logbook.`;
      logbook.style.display = 'none';
      togglelog.textContent = '📖';
    }

    Narrator.playSound('toggleLogSound');

    narrator.configureNarratorBox(message);
  });

  (document.querySelector('#returnhome') as HTMLButtonElement).addEventListener('click', () => {
    gameState.savable.globalXOffset = 0;
    gameState.savable.globalYOffset = 0;

    activeStateToSave += 1;
    showSaveSpinnerIfNecessary();
    painter.drawAllObjects();
  });

  (document.querySelector('#restart') as HTMLButtonElement).addEventListener('click', () => {
    if (spawnerInterval) {
      clearInterval(spawnerInterval);
    }

    if (saveInterval) {
      clearInterval(saveInterval);
    }

    ldb.delete(STORAGE_KEY);

    painter.clearImageCache();

    narrator.configureMessageBox(talkMessageBox, '');

    gameState.savable.objects.filter((obj: GameObject) => obj.ser.customState.paintingImageUrl).forEach((obj: GameObject) => {
      ldb.delete(`playground-${obj.ser.customState.id}states`);
      ldb.delete(`playground-${obj.ser.customState.id}penState`);
    });

    gameState.savable.backpack.statefulObjects.filter((obj: SerializableGameObject) => obj.customState.paintingImageUrl)
      .forEach((obj: SerializableGameObject) => {
        ldb.delete(`playground-${obj.customState.id}states`);
        ldb.delete(`playground-${obj.customState.id}penState`);
      });

    gameState.savable.peopleNamesBag = [];
    gameState.savable.objects = [];
    gameState.savable.resetCounts();

    gameState.savable.globalXOffset = 0;
    gameState.savable.globalYOffset = 0;

    gameState.savable.backpack = new Backpack();
    gameState.savable.discoveredRecipes = new DiscoveredRecipes();

    activeStateToSave = 0;
    currentSavedState = 0;

    gameState.savable.playerName = prompt("What's your name?") ?? 'You';
    if (gameState.savable.playerName.trim() === '') {
      gameState.savable.playerName = 'You';
    }

    let message;
    if (gameState.savable.playerName === 'You') {
      message = 'Welcome to the Playground!';
    } else {
      message = `Welcome to the Playground, ${gameState.savable.playerName}!`;
    }

    narrator.configureNarratorBox(message, 500);

    beginningObjects();
    activeStateToSave += 1;
    showSaveSpinnerIfNecessary();

    objectSpawner.resetChancesToSpawn();
    spawnerInterval = setInterval(objectSpawnHandler, 1000);
    saveInterval = setInterval(save, 5000);
    painter.drawAllObjects();
  });

  loadGameConfig();

  objectSpawner = new ObjectSpawner(
    spawnConfig,
    gameState,
    gameConfig.peoplenames,
    questTemplates,
    ctx,

    // Valid spawn area for objects to spawn in.
    () => ({
      x: -gameState.savable.globalXOffset,
      y: -gameState.savable.globalYOffset,
      width: canvas.width,
      height: canvas.height,
    } as BoundingBox),

    // Randomizer to jitter object spawning where it makes sense to do so.
    getRandomInt,

    // For checking whether objects are within a given path, e.g. the valid spawn area
    (path, x, y) => ctx.isPointInPath(path, x, y),

    // For checking whether other objects are within an object's path
    pointInPath,
  );

  backpackController = new BackpackController(
    gameState,
    backpackview,
    narrator,
    objectSpawner,
    getRandomInt,
    () => {
      setMoneyAvailable();

      activeStateToSave += 1;
      showSaveSpinnerIfNecessary();
      painter.drawAllObjects();
    },
  );

  const wandHandler = new WandHandler(
    gameState,
    objectSpawner,
    narrator,
    puzzleHandler,
    gameConfig.wishingWandPuzzles as Puzzle<unknown>[],
    getRandomInt,
    () => {
      activeStateToSave += 1;
      showSaveSpinnerIfNecessary();
      painter.drawAllObjects();
    },
  );

  pointerHandler = new PointerHandler(
    gameState,
    objectSpawner,
    narrator,
    wandHandler,
    talkMessageBox,
    canvasFront,
    getRandomInt,
    pointInPath,
    // Redraw when taking an action that creates an object or changes an object
    () => {
      activeStateToSave += 1;
      showSaveSpinnerIfNecessary();
      painter.drawAllObjects();
    },
    // Activate Puzzle (from a quest)
    (puzzle: Puzzle<unknown>) => {
      const questGiver = pointerHandler.activeConversation?.ser.givenName ?? 'the person';
      puzzleHandler.activatePuzzle(puzzle, questGiver);
    },
    // True when backpack is closed
    () => backpackview.style.display === 'none',
    // Activate Backpack (right click)
    () => togglebackpack.click(),
    // Activate Piano (right click)
    () => {
      apps.style.display = 'block';
      musicApp.style.display = 'block';
    },
    // Activate Shop (right click)
    (shop) => {
      setMoneyAvailable();
      shopHandler.activateShop(shop);
    },
    // Activate Painting (right click)
    (paintingObj) => {
      activePainting = paintingObj;
      apps.style.display = 'block';

      artApp.onload = () => {
        let squareWidth;
        if (artApp.clientWidth > artApp.clientHeight) {
          squareWidth = artApp.clientHeight - 200;
        } else {
          squareWidth = artApp.clientWidth - 200;
        }

        (artApp.contentWindow as ArtAppWindow).resizeCanvasToFitBounds(squareWidth, squareWidth);

        // If it doesn't have a painting already, initialize it with how we represent it in the playground.
        if (activePainting && !activePainting.ser.customState.paintingImageUrl) {
          const defaultRepresentation = painter.drawForArtCanvas(activePainting);
          (artApp.contentWindow as ArtAppWindow).drawOnCanvas(defaultRepresentation);
        }
      };

      artApp.src = `./apps/art/index.html?id=playground-${paintingObj.ser.customState.id}`;
      artApp.style.display = 'block';

      narrator.narrateFromAction(gameState, 'paintingSound', activePainting);
    },
    // Trash Painting (drag painting to trash can)
    (paintingObj) => {
      painter.deleteFromImageCache(paintingObj.ser.customState.id);
      if (paintingObj.ser.customState.paintingImageUrl) {
        ldb.delete(`playground-${paintingObj.ser.customState.id}states`);
        ldb.delete(`playground-${paintingObj.ser.customState.id}penState`);
      }
    },
  );

  navigationHandler = new NavigationHandler(
    gameState,
    talkMessageBox,
    // Disable navigation when apps are open.
    () => apps.style.display !== 'none',
    // On navigate, save the new postion and redraw.
    () => {
      activeStateToSave += 1;
      showSaveSpinnerIfNecessary();
      painter.drawAllObjects();
    },
  );

  canvasFront.addEventListener('pointerdown', (ev) => pointerHandler.handlePointerDown(ev));
  canvasFront.addEventListener('pointermove', (ev) => pointerHandler.handlePointerMove(ev));
  canvasFront.addEventListener('pointerup', (ev) => pointerHandler.handlePointerUp(ev));
  canvasFront.addEventListener('contextmenu', (ev) => pointerHandler.handleContextMenu(ev));
  window.addEventListener('pointerup', (ev) => pointerHandler.handlePointerUp(ev));
  window.addEventListener('keydown', (ev) => navigationHandler.handleKeyDown(ev));
  window.addEventListener('keyup', (ev) => navigationHandler.handleKeyUp(ev));

  load(
    // eslint-disable-next-line @typescript-eslint/require-await
    async () => {
      gameState.savable.playerName = prompt("What's your name?") ?? 'You';
      if (gameState.savable.playerName.trim() === '') {
        gameState.savable.playerName = 'You';
      }

      beginningObjects();
      activeStateToSave += 1;
    },
    // eslint-disable-next-line @typescript-eslint/require-await
    async () => {
      // if we introduce a new initial tool, spawn it near origin.
      // This may overlap with something else, but it's better than missing the tool and having to restart to get it.
      const initialCounts: Map<string, number> = new Map();
      initialObjects.forEach((objName) => {
        initialCounts.set(objName, (initialCounts.get(objName) ?? 0) + 1);
      });

      initialCounts.forEach((initialCount, objName) => {
        const countInBackpack = gameState.savable.backpack.totalInBackpack(objName);
        const countInField = gameState.savable.counts.get(objName) ?? 0;

        const totalToRespawn = initialCount - countInField - countInBackpack;
        for (let i = 0; i < totalToRespawn; i += 1) {
          objectSpawner.makeObject(objName, 20, 50);
        }
      });

      spawnerInterval = setInterval(objectSpawnHandler, 1000);
      saveInterval = setInterval(save, 5000);

      showSaveSpinnerIfNecessary();
      // sometimes the background isn't loaded before we reach this point. So ensure that we trigger drawing it when it is loaded.
      grassBackground.onload = () => {
        painter.drawAllObjects(grassBackground);
      };
      painter.drawAllObjects(grassBackground);

      // narrator.configureNarratorBox(message, 3000);

      const fallbackMessage = () => {
        let message;
        if (gameState.savable.playerName === 'You') {
          message = 'Welcome to the Playground!';
        } else {
          message = `Welcome to the Playground, ${gameState.savable.playerName}!`;
        }

        narrator.configureNarratorBox(message, 3000);
      };

      const canInvokeLLM = await canInvokeLLMCheck();
      if (canInvokeLLM) {
        invokeLLM(`write a friendly greeting to ${gameState.savable.playerName === 'You' ? 'me' : gameState.savable.playerName}, welcoming them to the playground!`, true).then((message) => {
          narrator.configureNarratorBox(message, 3000);
        }).catch((e) => {
          fallbackMessage();
          console.error(e);
        });
      } else {
        fallbackMessage();
      }
    },
  );
}

resolvePlatform?.then(() => {
  ready(onReady);
}, () => {});
