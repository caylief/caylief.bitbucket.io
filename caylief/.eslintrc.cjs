/* eslint-disable @typescript-eslint/no-unsafe-assignment */
module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'airbnb-base',
    'airbnb-typescript/base',
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  overrides: [
  ],
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: './tsconfig.json',
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  // TODO: for now, don't run ESLint on the art app. It needs a lot of work to get up to spec.
  ignorePatterns: ['apps/art/*.js'],
  rules: {
    // This feels like an odd one to have on by default. It's a handy part of the syntax and I've used it without issue. Just because it wasn't part of the spec originally doesn't mean it's anathema...
    'no-multi-str': 'off',

    // I get why some folks like this on, but for my purposes if my son runs into a bug, this may help debug it faster, and has, in the past.
    'no-console': 'off',

    // There's a variety of issues online about how '.mjs' and ts should be allowed by default but it appears ESLint hasn't done so yet.
    'import/extensions': ['error', 'always', {
      '.js': 'never',
      ts: 'never',
      '.mjs': 'never',
    }],

    // I prefer longer line length as I work on a 4K monitor. YMMV.
    'max-len': [
      'error',
      {
        code: 140,
        tabWidth: 2,
        ignoreComments: true, // "comments": 80
        ignoreUrls: true,
        ignoreStrings: true,
        ignoreTemplateLiterals: true,
      },
    ],
  },
};
