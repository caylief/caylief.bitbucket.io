/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import { resolve } from 'path';
import { defineConfig } from 'vite';
import eslint from 'vite-plugin-eslint';

export default defineConfig({
  base: 'https://caylief.bitbucket.io/caylief/dist/',
  plugins: [eslint({
    fix: true,
  })],
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, 'index.html'),
        artApp: resolve(__dirname, 'apps/art/index.html'),
        // ...
        // List all files you want in your build
      },
    },
  },
});
